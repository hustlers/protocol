#!/bin/bash

set -e;
set -e pipefail;

mkdir -p out/java out/rust out/java_c

[[ -e ./protocol.jar ]] && rm protocol.jar

echo "Compiling protobuf schema.."
protoc --java_out out/java \
       --rust_out out/rust \
       message.proto

echo "Applying patches.."
patch out/rust/message.rs ./rust.patch

echo "Compiling generated java.."
javac  \
   -cp $(find lib -name '*.jar' | tr '\n' ':') \
   -d "out/java_c" $(find out/java -name "*.java")

echo "Creating protocol.jar"
cd out/java_c
jar cvf ../../protocol.jar $(find ./ -name "*.class")
cd ../../

