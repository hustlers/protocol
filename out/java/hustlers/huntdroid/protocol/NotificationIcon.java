// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: message.proto

package hustlers.huntdroid.protocol;

/**
 * Protobuf enum {@code NotificationIcon}
 */
public enum NotificationIcon
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <pre>
   * Use the app's logo, for non-specific notifications
   * </pre>
   *
   * <code>GENERAL = 0;</code>
   */
  GENERAL(0),
  ;

  /**
   * <pre>
   * Use the app's logo, for non-specific notifications
   * </pre>
   *
   * <code>GENERAL = 0;</code>
   */
  public static final int GENERAL_VALUE = 0;


  public final int getNumber() {
    return value;
  }

  /**
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static NotificationIcon valueOf(int value) {
    return forNumber(value);
  }

  public static NotificationIcon forNumber(int value) {
    switch (value) {
      case 0: return GENERAL;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<NotificationIcon>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      NotificationIcon> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<NotificationIcon>() {
          public NotificationIcon findValueByNumber(int number) {
            return NotificationIcon.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return hustlers.huntdroid.protocol.MessageOuterClass.getDescriptor().getEnumTypes().get(0);
  }

  private static final NotificationIcon[] VALUES = values();

  public static NotificationIcon valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private NotificationIcon(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:NotificationIcon)
}

