// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: message.proto

package hustlers.huntdroid.protocol;

public interface UnlockOrBuilder extends
    // @@protoc_insertion_point(interface_extends:Unlock)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>required string code = 1;</code>
   */
  boolean hasCode();
  /**
   * <code>required string code = 1;</code>
   */
  java.lang.String getCode();
  /**
   * <code>required string code = 1;</code>
   */
  com.google.protobuf.ByteString
      getCodeBytes();
}
