// This file is generated. Do not edit
// @generated

// https://github.com/Manishearth/rust-clippy/issues/702
#![allow(unknown_lints)]
#![allow(clippy)]

#![cfg_attr(rustfmt, rustfmt_skip)]

#![allow(box_pointers)]
#![allow(dead_code)]
#![allow(missing_docs)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(trivial_casts)]
#![allow(unsafe_code)]
#![allow(unused_imports)]
#![allow(unused_results)]

use protobuf::Message as Message_imported_for_functions;
use protobuf::ProtobufEnum as ProtobufEnum_imported_for_functions;

#[derive(PartialEq,Clone,Default)]
pub struct Message {
    // message fields
    request_seq: ::std::option::Option<u32>,
    // message oneof groups
    transport: ::std::option::Option<MessageTransport>,
    msg_data: ::std::option::Option<MessageData>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Message {}

#[derive(Debug,Clone,PartialEq)]
pub enum MessageTransport {
    seq(u32),
    ack(u32),
}

#[derive(Debug,Clone,PartialEq)]
pub enum MessageData {
    data(Data),
    result(ReqResult),
    unlock(Unlock),
    data_delete(DataID),
    notify(Notification),
    data_add(Data),
    hunt_start(u32),
    json_hunt(::std::string::String),
    hunt_passwords(NewHunt),
    reset(Reset),
    login_request(Login),
    login_complete(Session),
    resume(Session),
    ping(bool),
}

impl Message {
    pub fn new() -> Message {
        ::std::default::Default::default()
    }

    pub fn get_msg_data(&self) -> &Option<MessageData> { &self.msg_data }
    pub fn get_transport(&self) -> &Option<MessageTransport> { &self.transport }
    pub fn take_msg_data(self) -> Option<MessageData> { self.msg_data }

    pub fn default_instance() -> &'static Message {
        static mut instance: ::protobuf::lazy::Lazy<Message> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Message,
        };
        unsafe {
            instance.get(Message::new)
        }
    }

    // optional uint32 seq = 1;

    pub fn clear_seq(&mut self) {
        self.transport = ::std::option::Option::None;
    }

    pub fn has_seq(&self) -> bool {
        match self.transport {
            ::std::option::Option::Some(MessageTransport::seq(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_seq(&mut self, v: u32) {
        self.transport = ::std::option::Option::Some(MessageTransport::seq(v))
    }

    pub fn get_seq(&self) -> u32 {
        match self.transport {
            ::std::option::Option::Some(MessageTransport::seq(v)) => v,
            _ => 0,
        }
    }

    // optional uint32 ack = 2;

    pub fn clear_ack(&mut self) {
        self.transport = ::std::option::Option::None;
    }

    pub fn has_ack(&self) -> bool {
        match self.transport {
            ::std::option::Option::Some(MessageTransport::ack(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_ack(&mut self, v: u32) {
        self.transport = ::std::option::Option::Some(MessageTransport::ack(v))
    }

    pub fn get_ack(&self) -> u32 {
        match self.transport {
            ::std::option::Option::Some(MessageTransport::ack(v)) => v,
            _ => 0,
        }
    }

    // optional .Data data = 3;

    pub fn clear_data(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_data(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_data(&mut self, v: Data) {
        self.msg_data = ::std::option::Option::Some(MessageData::data(v))
    }

    // Mutable pointer to the field.
    pub fn mut_data(&mut self) -> &mut Data {
        if let ::std::option::Option::Some(MessageData::data(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::data(Data::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_data(&mut self) -> Data {
        if self.has_data() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::data(v)) => v,
                _ => panic!(),
            }
        } else {
            Data::new()
        }
    }

    pub fn get_data(&self) -> &Data {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data(ref v)) => v,
            _ => Data::default_instance(),
        }
    }

    // optional .ReqResult result = 4;

    pub fn clear_result(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_result(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::result(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_result(&mut self, v: ReqResult) {
        self.msg_data = ::std::option::Option::Some(MessageData::result(v))
    }

    // Mutable pointer to the field.
    pub fn mut_result(&mut self) -> &mut ReqResult {
        if let ::std::option::Option::Some(MessageData::result(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::result(ReqResult::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::result(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_result(&mut self) -> ReqResult {
        if self.has_result() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::result(v)) => v,
                _ => panic!(),
            }
        } else {
            ReqResult::new()
        }
    }

    pub fn get_result(&self) -> &ReqResult {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::result(ref v)) => v,
            _ => ReqResult::default_instance(),
        }
    }

    // optional .Unlock unlock = 5;

    pub fn clear_unlock(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_unlock(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::unlock(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_unlock(&mut self, v: Unlock) {
        self.msg_data = ::std::option::Option::Some(MessageData::unlock(v))
    }

    // Mutable pointer to the field.
    pub fn mut_unlock(&mut self) -> &mut Unlock {
        if let ::std::option::Option::Some(MessageData::unlock(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::unlock(Unlock::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::unlock(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_unlock(&mut self) -> Unlock {
        if self.has_unlock() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::unlock(v)) => v,
                _ => panic!(),
            }
        } else {
            Unlock::new()
        }
    }

    pub fn get_unlock(&self) -> &Unlock {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::unlock(ref v)) => v,
            _ => Unlock::default_instance(),
        }
    }

    // optional .DataID data_delete = 6;

    pub fn clear_data_delete(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_data_delete(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data_delete(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_data_delete(&mut self, v: DataID) {
        self.msg_data = ::std::option::Option::Some(MessageData::data_delete(v))
    }

    // Mutable pointer to the field.
    pub fn mut_data_delete(&mut self) -> &mut DataID {
        if let ::std::option::Option::Some(MessageData::data_delete(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::data_delete(DataID::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data_delete(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_data_delete(&mut self) -> DataID {
        if self.has_data_delete() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::data_delete(v)) => v,
                _ => panic!(),
            }
        } else {
            DataID::new()
        }
    }

    pub fn get_data_delete(&self) -> &DataID {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data_delete(ref v)) => v,
            _ => DataID::default_instance(),
        }
    }

    // optional .Notification notify = 10;

    pub fn clear_notify(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_notify(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::notify(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_notify(&mut self, v: Notification) {
        self.msg_data = ::std::option::Option::Some(MessageData::notify(v))
    }

    // Mutable pointer to the field.
    pub fn mut_notify(&mut self) -> &mut Notification {
        if let ::std::option::Option::Some(MessageData::notify(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::notify(Notification::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::notify(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_notify(&mut self) -> Notification {
        if self.has_notify() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::notify(v)) => v,
                _ => panic!(),
            }
        } else {
            Notification::new()
        }
    }

    pub fn get_notify(&self) -> &Notification {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::notify(ref v)) => v,
            _ => Notification::default_instance(),
        }
    }

    // optional .Data data_add = 11;

    pub fn clear_data_add(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_data_add(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data_add(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_data_add(&mut self, v: Data) {
        self.msg_data = ::std::option::Option::Some(MessageData::data_add(v))
    }

    // Mutable pointer to the field.
    pub fn mut_data_add(&mut self) -> &mut Data {
        if let ::std::option::Option::Some(MessageData::data_add(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::data_add(Data::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data_add(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_data_add(&mut self) -> Data {
        if self.has_data_add() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::data_add(v)) => v,
                _ => panic!(),
            }
        } else {
            Data::new()
        }
    }

    pub fn get_data_add(&self) -> &Data {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::data_add(ref v)) => v,
            _ => Data::default_instance(),
        }
    }

    // optional uint32 hunt_start = 12;

    pub fn clear_hunt_start(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_hunt_start(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::hunt_start(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_hunt_start(&mut self, v: u32) {
        self.msg_data = ::std::option::Option::Some(MessageData::hunt_start(v))
    }

    pub fn get_hunt_start(&self) -> u32 {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::hunt_start(v)) => v,
            _ => 0,
        }
    }

    // optional string json_hunt = 13;

    pub fn clear_json_hunt(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_json_hunt(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::json_hunt(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_json_hunt(&mut self, v: ::std::string::String) {
        self.msg_data = ::std::option::Option::Some(MessageData::json_hunt(v))
    }

    // Mutable pointer to the field.
    pub fn mut_json_hunt(&mut self) -> &mut ::std::string::String {
        if let ::std::option::Option::Some(MessageData::json_hunt(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::json_hunt(::std::string::String::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::json_hunt(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_json_hunt(&mut self) -> ::std::string::String {
        if self.has_json_hunt() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::json_hunt(v)) => v,
                _ => panic!(),
            }
        } else {
            ::std::string::String::new()
        }
    }

    pub fn get_json_hunt(&self) -> &str {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::json_hunt(ref v)) => v,
            _ => "",
        }
    }

    // optional .NewHunt hunt_passwords = 16;

    pub fn clear_hunt_passwords(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_hunt_passwords(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::hunt_passwords(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_hunt_passwords(&mut self, v: NewHunt) {
        self.msg_data = ::std::option::Option::Some(MessageData::hunt_passwords(v))
    }

    // Mutable pointer to the field.
    pub fn mut_hunt_passwords(&mut self) -> &mut NewHunt {
        if let ::std::option::Option::Some(MessageData::hunt_passwords(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::hunt_passwords(NewHunt::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::hunt_passwords(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_hunt_passwords(&mut self) -> NewHunt {
        if self.has_hunt_passwords() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::hunt_passwords(v)) => v,
                _ => panic!(),
            }
        } else {
            NewHunt::new()
        }
    }

    pub fn get_hunt_passwords(&self) -> &NewHunt {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::hunt_passwords(ref v)) => v,
            _ => NewHunt::default_instance(),
        }
    }

    // optional .Reset reset = 17;

    pub fn clear_reset(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_reset(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::reset(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_reset(&mut self, v: Reset) {
        self.msg_data = ::std::option::Option::Some(MessageData::reset(v))
    }

    // Mutable pointer to the field.
    pub fn mut_reset(&mut self) -> &mut Reset {
        if let ::std::option::Option::Some(MessageData::reset(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::reset(Reset::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::reset(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_reset(&mut self) -> Reset {
        if self.has_reset() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::reset(v)) => v,
                _ => panic!(),
            }
        } else {
            Reset::new()
        }
    }

    pub fn get_reset(&self) -> &Reset {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::reset(ref v)) => v,
            _ => Reset::default_instance(),
        }
    }

    // optional .Login login_request = 7;

    pub fn clear_login_request(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_login_request(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::login_request(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_login_request(&mut self, v: Login) {
        self.msg_data = ::std::option::Option::Some(MessageData::login_request(v))
    }

    // Mutable pointer to the field.
    pub fn mut_login_request(&mut self) -> &mut Login {
        if let ::std::option::Option::Some(MessageData::login_request(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::login_request(Login::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::login_request(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_login_request(&mut self) -> Login {
        if self.has_login_request() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::login_request(v)) => v,
                _ => panic!(),
            }
        } else {
            Login::new()
        }
    }

    pub fn get_login_request(&self) -> &Login {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::login_request(ref v)) => v,
            _ => Login::default_instance(),
        }
    }

    // optional .Session login_complete = 8;

    pub fn clear_login_complete(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_login_complete(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::login_complete(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_login_complete(&mut self, v: Session) {
        self.msg_data = ::std::option::Option::Some(MessageData::login_complete(v))
    }

    // Mutable pointer to the field.
    pub fn mut_login_complete(&mut self) -> &mut Session {
        if let ::std::option::Option::Some(MessageData::login_complete(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::login_complete(Session::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::login_complete(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_login_complete(&mut self) -> Session {
        if self.has_login_complete() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::login_complete(v)) => v,
                _ => panic!(),
            }
        } else {
            Session::new()
        }
    }

    pub fn get_login_complete(&self) -> &Session {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::login_complete(ref v)) => v,
            _ => Session::default_instance(),
        }
    }

    // optional .Session resume = 9;

    pub fn clear_resume(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_resume(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::resume(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_resume(&mut self, v: Session) {
        self.msg_data = ::std::option::Option::Some(MessageData::resume(v))
    }

    // Mutable pointer to the field.
    pub fn mut_resume(&mut self) -> &mut Session {
        if let ::std::option::Option::Some(MessageData::resume(_)) = self.msg_data {
        } else {
            self.msg_data = ::std::option::Option::Some(MessageData::resume(Session::new()));
        }
        match self.msg_data {
            ::std::option::Option::Some(MessageData::resume(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_resume(&mut self) -> Session {
        if self.has_resume() {
            match self.msg_data.take() {
                ::std::option::Option::Some(MessageData::resume(v)) => v,
                _ => panic!(),
            }
        } else {
            Session::new()
        }
    }

    pub fn get_resume(&self) -> &Session {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::resume(ref v)) => v,
            _ => Session::default_instance(),
        }
    }

    // optional bool ping = 14;

    pub fn clear_ping(&mut self) {
        self.msg_data = ::std::option::Option::None;
    }

    pub fn has_ping(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::ping(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_ping(&mut self, v: bool) {
        self.msg_data = ::std::option::Option::Some(MessageData::ping(v))
    }

    pub fn get_ping(&self) -> bool {
        match self.msg_data {
            ::std::option::Option::Some(MessageData::ping(v)) => v,
            _ => false,
        }
    }

    // optional uint32 request_seq = 15;

    pub fn clear_request_seq(&mut self) {
        self.request_seq = ::std::option::Option::None;
    }

    pub fn has_request_seq(&self) -> bool {
        self.request_seq.is_some()
    }

    // Param is passed by value, moved
    pub fn set_request_seq(&mut self, v: u32) {
        self.request_seq = ::std::option::Option::Some(v);
    }

    pub fn get_request_seq(&self) -> u32 {
        self.request_seq.unwrap_or(0)
    }

    fn get_request_seq_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.request_seq
    }

    fn mut_request_seq_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.request_seq
    }
}

impl ::protobuf::Message for Message {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.transport = ::std::option::Option::Some(MessageTransport::seq(is.read_uint32()?));
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.transport = ::std::option::Option::Some(MessageTransport::ack(is.read_uint32()?));
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::data(is.read_message()?));
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::result(is.read_message()?));
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::unlock(is.read_message()?));
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::data_delete(is.read_message()?));
                },
                10 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::notify(is.read_message()?));
                },
                11 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::data_add(is.read_message()?));
                },
                12 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::hunt_start(is.read_uint32()?));
                },
                13 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::json_hunt(is.read_string()?));
                },
                16 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::hunt_passwords(is.read_message()?));
                },
                17 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::reset(is.read_message()?));
                },
                7 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::login_request(is.read_message()?));
                },
                8 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::login_complete(is.read_message()?));
                },
                9 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::resume(is.read_message()?));
                },
                14 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.msg_data = ::std::option::Option::Some(MessageData::ping(is.read_bool()?));
                },
                15 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.request_seq = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.request_seq {
            my_size += ::protobuf::rt::value_size(15, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let ::std::option::Option::Some(ref v) = self.transport {
            match v {
                &MessageTransport::seq(v) => {
                    my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
                },
                &MessageTransport::ack(v) => {
                    my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
                },
            };
        };
        if let ::std::option::Option::Some(ref v) = self.msg_data {
            match v {
                &MessageData::data(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::result(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::unlock(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::data_delete(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::notify(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::data_add(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::hunt_start(v) => {
                    my_size += ::protobuf::rt::value_size(12, v, ::protobuf::wire_format::WireTypeVarint);
                },
                &MessageData::json_hunt(ref v) => {
                    my_size += ::protobuf::rt::string_size(13, &v);
                },
                &MessageData::hunt_passwords(ref v) => {
                    let len = v.compute_size();
                    my_size += 2 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::reset(ref v) => {
                    let len = v.compute_size();
                    my_size += 2 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::login_request(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::login_complete(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::resume(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &MessageData::ping(v) => {
                    my_size += 2;
                },
            };
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.request_seq {
            os.write_uint32(15, v)?;
        };
        if let ::std::option::Option::Some(ref v) = self.transport {
            match v {
                &MessageTransport::seq(v) => {
                    os.write_uint32(1, v)?;
                },
                &MessageTransport::ack(v) => {
                    os.write_uint32(2, v)?;
                },
            };
        };
        if let ::std::option::Option::Some(ref v) = self.msg_data {
            match v {
                &MessageData::data(ref v) => {
                    os.write_tag(3, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::result(ref v) => {
                    os.write_tag(4, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::unlock(ref v) => {
                    os.write_tag(5, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::data_delete(ref v) => {
                    os.write_tag(6, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::notify(ref v) => {
                    os.write_tag(10, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::data_add(ref v) => {
                    os.write_tag(11, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::hunt_start(v) => {
                    os.write_uint32(12, v)?;
                },
                &MessageData::json_hunt(ref v) => {
                    os.write_string(13, v)?;
                },
                &MessageData::hunt_passwords(ref v) => {
                    os.write_tag(16, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::reset(ref v) => {
                    os.write_tag(17, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::login_request(ref v) => {
                    os.write_tag(7, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::login_complete(ref v) => {
                    os.write_tag(8, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::resume(ref v) => {
                    os.write_tag(9, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &MessageData::ping(v) => {
                    os.write_bool(14, v)?;
                },
            };
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Message {
    fn new() -> Message {
        Message::new()
    }

    fn descriptor_static(_: ::std::option::Option<Message>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_u32_accessor::<_>(
                    "seq",
                    Message::has_seq,
                    Message::get_seq,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_u32_accessor::<_>(
                    "ack",
                    Message::has_ack,
                    Message::get_ack,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Data>(
                    "data",
                    Message::has_data,
                    Message::get_data,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, ReqResult>(
                    "result",
                    Message::has_result,
                    Message::get_result,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Unlock>(
                    "unlock",
                    Message::has_unlock,
                    Message::get_unlock,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, DataID>(
                    "data_delete",
                    Message::has_data_delete,
                    Message::get_data_delete,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Notification>(
                    "notify",
                    Message::has_notify,
                    Message::get_notify,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Data>(
                    "data_add",
                    Message::has_data_add,
                    Message::get_data_add,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_u32_accessor::<_>(
                    "hunt_start",
                    Message::has_hunt_start,
                    Message::get_hunt_start,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_string_accessor::<_>(
                    "json_hunt",
                    Message::has_json_hunt,
                    Message::get_json_hunt,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, NewHunt>(
                    "hunt_passwords",
                    Message::has_hunt_passwords,
                    Message::get_hunt_passwords,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Reset>(
                    "reset",
                    Message::has_reset,
                    Message::get_reset,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Login>(
                    "login_request",
                    Message::has_login_request,
                    Message::get_login_request,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Session>(
                    "login_complete",
                    Message::has_login_complete,
                    Message::get_login_complete,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Session>(
                    "resume",
                    Message::has_resume,
                    Message::get_resume,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_bool_accessor::<_>(
                    "ping",
                    Message::has_ping,
                    Message::get_ping,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "request_seq",
                    Message::get_request_seq_for_reflect,
                    Message::mut_request_seq_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Message>(
                    "Message",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Message {
    fn clear(&mut self) {
        self.clear_seq();
        self.clear_ack();
        self.clear_data();
        self.clear_result();
        self.clear_unlock();
        self.clear_data_delete();
        self.clear_notify();
        self.clear_data_add();
        self.clear_hunt_start();
        self.clear_json_hunt();
        self.clear_hunt_passwords();
        self.clear_reset();
        self.clear_login_request();
        self.clear_login_complete();
        self.clear_resume();
        self.clear_ping();
        self.clear_request_seq();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Message {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Message {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Session {
    // message fields
    user_id: ::std::option::Option<u32>,
    session_id: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Session {}

impl Session {
    pub fn new() -> Session {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Session {
        static mut instance: ::protobuf::lazy::Lazy<Session> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Session,
        };
        unsafe {
            instance.get(Session::new)
        }
    }

    // required uint32 user_id = 1;

    pub fn clear_user_id(&mut self) {
        self.user_id = ::std::option::Option::None;
    }

    pub fn has_user_id(&self) -> bool {
        self.user_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_user_id(&mut self, v: u32) {
        self.user_id = ::std::option::Option::Some(v);
    }

    pub fn get_user_id(&self) -> u32 {
        self.user_id.unwrap_or(0)
    }

    fn get_user_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.user_id
    }

    fn mut_user_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.user_id
    }

    // required string session_id = 2;

    pub fn clear_session_id(&mut self) {
        self.session_id.clear();
    }

    pub fn has_session_id(&self) -> bool {
        self.session_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_session_id(&mut self, v: ::std::string::String) {
        self.session_id = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_session_id(&mut self) -> &mut ::std::string::String {
        if self.session_id.is_none() {
            self.session_id.set_default();
        };
        self.session_id.as_mut().unwrap()
    }

    // Take field
    pub fn take_session_id(&mut self) -> ::std::string::String {
        self.session_id.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_session_id(&self) -> &str {
        match self.session_id.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_session_id_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.session_id
    }

    fn mut_session_id_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.session_id
    }
}

impl ::protobuf::Message for Session {
    fn is_initialized(&self) -> bool {
        if self.user_id.is_none() {
            return false;
        };
        if self.session_id.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.user_id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.session_id)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.user_id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.session_id.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.user_id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.session_id.as_ref() {
            os.write_string(2, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Session {
    fn new() -> Session {
        Session::new()
    }

    fn descriptor_static(_: ::std::option::Option<Session>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "user_id",
                    Session::get_user_id_for_reflect,
                    Session::mut_user_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "session_id",
                    Session::get_session_id_for_reflect,
                    Session::mut_session_id_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Session>(
                    "Session",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Session {
    fn clear(&mut self) {
        self.clear_user_id();
        self.clear_session_id();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Session {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Session {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Unlock {
    // message fields
    code: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Unlock {}

impl Unlock {
    pub fn new() -> Unlock {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Unlock {
        static mut instance: ::protobuf::lazy::Lazy<Unlock> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Unlock,
        };
        unsafe {
            instance.get(Unlock::new)
        }
    }

    // required string code = 1;

    pub fn clear_code(&mut self) {
        self.code.clear();
    }

    pub fn has_code(&self) -> bool {
        self.code.is_some()
    }

    // Param is passed by value, moved
    pub fn set_code(&mut self, v: ::std::string::String) {
        self.code = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_code(&mut self) -> &mut ::std::string::String {
        if self.code.is_none() {
            self.code.set_default();
        };
        self.code.as_mut().unwrap()
    }

    // Take field
    pub fn take_code(&mut self) -> ::std::string::String {
        self.code.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_code(&self) -> &str {
        match self.code.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_code_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.code
    }

    fn mut_code_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.code
    }
}

impl ::protobuf::Message for Unlock {
    fn is_initialized(&self) -> bool {
        if self.code.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.code)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.code.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.code.as_ref() {
            os.write_string(1, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Unlock {
    fn new() -> Unlock {
        Unlock::new()
    }

    fn descriptor_static(_: ::std::option::Option<Unlock>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "code",
                    Unlock::get_code_for_reflect,
                    Unlock::mut_code_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Unlock>(
                    "Unlock",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Unlock {
    fn clear(&mut self) {
        self.clear_code();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Unlock {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Unlock {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Notification {
    // message fields
    title: ::protobuf::SingularField<::std::string::String>,
    text: ::protobuf::SingularField<::std::string::String>,
    icon: ::std::option::Option<NotificationIcon>,
    id: ::std::option::Option<u32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Notification {}

impl Notification {
    pub fn new() -> Notification {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Notification {
        static mut instance: ::protobuf::lazy::Lazy<Notification> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Notification,
        };
        unsafe {
            instance.get(Notification::new)
        }
    }

    // required string title = 1;

    pub fn clear_title(&mut self) {
        self.title.clear();
    }

    pub fn has_title(&self) -> bool {
        self.title.is_some()
    }

    // Param is passed by value, moved
    pub fn set_title(&mut self, v: ::std::string::String) {
        self.title = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_title(&mut self) -> &mut ::std::string::String {
        if self.title.is_none() {
            self.title.set_default();
        };
        self.title.as_mut().unwrap()
    }

    // Take field
    pub fn take_title(&mut self) -> ::std::string::String {
        self.title.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_title(&self) -> &str {
        match self.title.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_title_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.title
    }

    fn mut_title_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.title
    }

    // required string text = 2;

    pub fn clear_text(&mut self) {
        self.text.clear();
    }

    pub fn has_text(&self) -> bool {
        self.text.is_some()
    }

    // Param is passed by value, moved
    pub fn set_text(&mut self, v: ::std::string::String) {
        self.text = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_text(&mut self) -> &mut ::std::string::String {
        if self.text.is_none() {
            self.text.set_default();
        };
        self.text.as_mut().unwrap()
    }

    // Take field
    pub fn take_text(&mut self) -> ::std::string::String {
        self.text.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_text(&self) -> &str {
        match self.text.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_text_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.text
    }

    fn mut_text_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.text
    }

    // required .NotificationIcon icon = 3;

    pub fn clear_icon(&mut self) {
        self.icon = ::std::option::Option::None;
    }

    pub fn has_icon(&self) -> bool {
        self.icon.is_some()
    }

    // Param is passed by value, moved
    pub fn set_icon(&mut self, v: NotificationIcon) {
        self.icon = ::std::option::Option::Some(v);
    }

    pub fn get_icon(&self) -> NotificationIcon {
        self.icon.unwrap_or(NotificationIcon::GENERAL)
    }

    fn get_icon_for_reflect(&self) -> &::std::option::Option<NotificationIcon> {
        &self.icon
    }

    fn mut_icon_for_reflect(&mut self) -> &mut ::std::option::Option<NotificationIcon> {
        &mut self.icon
    }

    // required uint32 id = 4;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }
}

impl ::protobuf::Message for Notification {
    fn is_initialized(&self) -> bool {
        if self.title.is_none() {
            return false;
        };
        if self.text.is_none() {
            return false;
        };
        if self.icon.is_none() {
            return false;
        };
        if self.id.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.title)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.text)?;
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_enum()?;
                    self.icon = ::std::option::Option::Some(tmp);
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.title.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        };
        if let Some(v) = self.text.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        if let Some(v) = self.icon {
            my_size += ::protobuf::rt::enum_size(3, v);
        };
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(4, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.title.as_ref() {
            os.write_string(1, &v)?;
        };
        if let Some(v) = self.text.as_ref() {
            os.write_string(2, &v)?;
        };
        if let Some(v) = self.icon {
            os.write_enum(3, v.value())?;
        };
        if let Some(v) = self.id {
            os.write_uint32(4, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Notification {
    fn new() -> Notification {
        Notification::new()
    }

    fn descriptor_static(_: ::std::option::Option<Notification>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "title",
                    Notification::get_title_for_reflect,
                    Notification::mut_title_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "text",
                    Notification::get_text_for_reflect,
                    Notification::mut_text_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<NotificationIcon>>(
                    "icon",
                    Notification::get_icon_for_reflect,
                    Notification::mut_icon_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Notification::get_id_for_reflect,
                    Notification::mut_id_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Notification>(
                    "Notification",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Notification {
    fn clear(&mut self) {
        self.clear_title();
        self.clear_text();
        self.clear_icon();
        self.clear_id();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Notification {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Notification {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Login {
    // message fields
    username: ::protobuf::SingularField<::std::string::String>,
    password: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Login {}

impl Login {
    pub fn new() -> Login {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Login {
        static mut instance: ::protobuf::lazy::Lazy<Login> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Login,
        };
        unsafe {
            instance.get(Login::new)
        }
    }

    // required string username = 1;

    pub fn clear_username(&mut self) {
        self.username.clear();
    }

    pub fn has_username(&self) -> bool {
        self.username.is_some()
    }

    // Param is passed by value, moved
    pub fn set_username(&mut self, v: ::std::string::String) {
        self.username = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_username(&mut self) -> &mut ::std::string::String {
        if self.username.is_none() {
            self.username.set_default();
        };
        self.username.as_mut().unwrap()
    }

    // Take field
    pub fn take_username(&mut self) -> ::std::string::String {
        self.username.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_username(&self) -> &str {
        match self.username.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_username_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.username
    }

    fn mut_username_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.username
    }

    // required string password = 2;

    pub fn clear_password(&mut self) {
        self.password.clear();
    }

    pub fn has_password(&self) -> bool {
        self.password.is_some()
    }

    // Param is passed by value, moved
    pub fn set_password(&mut self, v: ::std::string::String) {
        self.password = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_password(&mut self) -> &mut ::std::string::String {
        if self.password.is_none() {
            self.password.set_default();
        };
        self.password.as_mut().unwrap()
    }

    // Take field
    pub fn take_password(&mut self) -> ::std::string::String {
        self.password.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_password(&self) -> &str {
        match self.password.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_password_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.password
    }

    fn mut_password_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.password
    }
}

impl ::protobuf::Message for Login {
    fn is_initialized(&self) -> bool {
        if self.username.is_none() {
            return false;
        };
        if self.password.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.username)?;
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.password)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.username.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        };
        if let Some(v) = self.password.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.username.as_ref() {
            os.write_string(1, &v)?;
        };
        if let Some(v) = self.password.as_ref() {
            os.write_string(2, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Login {
    fn new() -> Login {
        Login::new()
    }

    fn descriptor_static(_: ::std::option::Option<Login>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "username",
                    Login::get_username_for_reflect,
                    Login::mut_username_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "password",
                    Login::get_password_for_reflect,
                    Login::mut_password_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Login>(
                    "Login",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Login {
    fn clear(&mut self) {
        self.clear_username();
        self.clear_password();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Login {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Login {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct DataID {
    // message fields
    field_type: ::std::option::Option<DataType>,
    ids: ::std::option::Option<u32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for DataID {}

impl DataID {
    pub fn new() -> DataID {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static DataID {
        static mut instance: ::protobuf::lazy::Lazy<DataID> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const DataID,
        };
        unsafe {
            instance.get(DataID::new)
        }
    }

    // required .DataType type = 1;

    pub fn clear_field_type(&mut self) {
        self.field_type = ::std::option::Option::None;
    }

    pub fn has_field_type(&self) -> bool {
        self.field_type.is_some()
    }

    // Param is passed by value, moved
    pub fn set_field_type(&mut self, v: DataType) {
        self.field_type = ::std::option::Option::Some(v);
    }

    pub fn get_field_type(&self) -> DataType {
        self.field_type.unwrap_or(DataType::HUNT)
    }

    fn get_field_type_for_reflect(&self) -> &::std::option::Option<DataType> {
        &self.field_type
    }

    fn mut_field_type_for_reflect(&mut self) -> &mut ::std::option::Option<DataType> {
        &mut self.field_type
    }

    // required uint32 ids = 2;

    pub fn clear_ids(&mut self) {
        self.ids = ::std::option::Option::None;
    }

    pub fn has_ids(&self) -> bool {
        self.ids.is_some()
    }

    // Param is passed by value, moved
    pub fn set_ids(&mut self, v: u32) {
        self.ids = ::std::option::Option::Some(v);
    }

    pub fn get_ids(&self) -> u32 {
        self.ids.unwrap_or(0)
    }

    fn get_ids_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.ids
    }

    fn mut_ids_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.ids
    }
}

impl ::protobuf::Message for DataID {
    fn is_initialized(&self) -> bool {
        if self.field_type.is_none() {
            return false;
        };
        if self.ids.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_enum()?;
                    self.field_type = ::std::option::Option::Some(tmp);
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.ids = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.field_type {
            my_size += ::protobuf::rt::enum_size(1, v);
        };
        if let Some(v) = self.ids {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.field_type {
            os.write_enum(1, v.value())?;
        };
        if let Some(v) = self.ids {
            os.write_uint32(2, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for DataID {
    fn new() -> DataID {
        DataID::new()
    }

    fn descriptor_static(_: ::std::option::Option<DataID>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<DataType>>(
                    "type",
                    DataID::get_field_type_for_reflect,
                    DataID::mut_field_type_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "ids",
                    DataID::get_ids_for_reflect,
                    DataID::mut_ids_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<DataID>(
                    "DataID",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for DataID {
    fn clear(&mut self) {
        self.clear_field_type();
        self.clear_ids();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for DataID {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for DataID {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Data {
    // message oneof groups
    data: ::std::option::Option<DataInner>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Data {}

#[derive(Debug,Clone,PartialEq)]
pub enum DataInner {
    hunt(Hunt),
    user(User),
    team(Team),
    point(Point),
    bonus(Bonus),
    chat(Chat),
    chat_message(ChatMessage),
    login_details(LoginDetails),
}

impl DataInner {
   pub fn into_msg(self) -> Message {
      let mut msg = Message::new();
      let mut msg_data = Data::new();
      msg_data.data = Some(self);
      msg.set_data(msg_data);
      msg
   }
}

impl Data {
    pub fn new() -> Data {
        ::std::default::Default::default()
    }

    pub fn into_inner(self) -> Option<DataInner> { self.data }
    pub fn get_inner(&self) -> &Option<DataInner> { &self.data }

    pub fn set_inner(&mut self, data: DataInner) {
       self.data = ::std::option::Option::Some(data)
    }

    pub fn default_instance() -> &'static Data {
        static mut instance: ::protobuf::lazy::Lazy<Data> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Data,
        };
        unsafe {
            instance.get(Data::new)
        }
    }

    // optional .Hunt hunt = 2;

    pub fn clear_hunt(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_hunt(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::hunt(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_hunt(&mut self, v: Hunt) {
        self.data = ::std::option::Option::Some(DataInner::hunt(v))
    }

    // Mutable pointer to the field.
    pub fn mut_hunt(&mut self) -> &mut Hunt {
        if let ::std::option::Option::Some(DataInner::hunt(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::hunt(Hunt::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::hunt(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_hunt(&mut self) -> Hunt {
        if self.has_hunt() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::hunt(v)) => v,
                _ => panic!(),
            }
        } else {
            Hunt::new()
        }
    }

    pub fn get_hunt(&self) -> &Hunt {
        match self.data {
            ::std::option::Option::Some(DataInner::hunt(ref v)) => v,
            _ => Hunt::default_instance(),
        }
    }

    // optional .User user = 3;

    pub fn clear_user(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_user(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::user(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_user(&mut self, v: User) {
        self.data = ::std::option::Option::Some(DataInner::user(v))
    }

    // Mutable pointer to the field.
    pub fn mut_user(&mut self) -> &mut User {
        if let ::std::option::Option::Some(DataInner::user(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::user(User::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::user(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_user(&mut self) -> User {
        if self.has_user() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::user(v)) => v,
                _ => panic!(),
            }
        } else {
            User::new()
        }
    }

    pub fn get_user(&self) -> &User {
        match self.data {
            ::std::option::Option::Some(DataInner::user(ref v)) => v,
            _ => User::default_instance(),
        }
    }

    // optional .Team team = 4;

    pub fn clear_team(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_team(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::team(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_team(&mut self, v: Team) {
        self.data = ::std::option::Option::Some(DataInner::team(v))
    }

    // Mutable pointer to the field.
    pub fn mut_team(&mut self) -> &mut Team {
        if let ::std::option::Option::Some(DataInner::team(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::team(Team::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::team(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_team(&mut self) -> Team {
        if self.has_team() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::team(v)) => v,
                _ => panic!(),
            }
        } else {
            Team::new()
        }
    }

    pub fn get_team(&self) -> &Team {
        match self.data {
            ::std::option::Option::Some(DataInner::team(ref v)) => v,
            _ => Team::default_instance(),
        }
    }

    // optional .Point point = 5;

    pub fn clear_point(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_point(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::point(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_point(&mut self, v: Point) {
        self.data = ::std::option::Option::Some(DataInner::point(v))
    }

    // Mutable pointer to the field.
    pub fn mut_point(&mut self) -> &mut Point {
        if let ::std::option::Option::Some(DataInner::point(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::point(Point::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::point(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_point(&mut self) -> Point {
        if self.has_point() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::point(v)) => v,
                _ => panic!(),
            }
        } else {
            Point::new()
        }
    }

    pub fn get_point(&self) -> &Point {
        match self.data {
            ::std::option::Option::Some(DataInner::point(ref v)) => v,
            _ => Point::default_instance(),
        }
    }

    // optional .Bonus bonus = 7;

    pub fn clear_bonus(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_bonus(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::bonus(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_bonus(&mut self, v: Bonus) {
        self.data = ::std::option::Option::Some(DataInner::bonus(v))
    }

    // Mutable pointer to the field.
    pub fn mut_bonus(&mut self) -> &mut Bonus {
        if let ::std::option::Option::Some(DataInner::bonus(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::bonus(Bonus::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::bonus(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_bonus(&mut self) -> Bonus {
        if self.has_bonus() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::bonus(v)) => v,
                _ => panic!(),
            }
        } else {
            Bonus::new()
        }
    }

    pub fn get_bonus(&self) -> &Bonus {
        match self.data {
            ::std::option::Option::Some(DataInner::bonus(ref v)) => v,
            _ => Bonus::default_instance(),
        }
    }

    // optional .Chat chat = 8;

    pub fn clear_chat(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_chat(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::chat(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_chat(&mut self, v: Chat) {
        self.data = ::std::option::Option::Some(DataInner::chat(v))
    }

    // Mutable pointer to the field.
    pub fn mut_chat(&mut self) -> &mut Chat {
        if let ::std::option::Option::Some(DataInner::chat(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::chat(Chat::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::chat(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_chat(&mut self) -> Chat {
        if self.has_chat() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::chat(v)) => v,
                _ => panic!(),
            }
        } else {
            Chat::new()
        }
    }

    pub fn get_chat(&self) -> &Chat {
        match self.data {
            ::std::option::Option::Some(DataInner::chat(ref v)) => v,
            _ => Chat::default_instance(),
        }
    }

    // optional .ChatMessage chat_message = 9;

    pub fn clear_chat_message(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_chat_message(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::chat_message(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_chat_message(&mut self, v: ChatMessage) {
        self.data = ::std::option::Option::Some(DataInner::chat_message(v))
    }

    // Mutable pointer to the field.
    pub fn mut_chat_message(&mut self) -> &mut ChatMessage {
        if let ::std::option::Option::Some(DataInner::chat_message(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::chat_message(ChatMessage::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::chat_message(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_chat_message(&mut self) -> ChatMessage {
        if self.has_chat_message() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::chat_message(v)) => v,
                _ => panic!(),
            }
        } else {
            ChatMessage::new()
        }
    }

    pub fn get_chat_message(&self) -> &ChatMessage {
        match self.data {
            ::std::option::Option::Some(DataInner::chat_message(ref v)) => v,
            _ => ChatMessage::default_instance(),
        }
    }

    // optional .LoginDetails login_details = 10;

    pub fn clear_login_details(&mut self) {
        self.data = ::std::option::Option::None;
    }

    pub fn has_login_details(&self) -> bool {
        match self.data {
            ::std::option::Option::Some(DataInner::login_details(..)) => true,
            _ => false,
        }
    }

    // Param is passed by value, moved
    pub fn set_login_details(&mut self, v: LoginDetails) {
        self.data = ::std::option::Option::Some(DataInner::login_details(v))
    }

    // Mutable pointer to the field.
    pub fn mut_login_details(&mut self) -> &mut LoginDetails {
        if let ::std::option::Option::Some(DataInner::login_details(_)) = self.data {
        } else {
            self.data = ::std::option::Option::Some(DataInner::login_details(LoginDetails::new()));
        }
        match self.data {
            ::std::option::Option::Some(DataInner::login_details(ref mut v)) => v,
            _ => panic!(),
        }
    }

    // Take field
    pub fn take_login_details(&mut self) -> LoginDetails {
        if self.has_login_details() {
            match self.data.take() {
                ::std::option::Option::Some(DataInner::login_details(v)) => v,
                _ => panic!(),
            }
        } else {
            LoginDetails::new()
        }
    }

    pub fn get_login_details(&self) -> &LoginDetails {
        match self.data {
            ::std::option::Option::Some(DataInner::login_details(ref v)) => v,
            _ => LoginDetails::default_instance(),
        }
    }
}

impl ::protobuf::Message for Data {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::hunt(is.read_message()?));
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::user(is.read_message()?));
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::team(is.read_message()?));
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::point(is.read_message()?));
                },
                7 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::bonus(is.read_message()?));
                },
                8 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::chat(is.read_message()?));
                },
                9 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::chat_message(is.read_message()?));
                },
                10 => {
                    if wire_type != ::protobuf::wire_format::WireTypeLengthDelimited {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    self.data = ::std::option::Option::Some(DataInner::login_details(is.read_message()?));
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let ::std::option::Option::Some(ref v) = self.data {
            match v {
                &DataInner::hunt(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::user(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::team(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::point(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::bonus(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::chat(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::chat_message(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
                &DataInner::login_details(ref v) => {
                    let len = v.compute_size();
                    my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
                },
            };
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let ::std::option::Option::Some(ref v) = self.data {
            match v {
                &DataInner::hunt(ref v) => {
                    os.write_tag(2, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::user(ref v) => {
                    os.write_tag(3, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::team(ref v) => {
                    os.write_tag(4, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::point(ref v) => {
                    os.write_tag(5, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::bonus(ref v) => {
                    os.write_tag(7, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::chat(ref v) => {
                    os.write_tag(8, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::chat_message(ref v) => {
                    os.write_tag(9, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
                &DataInner::login_details(ref v) => {
                    os.write_tag(10, ::protobuf::wire_format::WireTypeLengthDelimited)?;
                    os.write_raw_varint32(v.get_cached_size())?;
                    v.write_to_with_cached_sizes(os)?;
                },
            };
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Data {
    fn new() -> Data {
        Data::new()
    }

    fn descriptor_static(_: ::std::option::Option<Data>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Hunt>(
                    "hunt",
                    Data::has_hunt,
                    Data::get_hunt,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, User>(
                    "user",
                    Data::has_user,
                    Data::get_user,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Team>(
                    "team",
                    Data::has_team,
                    Data::get_team,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Point>(
                    "point",
                    Data::has_point,
                    Data::get_point,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Bonus>(
                    "bonus",
                    Data::has_bonus,
                    Data::get_bonus,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, Chat>(
                    "chat",
                    Data::has_chat,
                    Data::get_chat,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, ChatMessage>(
                    "chat_message",
                    Data::has_chat_message,
                    Data::get_chat_message,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_message_accessor::<_, LoginDetails>(
                    "login_details",
                    Data::has_login_details,
                    Data::get_login_details,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Data>(
                    "Data",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Data {
    fn clear(&mut self) {
        self.clear_hunt();
        self.clear_user();
        self.clear_team();
        self.clear_point();
        self.clear_bonus();
        self.clear_chat();
        self.clear_chat_message();
        self.clear_login_details();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Data {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Data {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Location {
    // message fields
    latitude: ::std::option::Option<f64>,
    longitude: ::std::option::Option<f64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Location {}

impl Location {
    pub fn new() -> Location {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Location {
        static mut instance: ::protobuf::lazy::Lazy<Location> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Location,
        };
        unsafe {
            instance.get(Location::new)
        }
    }

    // required double latitude = 1;

    pub fn clear_latitude(&mut self) {
        self.latitude = ::std::option::Option::None;
    }

    pub fn has_latitude(&self) -> bool {
        self.latitude.is_some()
    }

    // Param is passed by value, moved
    pub fn set_latitude(&mut self, v: f64) {
        self.latitude = ::std::option::Option::Some(v);
    }

    pub fn get_latitude(&self) -> f64 {
        self.latitude.unwrap_or(0.)
    }

    fn get_latitude_for_reflect(&self) -> &::std::option::Option<f64> {
        &self.latitude
    }

    fn mut_latitude_for_reflect(&mut self) -> &mut ::std::option::Option<f64> {
        &mut self.latitude
    }

    // required double longitude = 2;

    pub fn clear_longitude(&mut self) {
        self.longitude = ::std::option::Option::None;
    }

    pub fn has_longitude(&self) -> bool {
        self.longitude.is_some()
    }

    // Param is passed by value, moved
    pub fn set_longitude(&mut self, v: f64) {
        self.longitude = ::std::option::Option::Some(v);
    }

    pub fn get_longitude(&self) -> f64 {
        self.longitude.unwrap_or(0.)
    }

    fn get_longitude_for_reflect(&self) -> &::std::option::Option<f64> {
        &self.longitude
    }

    fn mut_longitude_for_reflect(&mut self) -> &mut ::std::option::Option<f64> {
        &mut self.longitude
    }
}

impl ::protobuf::Message for Location {
    fn is_initialized(&self) -> bool {
        if self.latitude.is_none() {
            return false;
        };
        if self.longitude.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeFixed64 {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_double()?;
                    self.latitude = ::std::option::Option::Some(tmp);
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeFixed64 {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_double()?;
                    self.longitude = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.latitude {
            my_size += 9;
        };
        if let Some(v) = self.longitude {
            my_size += 9;
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.latitude {
            os.write_double(1, v)?;
        };
        if let Some(v) = self.longitude {
            os.write_double(2, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Location {
    fn new() -> Location {
        Location::new()
    }

    fn descriptor_static(_: ::std::option::Option<Location>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeDouble>(
                    "latitude",
                    Location::get_latitude_for_reflect,
                    Location::mut_latitude_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeDouble>(
                    "longitude",
                    Location::get_longitude_for_reflect,
                    Location::mut_longitude_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Location>(
                    "Location",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Location {
    fn clear(&mut self) {
        self.clear_latitude();
        self.clear_longitude();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Location {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Location {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct User {
    // message fields
    id: ::std::option::Option<u32>,
    team_id: ::std::option::Option<u32>,
    name: ::protobuf::SingularField<::std::string::String>,
    permission: ::std::option::Option<Permission>,
    location: ::protobuf::SingularPtrField<Location>,
    status: ::std::option::Option<PlayerStatus>,
    join_hunt: ::std::option::Option<u32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for User {}

impl User {
    pub fn new() -> User {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static User {
        static mut instance: ::protobuf::lazy::Lazy<User> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const User,
        };
        unsafe {
            instance.get(User::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // optional uint32 team_id = 2;

    pub fn clear_team_id(&mut self) {
        self.team_id = ::std::option::Option::None;
    }

    pub fn has_team_id(&self) -> bool {
        self.team_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_team_id(&mut self, v: u32) {
        self.team_id = ::std::option::Option::Some(v);
    }

    pub fn get_team_id(&self) -> u32 {
        self.team_id.unwrap_or(0)
    }

    fn get_team_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.team_id
    }

    fn mut_team_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.team_id
    }

    // optional string name = 3;

    pub fn clear_name(&mut self) {
        self.name.clear();
    }

    pub fn has_name(&self) -> bool {
        self.name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_name(&mut self, v: ::std::string::String) {
        self.name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_name(&mut self) -> &mut ::std::string::String {
        if self.name.is_none() {
            self.name.set_default();
        };
        self.name.as_mut().unwrap()
    }

    // Take field
    pub fn take_name(&mut self) -> ::std::string::String {
        self.name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_name(&self) -> &str {
        match self.name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.name
    }

    fn mut_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.name
    }

    // optional .Permission permission = 4;

    pub fn clear_permission(&mut self) {
        self.permission = ::std::option::Option::None;
    }

    pub fn has_permission(&self) -> bool {
        self.permission.is_some()
    }

    // Param is passed by value, moved
    pub fn set_permission(&mut self, v: Permission) {
        self.permission = ::std::option::Option::Some(v);
    }

    pub fn get_permission(&self) -> Permission {
        self.permission.unwrap_or(Permission::PLAYER)
    }

    fn get_permission_for_reflect(&self) -> &::std::option::Option<Permission> {
        &self.permission
    }

    fn mut_permission_for_reflect(&mut self) -> &mut ::std::option::Option<Permission> {
        &mut self.permission
    }

    // optional .Location location = 5;

    pub fn clear_location(&mut self) {
        self.location.clear();
    }

    pub fn has_location(&self) -> bool {
        self.location.is_some()
    }

    // Param is passed by value, moved
    pub fn set_location(&mut self, v: Location) {
        self.location = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_location(&mut self) -> &mut Location {
        if self.location.is_none() {
            self.location.set_default();
        };
        self.location.as_mut().unwrap()
    }

    // Take field
    pub fn take_location(&mut self) -> Location {
        self.location.take().unwrap_or_else(|| Location::new())
    }

    pub fn get_location(&self) -> &Location {
        self.location.as_ref().unwrap_or_else(|| Location::default_instance())
    }

    fn get_location_for_reflect(&self) -> &::protobuf::SingularPtrField<Location> {
        &self.location
    }

    fn mut_location_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<Location> {
        &mut self.location
    }

    // optional .PlayerStatus status = 6;

    pub fn clear_status(&mut self) {
        self.status = ::std::option::Option::None;
    }

    pub fn has_status(&self) -> bool {
        self.status.is_some()
    }

    // Param is passed by value, moved
    pub fn set_status(&mut self, v: PlayerStatus) {
        self.status = ::std::option::Option::Some(v);
    }

    pub fn get_status(&self) -> PlayerStatus {
        self.status.unwrap_or(PlayerStatus::OFFLINE)
    }

    fn get_status_for_reflect(&self) -> &::std::option::Option<PlayerStatus> {
        &self.status
    }

    fn mut_status_for_reflect(&mut self) -> &mut ::std::option::Option<PlayerStatus> {
        &mut self.status
    }

    // optional uint32 join_hunt = 7;

    pub fn clear_join_hunt(&mut self) {
        self.join_hunt = ::std::option::Option::None;
    }

    pub fn has_join_hunt(&self) -> bool {
        self.join_hunt.is_some()
    }

    // Param is passed by value, moved
    pub fn set_join_hunt(&mut self, v: u32) {
        self.join_hunt = ::std::option::Option::Some(v);
    }

    pub fn get_join_hunt(&self) -> u32 {
        self.join_hunt.unwrap_or(0)
    }

    fn get_join_hunt_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.join_hunt
    }

    fn mut_join_hunt_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.join_hunt
    }
}

impl ::protobuf::Message for User {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.team_id = ::std::option::Option::Some(tmp);
                },
                3 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.name)?;
                },
                4 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_enum()?;
                    self.permission = ::std::option::Option::Some(tmp);
                },
                5 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.location)?;
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_enum()?;
                    self.status = ::std::option::Option::Some(tmp);
                },
                7 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.join_hunt = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.team_id {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.name.as_ref() {
            my_size += ::protobuf::rt::string_size(3, &v);
        };
        if let Some(v) = self.permission {
            my_size += ::protobuf::rt::enum_size(4, v);
        };
        if let Some(v) = self.location.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        if let Some(v) = self.status {
            my_size += ::protobuf::rt::enum_size(6, v);
        };
        if let Some(v) = self.join_hunt {
            my_size += ::protobuf::rt::value_size(7, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.team_id {
            os.write_uint32(2, v)?;
        };
        if let Some(v) = self.name.as_ref() {
            os.write_string(3, &v)?;
        };
        if let Some(v) = self.permission {
            os.write_enum(4, v.value())?;
        };
        if let Some(v) = self.location.as_ref() {
            os.write_tag(5, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        if let Some(v) = self.status {
            os.write_enum(6, v.value())?;
        };
        if let Some(v) = self.join_hunt {
            os.write_uint32(7, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for User {
    fn new() -> User {
        User::new()
    }

    fn descriptor_static(_: ::std::option::Option<User>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    User::get_id_for_reflect,
                    User::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "team_id",
                    User::get_team_id_for_reflect,
                    User::mut_team_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "name",
                    User::get_name_for_reflect,
                    User::mut_name_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<Permission>>(
                    "permission",
                    User::get_permission_for_reflect,
                    User::mut_permission_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Location>>(
                    "location",
                    User::get_location_for_reflect,
                    User::mut_location_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeEnum<PlayerStatus>>(
                    "status",
                    User::get_status_for_reflect,
                    User::mut_status_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "join_hunt",
                    User::get_join_hunt_for_reflect,
                    User::mut_join_hunt_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<User>(
                    "User",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for User {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_team_id();
        self.clear_name();
        self.clear_permission();
        self.clear_location();
        self.clear_status();
        self.clear_join_hunt();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for User {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for User {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Hunt {
    // message fields
    id: ::std::option::Option<u32>,
    teams: ::std::vec::Vec<u32>,
    points: ::std::vec::Vec<u32>,
    bonuses: ::std::vec::Vec<u32>,
    chats: ::std::vec::Vec<u32>,
    demonstrators: ::std::vec::Vec<u32>,
    location: ::protobuf::SingularPtrField<Location>,
    radius: ::std::option::Option<f64>,
    total_points: ::std::option::Option<u32>,
    name: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Hunt {}

impl Hunt {
    pub fn new() -> Hunt {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Hunt {
        static mut instance: ::protobuf::lazy::Lazy<Hunt> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Hunt,
        };
        unsafe {
            instance.get(Hunt::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // repeated uint32 teams = 2;

    pub fn clear_teams(&mut self) {
        self.teams.clear();
    }

    // Param is passed by value, moved
    pub fn set_teams(&mut self, v: ::std::vec::Vec<u32>) {
        self.teams = v;
    }

    // Mutable pointer to the field.
    pub fn mut_teams(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.teams
    }

    // Take field
    pub fn take_teams(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.teams, ::std::vec::Vec::new())
    }

    pub fn get_teams(&self) -> &[u32] {
        &self.teams
    }

    fn get_teams_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.teams
    }

    fn mut_teams_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.teams
    }

    // repeated uint32 points = 8;

    pub fn clear_points(&mut self) {
        self.points.clear();
    }

    // Param is passed by value, moved
    pub fn set_points(&mut self, v: ::std::vec::Vec<u32>) {
        self.points = v;
    }

    // Mutable pointer to the field.
    pub fn mut_points(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.points
    }

    // Take field
    pub fn take_points(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.points, ::std::vec::Vec::new())
    }

    pub fn get_points(&self) -> &[u32] {
        &self.points
    }

    fn get_points_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.points
    }

    fn mut_points_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.points
    }

    // repeated uint32 bonuses = 4;

    pub fn clear_bonuses(&mut self) {
        self.bonuses.clear();
    }

    // Param is passed by value, moved
    pub fn set_bonuses(&mut self, v: ::std::vec::Vec<u32>) {
        self.bonuses = v;
    }

    // Mutable pointer to the field.
    pub fn mut_bonuses(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.bonuses
    }

    // Take field
    pub fn take_bonuses(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.bonuses, ::std::vec::Vec::new())
    }

    pub fn get_bonuses(&self) -> &[u32] {
        &self.bonuses
    }

    fn get_bonuses_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.bonuses
    }

    fn mut_bonuses_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.bonuses
    }

    // repeated uint32 chats = 7;

    pub fn clear_chats(&mut self) {
        self.chats.clear();
    }

    // Param is passed by value, moved
    pub fn set_chats(&mut self, v: ::std::vec::Vec<u32>) {
        self.chats = v;
    }

    // Mutable pointer to the field.
    pub fn mut_chats(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.chats
    }

    // Take field
    pub fn take_chats(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.chats, ::std::vec::Vec::new())
    }

    pub fn get_chats(&self) -> &[u32] {
        &self.chats
    }

    fn get_chats_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.chats
    }

    fn mut_chats_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.chats
    }

    // repeated uint32 demonstrators = 9;

    pub fn clear_demonstrators(&mut self) {
        self.demonstrators.clear();
    }

    // Param is passed by value, moved
    pub fn set_demonstrators(&mut self, v: ::std::vec::Vec<u32>) {
        self.demonstrators = v;
    }

    // Mutable pointer to the field.
    pub fn mut_demonstrators(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.demonstrators
    }

    // Take field
    pub fn take_demonstrators(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.demonstrators, ::std::vec::Vec::new())
    }

    pub fn get_demonstrators(&self) -> &[u32] {
        &self.demonstrators
    }

    fn get_demonstrators_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.demonstrators
    }

    fn mut_demonstrators_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.demonstrators
    }

    // required .Location location = 5;

    pub fn clear_location(&mut self) {
        self.location.clear();
    }

    pub fn has_location(&self) -> bool {
        self.location.is_some()
    }

    // Param is passed by value, moved
    pub fn set_location(&mut self, v: Location) {
        self.location = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_location(&mut self) -> &mut Location {
        if self.location.is_none() {
            self.location.set_default();
        };
        self.location.as_mut().unwrap()
    }

    // Take field
    pub fn take_location(&mut self) -> Location {
        self.location.take().unwrap_or_else(|| Location::new())
    }

    pub fn get_location(&self) -> &Location {
        self.location.as_ref().unwrap_or_else(|| Location::default_instance())
    }

    fn get_location_for_reflect(&self) -> &::protobuf::SingularPtrField<Location> {
        &self.location
    }

    fn mut_location_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<Location> {
        &mut self.location
    }

    // required double radius = 6;

    pub fn clear_radius(&mut self) {
        self.radius = ::std::option::Option::None;
    }

    pub fn has_radius(&self) -> bool {
        self.radius.is_some()
    }

    // Param is passed by value, moved
    pub fn set_radius(&mut self, v: f64) {
        self.radius = ::std::option::Option::Some(v);
    }

    pub fn get_radius(&self) -> f64 {
        self.radius.unwrap_or(0.)
    }

    fn get_radius_for_reflect(&self) -> &::std::option::Option<f64> {
        &self.radius
    }

    fn mut_radius_for_reflect(&mut self) -> &mut ::std::option::Option<f64> {
        &mut self.radius
    }

    // optional uint32 total_points = 10;

    pub fn clear_total_points(&mut self) {
        self.total_points = ::std::option::Option::None;
    }

    pub fn has_total_points(&self) -> bool {
        self.total_points.is_some()
    }

    // Param is passed by value, moved
    pub fn set_total_points(&mut self, v: u32) {
        self.total_points = ::std::option::Option::Some(v);
    }

    pub fn get_total_points(&self) -> u32 {
        self.total_points.unwrap_or(0)
    }

    fn get_total_points_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.total_points
    }

    fn mut_total_points_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.total_points
    }

    // optional string name = 11;

    pub fn clear_name(&mut self) {
        self.name.clear();
    }

    pub fn has_name(&self) -> bool {
        self.name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_name(&mut self, v: ::std::string::String) {
        self.name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_name(&mut self) -> &mut ::std::string::String {
        if self.name.is_none() {
            self.name.set_default();
        };
        self.name.as_mut().unwrap()
    }

    // Take field
    pub fn take_name(&mut self) -> ::std::string::String {
        self.name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_name(&self) -> &str {
        match self.name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.name
    }

    fn mut_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.name
    }
}

impl ::protobuf::Message for Hunt {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        if self.location.is_none() {
            return false;
        };
        if self.radius.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.teams)?;
                },
                8 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.points)?;
                },
                4 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.bonuses)?;
                },
                7 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.chats)?;
                },
                9 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.demonstrators)?;
                },
                5 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.location)?;
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeFixed64 {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_double()?;
                    self.radius = ::std::option::Option::Some(tmp);
                },
                10 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.total_points = ::std::option::Option::Some(tmp);
                },
                11 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.name)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.teams {
            my_size += ::protobuf::rt::value_size(2, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.points {
            my_size += ::protobuf::rt::value_size(8, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.bonuses {
            my_size += ::protobuf::rt::value_size(4, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.chats {
            my_size += ::protobuf::rt::value_size(7, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.demonstrators {
            my_size += ::protobuf::rt::value_size(9, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.location.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        if let Some(v) = self.radius {
            my_size += 9;
        };
        if let Some(v) = self.total_points {
            my_size += ::protobuf::rt::value_size(10, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.name.as_ref() {
            my_size += ::protobuf::rt::string_size(11, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        for v in &self.teams {
            os.write_uint32(2, *v)?;
        };
        for v in &self.points {
            os.write_uint32(8, *v)?;
        };
        for v in &self.bonuses {
            os.write_uint32(4, *v)?;
        };
        for v in &self.chats {
            os.write_uint32(7, *v)?;
        };
        for v in &self.demonstrators {
            os.write_uint32(9, *v)?;
        };
        if let Some(v) = self.location.as_ref() {
            os.write_tag(5, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        if let Some(v) = self.radius {
            os.write_double(6, v)?;
        };
        if let Some(v) = self.total_points {
            os.write_uint32(10, v)?;
        };
        if let Some(v) = self.name.as_ref() {
            os.write_string(11, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Hunt {
    fn new() -> Hunt {
        Hunt::new()
    }

    fn descriptor_static(_: ::std::option::Option<Hunt>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Hunt::get_id_for_reflect,
                    Hunt::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "teams",
                    Hunt::get_teams_for_reflect,
                    Hunt::mut_teams_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "points",
                    Hunt::get_points_for_reflect,
                    Hunt::mut_points_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "bonuses",
                    Hunt::get_bonuses_for_reflect,
                    Hunt::mut_bonuses_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "chats",
                    Hunt::get_chats_for_reflect,
                    Hunt::mut_chats_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "demonstrators",
                    Hunt::get_demonstrators_for_reflect,
                    Hunt::mut_demonstrators_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Location>>(
                    "location",
                    Hunt::get_location_for_reflect,
                    Hunt::mut_location_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeDouble>(
                    "radius",
                    Hunt::get_radius_for_reflect,
                    Hunt::mut_radius_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "total_points",
                    Hunt::get_total_points_for_reflect,
                    Hunt::mut_total_points_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "name",
                    Hunt::get_name_for_reflect,
                    Hunt::mut_name_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Hunt>(
                    "Hunt",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Hunt {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_teams();
        self.clear_points();
        self.clear_bonuses();
        self.clear_chats();
        self.clear_demonstrators();
        self.clear_location();
        self.clear_radius();
        self.clear_total_points();
        self.clear_name();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Hunt {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Hunt {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Team {
    // message fields
    id: ::std::option::Option<u32>,
    name: ::protobuf::SingularField<::std::string::String>,
    score: ::std::option::Option<u32>,
    hunt: ::std::option::Option<u32>,
    players: ::std::vec::Vec<u32>,
    points: ::protobuf::RepeatedField<Team_Point>,
    bonuses: ::std::vec::Vec<u32>,
    points_found: ::std::option::Option<u32>,
    bonuses_found: ::std::option::Option<u32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Team {}

impl Team {
    pub fn new() -> Team {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Team {
        static mut instance: ::protobuf::lazy::Lazy<Team> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Team,
        };
        unsafe {
            instance.get(Team::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // required string name = 2;

    pub fn clear_name(&mut self) {
        self.name.clear();
    }

    pub fn has_name(&self) -> bool {
        self.name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_name(&mut self, v: ::std::string::String) {
        self.name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_name(&mut self) -> &mut ::std::string::String {
        if self.name.is_none() {
            self.name.set_default();
        };
        self.name.as_mut().unwrap()
    }

    // Take field
    pub fn take_name(&mut self) -> ::std::string::String {
        self.name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_name(&self) -> &str {
        match self.name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.name
    }

    fn mut_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.name
    }

    // optional uint32 score = 5;

    pub fn clear_score(&mut self) {
        self.score = ::std::option::Option::None;
    }

    pub fn has_score(&self) -> bool {
        self.score.is_some()
    }

    // Param is passed by value, moved
    pub fn set_score(&mut self, v: u32) {
        self.score = ::std::option::Option::Some(v);
    }

    pub fn get_score(&self) -> u32 {
        self.score.unwrap_or(0)
    }

    fn get_score_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.score
    }

    fn mut_score_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.score
    }

    // required uint32 hunt = 6;

    pub fn clear_hunt(&mut self) {
        self.hunt = ::std::option::Option::None;
    }

    pub fn has_hunt(&self) -> bool {
        self.hunt.is_some()
    }

    // Param is passed by value, moved
    pub fn set_hunt(&mut self, v: u32) {
        self.hunt = ::std::option::Option::Some(v);
    }

    pub fn get_hunt(&self) -> u32 {
        self.hunt.unwrap_or(0)
    }

    fn get_hunt_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.hunt
    }

    fn mut_hunt_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.hunt
    }

    // repeated uint32 players = 3;

    pub fn clear_players(&mut self) {
        self.players.clear();
    }

    // Param is passed by value, moved
    pub fn set_players(&mut self, v: ::std::vec::Vec<u32>) {
        self.players = v;
    }

    // Mutable pointer to the field.
    pub fn mut_players(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.players
    }

    // Take field
    pub fn take_players(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.players, ::std::vec::Vec::new())
    }

    pub fn get_players(&self) -> &[u32] {
        &self.players
    }

    fn get_players_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.players
    }

    fn mut_players_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.players
    }

    // repeated .Team.Point points = 4;

    pub fn clear_points(&mut self) {
        self.points.clear();
    }

    // Param is passed by value, moved
    pub fn set_points(&mut self, v: ::protobuf::RepeatedField<Team_Point>) {
        self.points = v;
    }

    // Mutable pointer to the field.
    pub fn mut_points(&mut self) -> &mut ::protobuf::RepeatedField<Team_Point> {
        &mut self.points
    }

    // Take field
    pub fn take_points(&mut self) -> ::protobuf::RepeatedField<Team_Point> {
        ::std::mem::replace(&mut self.points, ::protobuf::RepeatedField::new())
    }

    pub fn get_points(&self) -> &[Team_Point] {
        &self.points
    }

    fn get_points_for_reflect(&self) -> &::protobuf::RepeatedField<Team_Point> {
        &self.points
    }

    fn mut_points_for_reflect(&mut self) -> &mut ::protobuf::RepeatedField<Team_Point> {
        &mut self.points
    }

    // repeated uint32 bonuses = 7;

    pub fn clear_bonuses(&mut self) {
        self.bonuses.clear();
    }

    // Param is passed by value, moved
    pub fn set_bonuses(&mut self, v: ::std::vec::Vec<u32>) {
        self.bonuses = v;
    }

    // Mutable pointer to the field.
    pub fn mut_bonuses(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.bonuses
    }

    // Take field
    pub fn take_bonuses(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.bonuses, ::std::vec::Vec::new())
    }

    pub fn get_bonuses(&self) -> &[u32] {
        &self.bonuses
    }

    fn get_bonuses_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.bonuses
    }

    fn mut_bonuses_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.bonuses
    }

    // optional uint32 points_found = 8;

    pub fn clear_points_found(&mut self) {
        self.points_found = ::std::option::Option::None;
    }

    pub fn has_points_found(&self) -> bool {
        self.points_found.is_some()
    }

    // Param is passed by value, moved
    pub fn set_points_found(&mut self, v: u32) {
        self.points_found = ::std::option::Option::Some(v);
    }

    pub fn get_points_found(&self) -> u32 {
        self.points_found.unwrap_or(0)
    }

    fn get_points_found_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.points_found
    }

    fn mut_points_found_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.points_found
    }

    // optional uint32 bonuses_found = 9;

    pub fn clear_bonuses_found(&mut self) {
        self.bonuses_found = ::std::option::Option::None;
    }

    pub fn has_bonuses_found(&self) -> bool {
        self.bonuses_found.is_some()
    }

    // Param is passed by value, moved
    pub fn set_bonuses_found(&mut self, v: u32) {
        self.bonuses_found = ::std::option::Option::Some(v);
    }

    pub fn get_bonuses_found(&self) -> u32 {
        self.bonuses_found.unwrap_or(0)
    }

    fn get_bonuses_found_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.bonuses_found
    }

    fn mut_bonuses_found_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.bonuses_found
    }
}

impl ::protobuf::Message for Team {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        if self.name.is_none() {
            return false;
        };
        if self.hunt.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.name)?;
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.score = ::std::option::Option::Some(tmp);
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.hunt = ::std::option::Option::Some(tmp);
                },
                3 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.players)?;
                },
                4 => {
                    ::protobuf::rt::read_repeated_message_into(wire_type, is, &mut self.points)?;
                },
                7 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.bonuses)?;
                },
                8 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.points_found = ::std::option::Option::Some(tmp);
                },
                9 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.bonuses_found = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.name.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        if let Some(v) = self.score {
            my_size += ::protobuf::rt::value_size(5, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.hunt {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.players {
            my_size += ::protobuf::rt::value_size(3, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.points {
            let len = value.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        for value in &self.bonuses {
            my_size += ::protobuf::rt::value_size(7, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.points_found {
            my_size += ::protobuf::rt::value_size(8, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.bonuses_found {
            my_size += ::protobuf::rt::value_size(9, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.name.as_ref() {
            os.write_string(2, &v)?;
        };
        if let Some(v) = self.score {
            os.write_uint32(5, v)?;
        };
        if let Some(v) = self.hunt {
            os.write_uint32(6, v)?;
        };
        for v in &self.players {
            os.write_uint32(3, *v)?;
        };
        for v in &self.points {
            os.write_tag(4, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        for v in &self.bonuses {
            os.write_uint32(7, *v)?;
        };
        if let Some(v) = self.points_found {
            os.write_uint32(8, v)?;
        };
        if let Some(v) = self.bonuses_found {
            os.write_uint32(9, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Team {
    fn new() -> Team {
        Team::new()
    }

    fn descriptor_static(_: ::std::option::Option<Team>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Team::get_id_for_reflect,
                    Team::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "name",
                    Team::get_name_for_reflect,
                    Team::mut_name_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "score",
                    Team::get_score_for_reflect,
                    Team::mut_score_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "hunt",
                    Team::get_hunt_for_reflect,
                    Team::mut_hunt_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "players",
                    Team::get_players_for_reflect,
                    Team::mut_players_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Team_Point>>(
                    "points",
                    Team::get_points_for_reflect,
                    Team::mut_points_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "bonuses",
                    Team::get_bonuses_for_reflect,
                    Team::mut_bonuses_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "points_found",
                    Team::get_points_found_for_reflect,
                    Team::mut_points_found_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "bonuses_found",
                    Team::get_bonuses_found_for_reflect,
                    Team::mut_bonuses_found_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Team>(
                    "Team",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Team {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_name();
        self.clear_score();
        self.clear_hunt();
        self.clear_players();
        self.clear_points();
        self.clear_bonuses();
        self.clear_points_found();
        self.clear_bonuses_found();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Team {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Team {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Team_Point {
    // message fields
    id: ::std::option::Option<u32>,
    time_found: ::std::option::Option<u64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Team_Point {}

impl Team_Point {
    pub fn new() -> Team_Point {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Team_Point {
        static mut instance: ::protobuf::lazy::Lazy<Team_Point> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Team_Point,
        };
        unsafe {
            instance.get(Team_Point::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // optional uint64 time_found = 2;

    pub fn clear_time_found(&mut self) {
        self.time_found = ::std::option::Option::None;
    }

    pub fn has_time_found(&self) -> bool {
        self.time_found.is_some()
    }

    // Param is passed by value, moved
    pub fn set_time_found(&mut self, v: u64) {
        self.time_found = ::std::option::Option::Some(v);
    }

    pub fn get_time_found(&self) -> u64 {
        self.time_found.unwrap_or(0)
    }

    fn get_time_found_for_reflect(&self) -> &::std::option::Option<u64> {
        &self.time_found
    }

    fn mut_time_found_for_reflect(&mut self) -> &mut ::std::option::Option<u64> {
        &mut self.time_found
    }
}

impl ::protobuf::Message for Team_Point {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint64()?;
                    self.time_found = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.time_found {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.time_found {
            os.write_uint64(2, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Team_Point {
    fn new() -> Team_Point {
        Team_Point::new()
    }

    fn descriptor_static(_: ::std::option::Option<Team_Point>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Team_Point::get_id_for_reflect,
                    Team_Point::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "time_found",
                    Team_Point::get_time_found_for_reflect,
                    Team_Point::mut_time_found_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Team_Point>(
                    "Team_Point",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Team_Point {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_time_found();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Team_Point {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Team_Point {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Point {
    // message fields
    id: ::std::option::Option<u32>,
    clue: ::protobuf::SingularField<::std::string::String>,
    password: ::protobuf::SingularField<::std::string::String>,
    location: ::protobuf::SingularPtrField<Location>,
    difficulty: ::std::option::Option<u32>,
    join_hunt: ::std::option::Option<u32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Point {}

impl Point {
    pub fn new() -> Point {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Point {
        static mut instance: ::protobuf::lazy::Lazy<Point> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Point,
        };
        unsafe {
            instance.get(Point::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // required string clue = 2;

    pub fn clear_clue(&mut self) {
        self.clue.clear();
    }

    pub fn has_clue(&self) -> bool {
        self.clue.is_some()
    }

    // Param is passed by value, moved
    pub fn set_clue(&mut self, v: ::std::string::String) {
        self.clue = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_clue(&mut self) -> &mut ::std::string::String {
        if self.clue.is_none() {
            self.clue.set_default();
        };
        self.clue.as_mut().unwrap()
    }

    // Take field
    pub fn take_clue(&mut self) -> ::std::string::String {
        self.clue.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_clue(&self) -> &str {
        match self.clue.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_clue_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.clue
    }

    fn mut_clue_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.clue
    }

    // optional string password = 5;

    pub fn clear_password(&mut self) {
        self.password.clear();
    }

    pub fn has_password(&self) -> bool {
        self.password.is_some()
    }

    // Param is passed by value, moved
    pub fn set_password(&mut self, v: ::std::string::String) {
        self.password = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_password(&mut self) -> &mut ::std::string::String {
        if self.password.is_none() {
            self.password.set_default();
        };
        self.password.as_mut().unwrap()
    }

    // Take field
    pub fn take_password(&mut self) -> ::std::string::String {
        self.password.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_password(&self) -> &str {
        match self.password.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_password_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.password
    }

    fn mut_password_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.password
    }

    // optional .Location location = 3;

    pub fn clear_location(&mut self) {
        self.location.clear();
    }

    pub fn has_location(&self) -> bool {
        self.location.is_some()
    }

    // Param is passed by value, moved
    pub fn set_location(&mut self, v: Location) {
        self.location = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_location(&mut self) -> &mut Location {
        if self.location.is_none() {
            self.location.set_default();
        };
        self.location.as_mut().unwrap()
    }

    // Take field
    pub fn take_location(&mut self) -> Location {
        self.location.take().unwrap_or_else(|| Location::new())
    }

    pub fn get_location(&self) -> &Location {
        self.location.as_ref().unwrap_or_else(|| Location::default_instance())
    }

    fn get_location_for_reflect(&self) -> &::protobuf::SingularPtrField<Location> {
        &self.location
    }

    fn mut_location_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<Location> {
        &mut self.location
    }

    // optional uint32 difficulty = 6;

    pub fn clear_difficulty(&mut self) {
        self.difficulty = ::std::option::Option::None;
    }

    pub fn has_difficulty(&self) -> bool {
        self.difficulty.is_some()
    }

    // Param is passed by value, moved
    pub fn set_difficulty(&mut self, v: u32) {
        self.difficulty = ::std::option::Option::Some(v);
    }

    pub fn get_difficulty(&self) -> u32 {
        self.difficulty.unwrap_or(0)
    }

    fn get_difficulty_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.difficulty
    }

    fn mut_difficulty_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.difficulty
    }

    // optional uint32 join_hunt = 7;

    pub fn clear_join_hunt(&mut self) {
        self.join_hunt = ::std::option::Option::None;
    }

    pub fn has_join_hunt(&self) -> bool {
        self.join_hunt.is_some()
    }

    // Param is passed by value, moved
    pub fn set_join_hunt(&mut self, v: u32) {
        self.join_hunt = ::std::option::Option::Some(v);
    }

    pub fn get_join_hunt(&self) -> u32 {
        self.join_hunt.unwrap_or(0)
    }

    fn get_join_hunt_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.join_hunt
    }

    fn mut_join_hunt_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.join_hunt
    }
}

impl ::protobuf::Message for Point {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        if self.clue.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.clue)?;
                },
                5 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.password)?;
                },
                3 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.location)?;
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.difficulty = ::std::option::Option::Some(tmp);
                },
                7 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.join_hunt = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.clue.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        if let Some(v) = self.password.as_ref() {
            my_size += ::protobuf::rt::string_size(5, &v);
        };
        if let Some(v) = self.location.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        if let Some(v) = self.difficulty {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.join_hunt {
            my_size += ::protobuf::rt::value_size(7, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.clue.as_ref() {
            os.write_string(2, &v)?;
        };
        if let Some(v) = self.password.as_ref() {
            os.write_string(5, &v)?;
        };
        if let Some(v) = self.location.as_ref() {
            os.write_tag(3, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        if let Some(v) = self.difficulty {
            os.write_uint32(6, v)?;
        };
        if let Some(v) = self.join_hunt {
            os.write_uint32(7, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Point {
    fn new() -> Point {
        Point::new()
    }

    fn descriptor_static(_: ::std::option::Option<Point>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Point::get_id_for_reflect,
                    Point::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "clue",
                    Point::get_clue_for_reflect,
                    Point::mut_clue_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "password",
                    Point::get_password_for_reflect,
                    Point::mut_password_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Location>>(
                    "location",
                    Point::get_location_for_reflect,
                    Point::mut_location_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "difficulty",
                    Point::get_difficulty_for_reflect,
                    Point::mut_difficulty_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "join_hunt",
                    Point::get_join_hunt_for_reflect,
                    Point::mut_join_hunt_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Point>(
                    "Point",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Point {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_clue();
        self.clear_password();
        self.clear_location();
        self.clear_difficulty();
        self.clear_join_hunt();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Point {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Point {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Bonus {
    // message fields
    id: ::std::option::Option<u32>,
    image_url: ::protobuf::SingularField<::std::string::String>,
    name: ::protobuf::SingularField<::std::string::String>,
    location: ::protobuf::SingularPtrField<Location>,
    difficulty: ::std::option::Option<u32>,
    password: ::protobuf::SingularField<::std::string::String>,
    join_hunt: ::std::option::Option<u32>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Bonus {}

impl Bonus {
    pub fn new() -> Bonus {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Bonus {
        static mut instance: ::protobuf::lazy::Lazy<Bonus> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Bonus,
        };
        unsafe {
            instance.get(Bonus::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // required string image_url = 2;

    pub fn clear_image_url(&mut self) {
        self.image_url.clear();
    }

    pub fn has_image_url(&self) -> bool {
        self.image_url.is_some()
    }

    // Param is passed by value, moved
    pub fn set_image_url(&mut self, v: ::std::string::String) {
        self.image_url = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_image_url(&mut self) -> &mut ::std::string::String {
        if self.image_url.is_none() {
            self.image_url.set_default();
        };
        self.image_url.as_mut().unwrap()
    }

    // Take field
    pub fn take_image_url(&mut self) -> ::std::string::String {
        self.image_url.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_image_url(&self) -> &str {
        match self.image_url.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_image_url_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.image_url
    }

    fn mut_image_url_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.image_url
    }

    // optional string name = 4;

    pub fn clear_name(&mut self) {
        self.name.clear();
    }

    pub fn has_name(&self) -> bool {
        self.name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_name(&mut self, v: ::std::string::String) {
        self.name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_name(&mut self) -> &mut ::std::string::String {
        if self.name.is_none() {
            self.name.set_default();
        };
        self.name.as_mut().unwrap()
    }

    // Take field
    pub fn take_name(&mut self) -> ::std::string::String {
        self.name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_name(&self) -> &str {
        match self.name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.name
    }

    fn mut_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.name
    }

    // optional .Location location = 3;

    pub fn clear_location(&mut self) {
        self.location.clear();
    }

    pub fn has_location(&self) -> bool {
        self.location.is_some()
    }

    // Param is passed by value, moved
    pub fn set_location(&mut self, v: Location) {
        self.location = ::protobuf::SingularPtrField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_location(&mut self) -> &mut Location {
        if self.location.is_none() {
            self.location.set_default();
        };
        self.location.as_mut().unwrap()
    }

    // Take field
    pub fn take_location(&mut self) -> Location {
        self.location.take().unwrap_or_else(|| Location::new())
    }

    pub fn get_location(&self) -> &Location {
        self.location.as_ref().unwrap_or_else(|| Location::default_instance())
    }

    fn get_location_for_reflect(&self) -> &::protobuf::SingularPtrField<Location> {
        &self.location
    }

    fn mut_location_for_reflect(&mut self) -> &mut ::protobuf::SingularPtrField<Location> {
        &mut self.location
    }

    // optional uint32 difficulty = 5;

    pub fn clear_difficulty(&mut self) {
        self.difficulty = ::std::option::Option::None;
    }

    pub fn has_difficulty(&self) -> bool {
        self.difficulty.is_some()
    }

    // Param is passed by value, moved
    pub fn set_difficulty(&mut self, v: u32) {
        self.difficulty = ::std::option::Option::Some(v);
    }

    pub fn get_difficulty(&self) -> u32 {
        self.difficulty.unwrap_or(0)
    }

    fn get_difficulty_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.difficulty
    }

    fn mut_difficulty_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.difficulty
    }

    // optional string password = 6;

    pub fn clear_password(&mut self) {
        self.password.clear();
    }

    pub fn has_password(&self) -> bool {
        self.password.is_some()
    }

    // Param is passed by value, moved
    pub fn set_password(&mut self, v: ::std::string::String) {
        self.password = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_password(&mut self) -> &mut ::std::string::String {
        if self.password.is_none() {
            self.password.set_default();
        };
        self.password.as_mut().unwrap()
    }

    // Take field
    pub fn take_password(&mut self) -> ::std::string::String {
        self.password.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_password(&self) -> &str {
        match self.password.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_password_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.password
    }

    fn mut_password_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.password
    }

    // optional uint32 join_hunt = 7;

    pub fn clear_join_hunt(&mut self) {
        self.join_hunt = ::std::option::Option::None;
    }

    pub fn has_join_hunt(&self) -> bool {
        self.join_hunt.is_some()
    }

    // Param is passed by value, moved
    pub fn set_join_hunt(&mut self, v: u32) {
        self.join_hunt = ::std::option::Option::Some(v);
    }

    pub fn get_join_hunt(&self) -> u32 {
        self.join_hunt.unwrap_or(0)
    }

    fn get_join_hunt_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.join_hunt
    }

    fn mut_join_hunt_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.join_hunt
    }
}

impl ::protobuf::Message for Bonus {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        if self.image_url.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.image_url)?;
                },
                4 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.name)?;
                },
                3 => {
                    ::protobuf::rt::read_singular_message_into(wire_type, is, &mut self.location)?;
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.difficulty = ::std::option::Option::Some(tmp);
                },
                6 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.password)?;
                },
                7 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.join_hunt = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.image_url.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        if let Some(v) = self.name.as_ref() {
            my_size += ::protobuf::rt::string_size(4, &v);
        };
        if let Some(v) = self.location.as_ref() {
            let len = v.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        if let Some(v) = self.difficulty {
            my_size += ::protobuf::rt::value_size(5, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.password.as_ref() {
            my_size += ::protobuf::rt::string_size(6, &v);
        };
        if let Some(v) = self.join_hunt {
            my_size += ::protobuf::rt::value_size(7, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.image_url.as_ref() {
            os.write_string(2, &v)?;
        };
        if let Some(v) = self.name.as_ref() {
            os.write_string(4, &v)?;
        };
        if let Some(v) = self.location.as_ref() {
            os.write_tag(3, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        if let Some(v) = self.difficulty {
            os.write_uint32(5, v)?;
        };
        if let Some(v) = self.password.as_ref() {
            os.write_string(6, &v)?;
        };
        if let Some(v) = self.join_hunt {
            os.write_uint32(7, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Bonus {
    fn new() -> Bonus {
        Bonus::new()
    }

    fn descriptor_static(_: ::std::option::Option<Bonus>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Bonus::get_id_for_reflect,
                    Bonus::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "image_url",
                    Bonus::get_image_url_for_reflect,
                    Bonus::mut_image_url_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "name",
                    Bonus::get_name_for_reflect,
                    Bonus::mut_name_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_ptr_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<Location>>(
                    "location",
                    Bonus::get_location_for_reflect,
                    Bonus::mut_location_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "difficulty",
                    Bonus::get_difficulty_for_reflect,
                    Bonus::mut_difficulty_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "password",
                    Bonus::get_password_for_reflect,
                    Bonus::mut_password_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "join_hunt",
                    Bonus::get_join_hunt_for_reflect,
                    Bonus::mut_join_hunt_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Bonus>(
                    "Bonus",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Bonus {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_image_url();
        self.clear_name();
        self.clear_location();
        self.clear_difficulty();
        self.clear_password();
        self.clear_join_hunt();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Bonus {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Bonus {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Chat {
    // message fields
    id: ::std::option::Option<u32>,
    group_name: ::protobuf::SingularField<::std::string::String>,
    participants: ::std::vec::Vec<u32>,
    messages: ::std::vec::Vec<u32>,
    users_typing: ::std::vec::Vec<u32>,
    is_broadcast: ::std::option::Option<bool>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Chat {}

impl Chat {
    pub fn new() -> Chat {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Chat {
        static mut instance: ::protobuf::lazy::Lazy<Chat> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Chat,
        };
        unsafe {
            instance.get(Chat::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // required string group_name = 6;

    pub fn clear_group_name(&mut self) {
        self.group_name.clear();
    }

    pub fn has_group_name(&self) -> bool {
        self.group_name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_group_name(&mut self, v: ::std::string::String) {
        self.group_name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_group_name(&mut self) -> &mut ::std::string::String {
        if self.group_name.is_none() {
            self.group_name.set_default();
        };
        self.group_name.as_mut().unwrap()
    }

    // Take field
    pub fn take_group_name(&mut self) -> ::std::string::String {
        self.group_name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_group_name(&self) -> &str {
        match self.group_name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_group_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.group_name
    }

    fn mut_group_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.group_name
    }

    // repeated uint32 participants = 2;

    pub fn clear_participants(&mut self) {
        self.participants.clear();
    }

    // Param is passed by value, moved
    pub fn set_participants(&mut self, v: ::std::vec::Vec<u32>) {
        self.participants = v;
    }

    // Mutable pointer to the field.
    pub fn mut_participants(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.participants
    }

    // Take field
    pub fn take_participants(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.participants, ::std::vec::Vec::new())
    }

    pub fn get_participants(&self) -> &[u32] {
        &self.participants
    }

    fn get_participants_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.participants
    }

    fn mut_participants_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.participants
    }

    // repeated uint32 messages = 3;

    pub fn clear_messages(&mut self) {
        self.messages.clear();
    }

    // Param is passed by value, moved
    pub fn set_messages(&mut self, v: ::std::vec::Vec<u32>) {
        self.messages = v;
    }

    // Mutable pointer to the field.
    pub fn mut_messages(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.messages
    }

    // Take field
    pub fn take_messages(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.messages, ::std::vec::Vec::new())
    }

    pub fn get_messages(&self) -> &[u32] {
        &self.messages
    }

    fn get_messages_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.messages
    }

    fn mut_messages_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.messages
    }

    // repeated uint32 users_typing = 4;

    pub fn clear_users_typing(&mut self) {
        self.users_typing.clear();
    }

    // Param is passed by value, moved
    pub fn set_users_typing(&mut self, v: ::std::vec::Vec<u32>) {
        self.users_typing = v;
    }

    // Mutable pointer to the field.
    pub fn mut_users_typing(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.users_typing
    }

    // Take field
    pub fn take_users_typing(&mut self) -> ::std::vec::Vec<u32> {
        ::std::mem::replace(&mut self.users_typing, ::std::vec::Vec::new())
    }

    pub fn get_users_typing(&self) -> &[u32] {
        &self.users_typing
    }

    fn get_users_typing_for_reflect(&self) -> &::std::vec::Vec<u32> {
        &self.users_typing
    }

    fn mut_users_typing_for_reflect(&mut self) -> &mut ::std::vec::Vec<u32> {
        &mut self.users_typing
    }

    // required bool is_broadcast = 5;

    pub fn clear_is_broadcast(&mut self) {
        self.is_broadcast = ::std::option::Option::None;
    }

    pub fn has_is_broadcast(&self) -> bool {
        self.is_broadcast.is_some()
    }

    // Param is passed by value, moved
    pub fn set_is_broadcast(&mut self, v: bool) {
        self.is_broadcast = ::std::option::Option::Some(v);
    }

    pub fn get_is_broadcast(&self) -> bool {
        self.is_broadcast.unwrap_or(false)
    }

    fn get_is_broadcast_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.is_broadcast
    }

    fn mut_is_broadcast_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.is_broadcast
    }
}

impl ::protobuf::Message for Chat {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        if self.group_name.is_none() {
            return false;
        };
        if self.is_broadcast.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                6 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.group_name)?;
                },
                2 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.participants)?;
                },
                3 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.messages)?;
                },
                4 => {
                    ::protobuf::rt::read_repeated_uint32_into(wire_type, is, &mut self.users_typing)?;
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_bool()?;
                    self.is_broadcast = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.group_name.as_ref() {
            my_size += ::protobuf::rt::string_size(6, &v);
        };
        for value in &self.participants {
            my_size += ::protobuf::rt::value_size(2, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.messages {
            my_size += ::protobuf::rt::value_size(3, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.users_typing {
            my_size += ::protobuf::rt::value_size(4, *value, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.is_broadcast {
            my_size += 2;
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.group_name.as_ref() {
            os.write_string(6, &v)?;
        };
        for v in &self.participants {
            os.write_uint32(2, *v)?;
        };
        for v in &self.messages {
            os.write_uint32(3, *v)?;
        };
        for v in &self.users_typing {
            os.write_uint32(4, *v)?;
        };
        if let Some(v) = self.is_broadcast {
            os.write_bool(5, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Chat {
    fn new() -> Chat {
        Chat::new()
    }

    fn descriptor_static(_: ::std::option::Option<Chat>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    Chat::get_id_for_reflect,
                    Chat::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "group_name",
                    Chat::get_group_name_for_reflect,
                    Chat::mut_group_name_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "participants",
                    Chat::get_participants_for_reflect,
                    Chat::mut_participants_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "messages",
                    Chat::get_messages_for_reflect,
                    Chat::mut_messages_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_vec_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "users_typing",
                    Chat::get_users_typing_for_reflect,
                    Chat::mut_users_typing_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "is_broadcast",
                    Chat::get_is_broadcast_for_reflect,
                    Chat::mut_is_broadcast_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Chat>(
                    "Chat",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Chat {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_group_name();
        self.clear_participants();
        self.clear_messages();
        self.clear_users_typing();
        self.clear_is_broadcast();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Chat {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Chat {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ChatMessage {
    // message fields
    id: ::std::option::Option<u32>,
    chat_id: ::std::option::Option<u32>,
    sender_id: ::std::option::Option<u32>,
    text: ::protobuf::SingularField<::std::string::String>,
    time_sent: ::std::option::Option<u64>,
    time_edited: ::std::option::Option<u64>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ChatMessage {}

impl ChatMessage {
    pub fn new() -> ChatMessage {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ChatMessage {
        static mut instance: ::protobuf::lazy::Lazy<ChatMessage> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ChatMessage,
        };
        unsafe {
            instance.get(ChatMessage::new)
        }
    }

    // required uint32 id = 1;

    pub fn clear_id(&mut self) {
        self.id = ::std::option::Option::None;
    }

    pub fn has_id(&self) -> bool {
        self.id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_id(&mut self, v: u32) {
        self.id = ::std::option::Option::Some(v);
    }

    pub fn get_id(&self) -> u32 {
        self.id.unwrap_or(0)
    }

    fn get_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.id
    }

    fn mut_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.id
    }

    // required uint32 chat_id = 2;

    pub fn clear_chat_id(&mut self) {
        self.chat_id = ::std::option::Option::None;
    }

    pub fn has_chat_id(&self) -> bool {
        self.chat_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_chat_id(&mut self, v: u32) {
        self.chat_id = ::std::option::Option::Some(v);
    }

    pub fn get_chat_id(&self) -> u32 {
        self.chat_id.unwrap_or(0)
    }

    fn get_chat_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.chat_id
    }

    fn mut_chat_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.chat_id
    }

    // required uint32 sender_id = 3;

    pub fn clear_sender_id(&mut self) {
        self.sender_id = ::std::option::Option::None;
    }

    pub fn has_sender_id(&self) -> bool {
        self.sender_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_sender_id(&mut self, v: u32) {
        self.sender_id = ::std::option::Option::Some(v);
    }

    pub fn get_sender_id(&self) -> u32 {
        self.sender_id.unwrap_or(0)
    }

    fn get_sender_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.sender_id
    }

    fn mut_sender_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.sender_id
    }

    // required string text = 4;

    pub fn clear_text(&mut self) {
        self.text.clear();
    }

    pub fn has_text(&self) -> bool {
        self.text.is_some()
    }

    // Param is passed by value, moved
    pub fn set_text(&mut self, v: ::std::string::String) {
        self.text = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_text(&mut self) -> &mut ::std::string::String {
        if self.text.is_none() {
            self.text.set_default();
        };
        self.text.as_mut().unwrap()
    }

    // Take field
    pub fn take_text(&mut self) -> ::std::string::String {
        self.text.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_text(&self) -> &str {
        match self.text.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_text_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.text
    }

    fn mut_text_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.text
    }

    // required uint64 time_sent = 5;

    pub fn clear_time_sent(&mut self) {
        self.time_sent = ::std::option::Option::None;
    }

    pub fn has_time_sent(&self) -> bool {
        self.time_sent.is_some()
    }

    // Param is passed by value, moved
    pub fn set_time_sent(&mut self, v: u64) {
        self.time_sent = ::std::option::Option::Some(v);
    }

    pub fn get_time_sent(&self) -> u64 {
        self.time_sent.unwrap_or(0)
    }

    fn get_time_sent_for_reflect(&self) -> &::std::option::Option<u64> {
        &self.time_sent
    }

    fn mut_time_sent_for_reflect(&mut self) -> &mut ::std::option::Option<u64> {
        &mut self.time_sent
    }

    // optional uint64 time_edited = 6;

    pub fn clear_time_edited(&mut self) {
        self.time_edited = ::std::option::Option::None;
    }

    pub fn has_time_edited(&self) -> bool {
        self.time_edited.is_some()
    }

    // Param is passed by value, moved
    pub fn set_time_edited(&mut self, v: u64) {
        self.time_edited = ::std::option::Option::Some(v);
    }

    pub fn get_time_edited(&self) -> u64 {
        self.time_edited.unwrap_or(0)
    }

    fn get_time_edited_for_reflect(&self) -> &::std::option::Option<u64> {
        &self.time_edited
    }

    fn mut_time_edited_for_reflect(&mut self) -> &mut ::std::option::Option<u64> {
        &mut self.time_edited
    }
}

impl ::protobuf::Message for ChatMessage {
    fn is_initialized(&self) -> bool {
        if self.id.is_none() {
            return false;
        };
        if self.chat_id.is_none() {
            return false;
        };
        if self.sender_id.is_none() {
            return false;
        };
        if self.text.is_none() {
            return false;
        };
        if self.time_sent.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.chat_id = ::std::option::Option::Some(tmp);
                },
                3 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.sender_id = ::std::option::Option::Some(tmp);
                },
                4 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.text)?;
                },
                5 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint64()?;
                    self.time_sent = ::std::option::Option::Some(tmp);
                },
                6 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint64()?;
                    self.time_edited = ::std::option::Option::Some(tmp);
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.chat_id {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.sender_id {
            my_size += ::protobuf::rt::value_size(3, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.text.as_ref() {
            my_size += ::protobuf::rt::string_size(4, &v);
        };
        if let Some(v) = self.time_sent {
            my_size += ::protobuf::rt::value_size(5, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.time_edited {
            my_size += ::protobuf::rt::value_size(6, v, ::protobuf::wire_format::WireTypeVarint);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.chat_id {
            os.write_uint32(2, v)?;
        };
        if let Some(v) = self.sender_id {
            os.write_uint32(3, v)?;
        };
        if let Some(v) = self.text.as_ref() {
            os.write_string(4, &v)?;
        };
        if let Some(v) = self.time_sent {
            os.write_uint64(5, v)?;
        };
        if let Some(v) = self.time_edited {
            os.write_uint64(6, v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ChatMessage {
    fn new() -> ChatMessage {
        ChatMessage::new()
    }

    fn descriptor_static(_: ::std::option::Option<ChatMessage>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "id",
                    ChatMessage::get_id_for_reflect,
                    ChatMessage::mut_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "chat_id",
                    ChatMessage::get_chat_id_for_reflect,
                    ChatMessage::mut_chat_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "sender_id",
                    ChatMessage::get_sender_id_for_reflect,
                    ChatMessage::mut_sender_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "text",
                    ChatMessage::get_text_for_reflect,
                    ChatMessage::mut_text_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "time_sent",
                    ChatMessage::get_time_sent_for_reflect,
                    ChatMessage::mut_time_sent_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint64>(
                    "time_edited",
                    ChatMessage::get_time_edited_for_reflect,
                    ChatMessage::mut_time_edited_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ChatMessage>(
                    "ChatMessage",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ChatMessage {
    fn clear(&mut self) {
        self.clear_id();
        self.clear_chat_id();
        self.clear_sender_id();
        self.clear_text();
        self.clear_time_sent();
        self.clear_time_edited();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ChatMessage {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ChatMessage {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct LoginDetails {
    // message fields
    user_id: ::std::option::Option<u32>,
    username: ::protobuf::SingularField<::std::string::String>,
    password: ::protobuf::SingularField<::std::string::String>,
    name: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for LoginDetails {}

impl LoginDetails {
    pub fn new() -> LoginDetails {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static LoginDetails {
        static mut instance: ::protobuf::lazy::Lazy<LoginDetails> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const LoginDetails,
        };
        unsafe {
            instance.get(LoginDetails::new)
        }
    }

    // required uint32 user_id = 1;

    pub fn clear_user_id(&mut self) {
        self.user_id = ::std::option::Option::None;
    }

    pub fn has_user_id(&self) -> bool {
        self.user_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_user_id(&mut self, v: u32) {
        self.user_id = ::std::option::Option::Some(v);
    }

    pub fn get_user_id(&self) -> u32 {
        self.user_id.unwrap_or(0)
    }

    fn get_user_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.user_id
    }

    fn mut_user_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.user_id
    }

    // required string username = 2;

    pub fn clear_username(&mut self) {
        self.username.clear();
    }

    pub fn has_username(&self) -> bool {
        self.username.is_some()
    }

    // Param is passed by value, moved
    pub fn set_username(&mut self, v: ::std::string::String) {
        self.username = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_username(&mut self) -> &mut ::std::string::String {
        if self.username.is_none() {
            self.username.set_default();
        };
        self.username.as_mut().unwrap()
    }

    // Take field
    pub fn take_username(&mut self) -> ::std::string::String {
        self.username.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_username(&self) -> &str {
        match self.username.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_username_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.username
    }

    fn mut_username_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.username
    }

    // optional string password = 3;

    pub fn clear_password(&mut self) {
        self.password.clear();
    }

    pub fn has_password(&self) -> bool {
        self.password.is_some()
    }

    // Param is passed by value, moved
    pub fn set_password(&mut self, v: ::std::string::String) {
        self.password = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_password(&mut self) -> &mut ::std::string::String {
        if self.password.is_none() {
            self.password.set_default();
        };
        self.password.as_mut().unwrap()
    }

    // Take field
    pub fn take_password(&mut self) -> ::std::string::String {
        self.password.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_password(&self) -> &str {
        match self.password.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_password_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.password
    }

    fn mut_password_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.password
    }

    // optional string name = 4;

    pub fn clear_name(&mut self) {
        self.name.clear();
    }

    pub fn has_name(&self) -> bool {
        self.name.is_some()
    }

    // Param is passed by value, moved
    pub fn set_name(&mut self, v: ::std::string::String) {
        self.name = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_name(&mut self) -> &mut ::std::string::String {
        if self.name.is_none() {
            self.name.set_default();
        };
        self.name.as_mut().unwrap()
    }

    // Take field
    pub fn take_name(&mut self) -> ::std::string::String {
        self.name.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_name(&self) -> &str {
        match self.name.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_name_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.name
    }

    fn mut_name_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.name
    }
}

impl ::protobuf::Message for LoginDetails {
    fn is_initialized(&self) -> bool {
        if self.user_id.is_none() {
            return false;
        };
        if self.username.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.user_id = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.username)?;
                },
                3 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.password)?;
                },
                4 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.name)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.user_id {
            my_size += ::protobuf::rt::value_size(1, v, ::protobuf::wire_format::WireTypeVarint);
        };
        if let Some(v) = self.username.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        if let Some(v) = self.password.as_ref() {
            my_size += ::protobuf::rt::string_size(3, &v);
        };
        if let Some(v) = self.name.as_ref() {
            my_size += ::protobuf::rt::string_size(4, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.user_id {
            os.write_uint32(1, v)?;
        };
        if let Some(v) = self.username.as_ref() {
            os.write_string(2, &v)?;
        };
        if let Some(v) = self.password.as_ref() {
            os.write_string(3, &v)?;
        };
        if let Some(v) = self.name.as_ref() {
            os.write_string(4, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for LoginDetails {
    fn new() -> LoginDetails {
        LoginDetails::new()
    }

    fn descriptor_static(_: ::std::option::Option<LoginDetails>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "user_id",
                    LoginDetails::get_user_id_for_reflect,
                    LoginDetails::mut_user_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "username",
                    LoginDetails::get_username_for_reflect,
                    LoginDetails::mut_username_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "password",
                    LoginDetails::get_password_for_reflect,
                    LoginDetails::mut_password_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "name",
                    LoginDetails::get_name_for_reflect,
                    LoginDetails::mut_name_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<LoginDetails>(
                    "LoginDetails",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for LoginDetails {
    fn clear(&mut self) {
        self.clear_user_id();
        self.clear_username();
        self.clear_password();
        self.clear_name();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for LoginDetails {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for LoginDetails {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct ReqResult {
    // message fields
    error: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for ReqResult {}

impl ReqResult {
    pub fn new() -> ReqResult {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static ReqResult {
        static mut instance: ::protobuf::lazy::Lazy<ReqResult> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ReqResult,
        };
        unsafe {
            instance.get(ReqResult::new)
        }
    }

    // optional string error = 1;

    pub fn clear_error(&mut self) {
        self.error.clear();
    }

    pub fn has_error(&self) -> bool {
        self.error.is_some()
    }

    // Param is passed by value, moved
    pub fn set_error(&mut self, v: ::std::string::String) {
        self.error = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_error(&mut self) -> &mut ::std::string::String {
        if self.error.is_none() {
            self.error.set_default();
        };
        self.error.as_mut().unwrap()
    }

    // Take field
    pub fn take_error(&mut self) -> ::std::string::String {
        self.error.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_error(&self) -> &str {
        match self.error.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_error_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.error
    }

    fn mut_error_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.error
    }
}

impl ::protobuf::Message for ReqResult {
    fn is_initialized(&self) -> bool {
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.error)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.error.as_ref() {
            my_size += ::protobuf::rt::string_size(1, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.error.as_ref() {
            os.write_string(1, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for ReqResult {
    fn new() -> ReqResult {
        ReqResult::new()
    }

    fn descriptor_static(_: ::std::option::Option<ReqResult>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "error",
                    ReqResult::get_error_for_reflect,
                    ReqResult::mut_error_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<ReqResult>(
                    "ReqResult",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for ReqResult {
    fn clear(&mut self) {
        self.clear_error();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for ReqResult {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for ReqResult {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct NewHunt {
    // message fields
    hunt_id: ::std::option::Option<u32>,
    passwords: ::protobuf::RepeatedField<LoginDetails>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for NewHunt {}

impl NewHunt {
    pub fn new() -> NewHunt {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static NewHunt {
        static mut instance: ::protobuf::lazy::Lazy<NewHunt> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const NewHunt,
        };
        unsafe {
            instance.get(NewHunt::new)
        }
    }

    // required uint32 hunt_id = 2;

    pub fn clear_hunt_id(&mut self) {
        self.hunt_id = ::std::option::Option::None;
    }

    pub fn has_hunt_id(&self) -> bool {
        self.hunt_id.is_some()
    }

    // Param is passed by value, moved
    pub fn set_hunt_id(&mut self, v: u32) {
        self.hunt_id = ::std::option::Option::Some(v);
    }

    pub fn get_hunt_id(&self) -> u32 {
        self.hunt_id.unwrap_or(0)
    }

    fn get_hunt_id_for_reflect(&self) -> &::std::option::Option<u32> {
        &self.hunt_id
    }

    fn mut_hunt_id_for_reflect(&mut self) -> &mut ::std::option::Option<u32> {
        &mut self.hunt_id
    }

    // repeated .LoginDetails passwords = 1;

    pub fn clear_passwords(&mut self) {
        self.passwords.clear();
    }

    // Param is passed by value, moved
    pub fn set_passwords(&mut self, v: ::protobuf::RepeatedField<LoginDetails>) {
        self.passwords = v;
    }

    // Mutable pointer to the field.
    pub fn mut_passwords(&mut self) -> &mut ::protobuf::RepeatedField<LoginDetails> {
        &mut self.passwords
    }

    // Take field
    pub fn take_passwords(&mut self) -> ::protobuf::RepeatedField<LoginDetails> {
        ::std::mem::replace(&mut self.passwords, ::protobuf::RepeatedField::new())
    }

    pub fn get_passwords(&self) -> &[LoginDetails] {
        &self.passwords
    }

    fn get_passwords_for_reflect(&self) -> &::protobuf::RepeatedField<LoginDetails> {
        &self.passwords
    }

    fn mut_passwords_for_reflect(&mut self) -> &mut ::protobuf::RepeatedField<LoginDetails> {
        &mut self.passwords
    }
}

impl ::protobuf::Message for NewHunt {
    fn is_initialized(&self) -> bool {
        if self.hunt_id.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                2 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_uint32()?;
                    self.hunt_id = ::std::option::Option::Some(tmp);
                },
                1 => {
                    ::protobuf::rt::read_repeated_message_into(wire_type, is, &mut self.passwords)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.hunt_id {
            my_size += ::protobuf::rt::value_size(2, v, ::protobuf::wire_format::WireTypeVarint);
        };
        for value in &self.passwords {
            let len = value.compute_size();
            my_size += 1 + ::protobuf::rt::compute_raw_varint32_size(len) + len;
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.hunt_id {
            os.write_uint32(2, v)?;
        };
        for v in &self.passwords {
            os.write_tag(1, ::protobuf::wire_format::WireTypeLengthDelimited)?;
            os.write_raw_varint32(v.get_cached_size())?;
            v.write_to_with_cached_sizes(os)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for NewHunt {
    fn new() -> NewHunt {
        NewHunt::new()
    }

    fn descriptor_static(_: ::std::option::Option<NewHunt>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeUint32>(
                    "hunt_id",
                    NewHunt::get_hunt_id_for_reflect,
                    NewHunt::mut_hunt_id_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_repeated_field_accessor::<_, ::protobuf::types::ProtobufTypeMessage<LoginDetails>>(
                    "passwords",
                    NewHunt::get_passwords_for_reflect,
                    NewHunt::mut_passwords_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<NewHunt>(
                    "NewHunt",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for NewHunt {
    fn clear(&mut self) {
        self.clear_hunt_id();
        self.clear_passwords();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for NewHunt {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for NewHunt {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(PartialEq,Clone,Default)]
pub struct Reset {
    // message fields
    same_user: ::std::option::Option<bool>,
    reason: ::protobuf::SingularField<::std::string::String>,
    // special fields
    unknown_fields: ::protobuf::UnknownFields,
    cached_size: ::protobuf::CachedSize,
}

// see codegen.rs for the explanation why impl Sync explicitly
unsafe impl ::std::marker::Sync for Reset {}

impl Reset {
    pub fn new() -> Reset {
        ::std::default::Default::default()
    }

    pub fn default_instance() -> &'static Reset {
        static mut instance: ::protobuf::lazy::Lazy<Reset> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const Reset,
        };
        unsafe {
            instance.get(Reset::new)
        }
    }

    // required bool same_user = 1;

    pub fn clear_same_user(&mut self) {
        self.same_user = ::std::option::Option::None;
    }

    pub fn has_same_user(&self) -> bool {
        self.same_user.is_some()
    }

    // Param is passed by value, moved
    pub fn set_same_user(&mut self, v: bool) {
        self.same_user = ::std::option::Option::Some(v);
    }

    pub fn get_same_user(&self) -> bool {
        self.same_user.unwrap_or(false)
    }

    fn get_same_user_for_reflect(&self) -> &::std::option::Option<bool> {
        &self.same_user
    }

    fn mut_same_user_for_reflect(&mut self) -> &mut ::std::option::Option<bool> {
        &mut self.same_user
    }

    // required string reason = 2;

    pub fn clear_reason(&mut self) {
        self.reason.clear();
    }

    pub fn has_reason(&self) -> bool {
        self.reason.is_some()
    }

    // Param is passed by value, moved
    pub fn set_reason(&mut self, v: ::std::string::String) {
        self.reason = ::protobuf::SingularField::some(v);
    }

    // Mutable pointer to the field.
    // If field is not initialized, it is initialized with default value first.
    pub fn mut_reason(&mut self) -> &mut ::std::string::String {
        if self.reason.is_none() {
            self.reason.set_default();
        };
        self.reason.as_mut().unwrap()
    }

    // Take field
    pub fn take_reason(&mut self) -> ::std::string::String {
        self.reason.take().unwrap_or_else(|| ::std::string::String::new())
    }

    pub fn get_reason(&self) -> &str {
        match self.reason.as_ref() {
            Some(v) => &v,
            None => "",
        }
    }

    fn get_reason_for_reflect(&self) -> &::protobuf::SingularField<::std::string::String> {
        &self.reason
    }

    fn mut_reason_for_reflect(&mut self) -> &mut ::protobuf::SingularField<::std::string::String> {
        &mut self.reason
    }
}

impl ::protobuf::Message for Reset {
    fn is_initialized(&self) -> bool {
        if self.same_user.is_none() {
            return false;
        };
        if self.reason.is_none() {
            return false;
        };
        true
    }

    fn merge_from(&mut self, is: &mut ::protobuf::CodedInputStream) -> ::protobuf::ProtobufResult<()> {
        while !is.eof()? {
            let (field_number, wire_type) = is.read_tag_unpack()?;
            match field_number {
                1 => {
                    if wire_type != ::protobuf::wire_format::WireTypeVarint {
                        return ::std::result::Result::Err(::protobuf::rt::unexpected_wire_type(wire_type));
                    };
                    let tmp = is.read_bool()?;
                    self.same_user = ::std::option::Option::Some(tmp);
                },
                2 => {
                    ::protobuf::rt::read_singular_string_into(wire_type, is, &mut self.reason)?;
                },
                _ => {
                    ::protobuf::rt::read_unknown_or_skip_group(field_number, wire_type, is, self.mut_unknown_fields())?;
                },
            };
        }
        ::std::result::Result::Ok(())
    }

    // Compute sizes of nested messages
    #[allow(unused_variables)]
    fn compute_size(&self) -> u32 {
        let mut my_size = 0;
        if let Some(v) = self.same_user {
            my_size += 2;
        };
        if let Some(v) = self.reason.as_ref() {
            my_size += ::protobuf::rt::string_size(2, &v);
        };
        my_size += ::protobuf::rt::unknown_fields_size(self.get_unknown_fields());
        self.cached_size.set(my_size);
        my_size
    }

    fn write_to_with_cached_sizes(&self, os: &mut ::protobuf::CodedOutputStream) -> ::protobuf::ProtobufResult<()> {
        if let Some(v) = self.same_user {
            os.write_bool(1, v)?;
        };
        if let Some(v) = self.reason.as_ref() {
            os.write_string(2, &v)?;
        };
        os.write_unknown_fields(self.get_unknown_fields())?;
        ::std::result::Result::Ok(())
    }

    fn get_cached_size(&self) -> u32 {
        self.cached_size.get()
    }

    fn get_unknown_fields(&self) -> &::protobuf::UnknownFields {
        &self.unknown_fields
    }

    fn mut_unknown_fields(&mut self) -> &mut ::protobuf::UnknownFields {
        &mut self.unknown_fields
    }

    fn as_any(&self) -> &::std::any::Any {
        self as &::std::any::Any
    }
    fn as_any_mut(&mut self) -> &mut ::std::any::Any {
        self as &mut ::std::any::Any
    }
    fn into_any(self: Box<Self>) -> ::std::boxed::Box<::std::any::Any> {
        self
    }

    fn descriptor(&self) -> &'static ::protobuf::reflect::MessageDescriptor {
        ::protobuf::MessageStatic::descriptor_static(None::<Self>)
    }
}

impl ::protobuf::MessageStatic for Reset {
    fn new() -> Reset {
        Reset::new()
    }

    fn descriptor_static(_: ::std::option::Option<Reset>) -> &'static ::protobuf::reflect::MessageDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::MessageDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::MessageDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                let mut fields = ::std::vec::Vec::new();
                fields.push(::protobuf::reflect::accessor::make_option_accessor::<_, ::protobuf::types::ProtobufTypeBool>(
                    "same_user",
                    Reset::get_same_user_for_reflect,
                    Reset::mut_same_user_for_reflect,
                ));
                fields.push(::protobuf::reflect::accessor::make_singular_field_accessor::<_, ::protobuf::types::ProtobufTypeString>(
                    "reason",
                    Reset::get_reason_for_reflect,
                    Reset::mut_reason_for_reflect,
                ));
                ::protobuf::reflect::MessageDescriptor::new::<Reset>(
                    "Reset",
                    fields,
                    file_descriptor_proto()
                )
            })
        }
    }
}

impl ::protobuf::Clear for Reset {
    fn clear(&mut self) {
        self.clear_same_user();
        self.clear_reason();
        self.unknown_fields.clear();
    }
}

impl ::std::fmt::Debug for Reset {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::protobuf::text_format::fmt(self, f)
    }
}

impl ::protobuf::reflect::ProtobufValue for Reset {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Message(self)
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum NotificationIcon {
    GENERAL = 0,
}

impl ::protobuf::ProtobufEnum for NotificationIcon {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<NotificationIcon> {
        match value {
            0 => ::std::option::Option::Some(NotificationIcon::GENERAL),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [NotificationIcon] = &[
            NotificationIcon::GENERAL,
        ];
        values
    }

    fn enum_descriptor_static(_: Option<NotificationIcon>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("NotificationIcon", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for NotificationIcon {
}

impl ::protobuf::reflect::ProtobufValue for NotificationIcon {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum PlayerStatus {
    OFFLINE = 0,
    ASLEEP = 1,
    ACTIVE = 2,
}

impl ::protobuf::ProtobufEnum for PlayerStatus {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<PlayerStatus> {
        match value {
            0 => ::std::option::Option::Some(PlayerStatus::OFFLINE),
            1 => ::std::option::Option::Some(PlayerStatus::ASLEEP),
            2 => ::std::option::Option::Some(PlayerStatus::ACTIVE),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [PlayerStatus] = &[
            PlayerStatus::OFFLINE,
            PlayerStatus::ASLEEP,
            PlayerStatus::ACTIVE,
        ];
        values
    }

    fn enum_descriptor_static(_: Option<PlayerStatus>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("PlayerStatus", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for PlayerStatus {
}

impl ::protobuf::reflect::ProtobufValue for PlayerStatus {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum DataType {
    HUNT = 0,
    USER = 1,
    TEAM = 2,
    POINT = 3,
    BONUS = 4,
    CHAT = 6,
    CHAT_MESSAGE = 7,
    LOGIN_DETAILS = 8,
}

impl ::protobuf::ProtobufEnum for DataType {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<DataType> {
        match value {
            0 => ::std::option::Option::Some(DataType::HUNT),
            1 => ::std::option::Option::Some(DataType::USER),
            2 => ::std::option::Option::Some(DataType::TEAM),
            3 => ::std::option::Option::Some(DataType::POINT),
            4 => ::std::option::Option::Some(DataType::BONUS),
            6 => ::std::option::Option::Some(DataType::CHAT),
            7 => ::std::option::Option::Some(DataType::CHAT_MESSAGE),
            8 => ::std::option::Option::Some(DataType::LOGIN_DETAILS),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [DataType] = &[
            DataType::HUNT,
            DataType::USER,
            DataType::TEAM,
            DataType::POINT,
            DataType::BONUS,
            DataType::CHAT,
            DataType::CHAT_MESSAGE,
            DataType::LOGIN_DETAILS,
        ];
        values
    }

    fn enum_descriptor_static(_: Option<DataType>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("DataType", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for DataType {
}

impl ::protobuf::reflect::ProtobufValue for DataType {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

#[derive(Clone,PartialEq,Eq,Debug,Hash)]
pub enum Permission {
    PLAYER = 0,
    DEMONSTRATOR = 1,
    ADMIN = 2,
}

impl ::protobuf::ProtobufEnum for Permission {
    fn value(&self) -> i32 {
        *self as i32
    }

    fn from_i32(value: i32) -> ::std::option::Option<Permission> {
        match value {
            0 => ::std::option::Option::Some(Permission::PLAYER),
            1 => ::std::option::Option::Some(Permission::DEMONSTRATOR),
            2 => ::std::option::Option::Some(Permission::ADMIN),
            _ => ::std::option::Option::None
        }
    }

    fn values() -> &'static [Self] {
        static values: &'static [Permission] = &[
            Permission::PLAYER,
            Permission::DEMONSTRATOR,
            Permission::ADMIN,
        ];
        values
    }

    fn enum_descriptor_static(_: Option<Permission>) -> &'static ::protobuf::reflect::EnumDescriptor {
        static mut descriptor: ::protobuf::lazy::Lazy<::protobuf::reflect::EnumDescriptor> = ::protobuf::lazy::Lazy {
            lock: ::protobuf::lazy::ONCE_INIT,
            ptr: 0 as *const ::protobuf::reflect::EnumDescriptor,
        };
        unsafe {
            descriptor.get(|| {
                ::protobuf::reflect::EnumDescriptor::new("Permission", file_descriptor_proto())
            })
        }
    }
}

impl ::std::marker::Copy for Permission {
}

impl ::protobuf::reflect::ProtobufValue for Permission {
    fn as_ref(&self) -> ::protobuf::reflect::ProtobufValueRef {
        ::protobuf::reflect::ProtobufValueRef::Enum(self.descriptor())
    }
}

static file_descriptor_proto_data: &'static [u8] = &[
    0x0a, 0x0d, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
    0xf9, 0x04, 0x0a, 0x07, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x12, 0x0a, 0x03, 0x73,
    0x65, 0x71, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x48, 0x00, 0x52, 0x03, 0x73, 0x65, 0x71, 0x12,
    0x12, 0x0a, 0x03, 0x61, 0x63, 0x6b, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x48, 0x00, 0x52, 0x03,
    0x61, 0x63, 0x6b, 0x12, 0x1b, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18, 0x03, 0x20, 0x01, 0x28,
    0x0b, 0x32, 0x05, 0x2e, 0x44, 0x61, 0x74, 0x61, 0x48, 0x01, 0x52, 0x04, 0x64, 0x61, 0x74, 0x61,
    0x12, 0x24, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b,
    0x32, 0x0a, 0x2e, 0x52, 0x65, 0x71, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x48, 0x01, 0x52, 0x06,
    0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12, 0x21, 0x0a, 0x06, 0x75, 0x6e, 0x6c, 0x6f, 0x63, 0x6b,
    0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x07, 0x2e, 0x55, 0x6e, 0x6c, 0x6f, 0x63, 0x6b, 0x48,
    0x01, 0x52, 0x06, 0x75, 0x6e, 0x6c, 0x6f, 0x63, 0x6b, 0x12, 0x2a, 0x0a, 0x0b, 0x64, 0x61, 0x74,
    0x61, 0x5f, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x07,
    0x2e, 0x44, 0x61, 0x74, 0x61, 0x49, 0x44, 0x48, 0x01, 0x52, 0x0a, 0x64, 0x61, 0x74, 0x61, 0x44,
    0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x27, 0x0a, 0x06, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x79, 0x18,
    0x0a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61,
    0x74, 0x69, 0x6f, 0x6e, 0x48, 0x01, 0x52, 0x06, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x79, 0x12, 0x22,
    0x0a, 0x08, 0x64, 0x61, 0x74, 0x61, 0x5f, 0x61, 0x64, 0x64, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x0b,
    0x32, 0x05, 0x2e, 0x44, 0x61, 0x74, 0x61, 0x48, 0x01, 0x52, 0x07, 0x64, 0x61, 0x74, 0x61, 0x41,
    0x64, 0x64, 0x12, 0x1f, 0x0a, 0x0a, 0x68, 0x75, 0x6e, 0x74, 0x5f, 0x73, 0x74, 0x61, 0x72, 0x74,
    0x18, 0x0c, 0x20, 0x01, 0x28, 0x0d, 0x48, 0x01, 0x52, 0x09, 0x68, 0x75, 0x6e, 0x74, 0x53, 0x74,
    0x61, 0x72, 0x74, 0x12, 0x1d, 0x0a, 0x09, 0x6a, 0x73, 0x6f, 0x6e, 0x5f, 0x68, 0x75, 0x6e, 0x74,
    0x18, 0x0d, 0x20, 0x01, 0x28, 0x09, 0x48, 0x01, 0x52, 0x08, 0x6a, 0x73, 0x6f, 0x6e, 0x48, 0x75,
    0x6e, 0x74, 0x12, 0x31, 0x0a, 0x0e, 0x68, 0x75, 0x6e, 0x74, 0x5f, 0x70, 0x61, 0x73, 0x73, 0x77,
    0x6f, 0x72, 0x64, 0x73, 0x18, 0x10, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x08, 0x2e, 0x4e, 0x65, 0x77,
    0x48, 0x75, 0x6e, 0x74, 0x48, 0x01, 0x52, 0x0d, 0x68, 0x75, 0x6e, 0x74, 0x50, 0x61, 0x73, 0x73,
    0x77, 0x6f, 0x72, 0x64, 0x73, 0x12, 0x1e, 0x0a, 0x05, 0x72, 0x65, 0x73, 0x65, 0x74, 0x18, 0x11,
    0x20, 0x01, 0x28, 0x0b, 0x32, 0x06, 0x2e, 0x52, 0x65, 0x73, 0x65, 0x74, 0x48, 0x01, 0x52, 0x05,
    0x72, 0x65, 0x73, 0x65, 0x74, 0x12, 0x2d, 0x0a, 0x0d, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x5f, 0x72,
    0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x06, 0x2e, 0x4c,
    0x6f, 0x67, 0x69, 0x6e, 0x48, 0x01, 0x52, 0x0c, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x52, 0x65, 0x71,
    0x75, 0x65, 0x73, 0x74, 0x12, 0x31, 0x0a, 0x0e, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x5f, 0x63, 0x6f,
    0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x08, 0x2e, 0x53,
    0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x48, 0x01, 0x52, 0x0d, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x43,
    0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x22, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6d,
    0x65, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x08, 0x2e, 0x53, 0x65, 0x73, 0x73, 0x69, 0x6f,
    0x6e, 0x48, 0x01, 0x52, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x04, 0x70,
    0x69, 0x6e, 0x67, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x08, 0x48, 0x01, 0x52, 0x04, 0x70, 0x69, 0x6e,
    0x67, 0x12, 0x1f, 0x0a, 0x0b, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x5f, 0x73, 0x65, 0x71,
    0x18, 0x0f, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x0a, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x53,
    0x65, 0x71, 0x42, 0x0b, 0x0a, 0x09, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x42,
    0x0a, 0x0a, 0x08, 0x6d, 0x73, 0x67, 0x5f, 0x64, 0x61, 0x74, 0x61, 0x22, 0x41, 0x0a, 0x07, 0x53,
    0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69,
    0x64, 0x18, 0x01, 0x20, 0x02, 0x28, 0x0d, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12,
    0x1d, 0x0a, 0x0a, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20,
    0x02, 0x28, 0x09, 0x52, 0x09, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x22, 0x1c,
    0x0a, 0x06, 0x55, 0x6e, 0x6c, 0x6f, 0x63, 0x6b, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65,
    0x18, 0x01, 0x20, 0x02, 0x28, 0x09, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x22, 0x6f, 0x0a, 0x0c,
    0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x14, 0x0a, 0x05,
    0x74, 0x69, 0x74, 0x6c, 0x65, 0x18, 0x01, 0x20, 0x02, 0x28, 0x09, 0x52, 0x05, 0x74, 0x69, 0x74,
    0x6c, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x65, 0x78, 0x74, 0x18, 0x02, 0x20, 0x02, 0x28, 0x09,
    0x52, 0x04, 0x74, 0x65, 0x78, 0x74, 0x12, 0x25, 0x0a, 0x04, 0x69, 0x63, 0x6f, 0x6e, 0x18, 0x03,
    0x20, 0x02, 0x28, 0x0e, 0x32, 0x11, 0x2e, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74,
    0x69, 0x6f, 0x6e, 0x49, 0x63, 0x6f, 0x6e, 0x52, 0x04, 0x69, 0x63, 0x6f, 0x6e, 0x12, 0x0e, 0x0a,
    0x02, 0x69, 0x64, 0x18, 0x04, 0x20, 0x02, 0x28, 0x0d, 0x52, 0x02, 0x69, 0x64, 0x22, 0x3f, 0x0a,
    0x05, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x12, 0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61,
    0x6d, 0x65, 0x18, 0x01, 0x20, 0x02, 0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61,
    0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x02,
    0x20, 0x02, 0x28, 0x09, 0x52, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x22, 0x39,
    0x0a, 0x06, 0x44, 0x61, 0x74, 0x61, 0x49, 0x44, 0x12, 0x1d, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65,
    0x18, 0x01, 0x20, 0x02, 0x28, 0x0e, 0x32, 0x09, 0x2e, 0x44, 0x61, 0x74, 0x61, 0x54, 0x79, 0x70,
    0x65, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x69, 0x64, 0x73, 0x18, 0x02,
    0x20, 0x02, 0x28, 0x0d, 0x52, 0x03, 0x69, 0x64, 0x73, 0x22, 0xab, 0x02, 0x0a, 0x04, 0x44, 0x61,
    0x74, 0x61, 0x12, 0x1b, 0x0a, 0x04, 0x68, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
    0x32, 0x05, 0x2e, 0x48, 0x75, 0x6e, 0x74, 0x48, 0x00, 0x52, 0x04, 0x68, 0x75, 0x6e, 0x74, 0x12,
    0x1b, 0x0a, 0x04, 0x75, 0x73, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x05, 0x2e,
    0x55, 0x73, 0x65, 0x72, 0x48, 0x00, 0x52, 0x04, 0x75, 0x73, 0x65, 0x72, 0x12, 0x1b, 0x0a, 0x04,
    0x74, 0x65, 0x61, 0x6d, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x05, 0x2e, 0x54, 0x65, 0x61,
    0x6d, 0x48, 0x00, 0x52, 0x04, 0x74, 0x65, 0x61, 0x6d, 0x12, 0x1e, 0x0a, 0x05, 0x70, 0x6f, 0x69,
    0x6e, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x06, 0x2e, 0x50, 0x6f, 0x69, 0x6e, 0x74,
    0x48, 0x00, 0x52, 0x05, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x12, 0x1e, 0x0a, 0x05, 0x62, 0x6f, 0x6e,
    0x75, 0x73, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x06, 0x2e, 0x42, 0x6f, 0x6e, 0x75, 0x73,
    0x48, 0x00, 0x52, 0x05, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x12, 0x1b, 0x0a, 0x04, 0x63, 0x68, 0x61,
    0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x05, 0x2e, 0x43, 0x68, 0x61, 0x74, 0x48, 0x00,
    0x52, 0x04, 0x63, 0x68, 0x61, 0x74, 0x12, 0x31, 0x0a, 0x0c, 0x63, 0x68, 0x61, 0x74, 0x5f, 0x6d,
    0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0c, 0x2e, 0x43,
    0x68, 0x61, 0x74, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x48, 0x00, 0x52, 0x0b, 0x63, 0x68,
    0x61, 0x74, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x34, 0x0a, 0x0d, 0x6c, 0x6f, 0x67,
    0x69, 0x6e, 0x5f, 0x64, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x0b,
    0x32, 0x0d, 0x2e, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x48,
    0x00, 0x52, 0x0c, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x42,
    0x06, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x44, 0x0a, 0x08, 0x4c, 0x6f, 0x63, 0x61, 0x74,
    0x69, 0x6f, 0x6e, 0x12, 0x1a, 0x0a, 0x08, 0x6c, 0x61, 0x74, 0x69, 0x74, 0x75, 0x64, 0x65, 0x18,
    0x01, 0x20, 0x02, 0x28, 0x01, 0x52, 0x08, 0x6c, 0x61, 0x74, 0x69, 0x74, 0x75, 0x64, 0x65, 0x12,
    0x1c, 0x0a, 0x09, 0x6c, 0x6f, 0x6e, 0x67, 0x69, 0x74, 0x75, 0x64, 0x65, 0x18, 0x02, 0x20, 0x02,
    0x28, 0x01, 0x52, 0x09, 0x6c, 0x6f, 0x6e, 0x67, 0x69, 0x74, 0x75, 0x64, 0x65, 0x22, 0xdb, 0x01,
    0x0a, 0x04, 0x55, 0x73, 0x65, 0x72, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02,
    0x28, 0x0d, 0x52, 0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x74, 0x65, 0x61, 0x6d, 0x5f, 0x69,
    0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x06, 0x74, 0x65, 0x61, 0x6d, 0x49, 0x64, 0x12,
    0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e,
    0x61, 0x6d, 0x65, 0x12, 0x2b, 0x0a, 0x0a, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f,
    0x6e, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x0b, 0x2e, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x73,
    0x73, 0x69, 0x6f, 0x6e, 0x52, 0x0a, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e,
    0x12, 0x25, 0x0a, 0x08, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x05, 0x20, 0x01,
    0x28, 0x0b, 0x32, 0x09, 0x2e, 0x4c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x08, 0x6c,
    0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x25, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
    0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x0d, 0x2e, 0x50, 0x6c, 0x61, 0x79, 0x65, 0x72,
    0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1b,
    0x0a, 0x09, 0x6a, 0x6f, 0x69, 0x6e, 0x5f, 0x68, 0x75, 0x6e, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28,
    0x0d, 0x52, 0x08, 0x6a, 0x6f, 0x69, 0x6e, 0x48, 0x75, 0x6e, 0x74, 0x22, 0x90, 0x02, 0x0a, 0x04,
    0x48, 0x75, 0x6e, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02, 0x28, 0x0d,
    0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x65, 0x61, 0x6d, 0x73, 0x18, 0x02, 0x20,
    0x03, 0x28, 0x0d, 0x52, 0x05, 0x74, 0x65, 0x61, 0x6d, 0x73, 0x12, 0x16, 0x0a, 0x06, 0x70, 0x6f,
    0x69, 0x6e, 0x74, 0x73, 0x18, 0x08, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x06, 0x70, 0x6f, 0x69, 0x6e,
    0x74, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65, 0x73, 0x18, 0x04, 0x20,
    0x03, 0x28, 0x0d, 0x52, 0x07, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65, 0x73, 0x12, 0x14, 0x0a, 0x05,
    0x63, 0x68, 0x61, 0x74, 0x73, 0x18, 0x07, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x05, 0x63, 0x68, 0x61,
    0x74, 0x73, 0x12, 0x24, 0x0a, 0x0d, 0x64, 0x65, 0x6d, 0x6f, 0x6e, 0x73, 0x74, 0x72, 0x61, 0x74,
    0x6f, 0x72, 0x73, 0x18, 0x09, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x0d, 0x64, 0x65, 0x6d, 0x6f, 0x6e,
    0x73, 0x74, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x73, 0x12, 0x25, 0x0a, 0x08, 0x6c, 0x6f, 0x63, 0x61,
    0x74, 0x69, 0x6f, 0x6e, 0x18, 0x05, 0x20, 0x02, 0x28, 0x0b, 0x32, 0x09, 0x2e, 0x4c, 0x6f, 0x63,
    0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x08, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12,
    0x16, 0x0a, 0x06, 0x72, 0x61, 0x64, 0x69, 0x75, 0x73, 0x18, 0x06, 0x20, 0x02, 0x28, 0x01, 0x52,
    0x06, 0x72, 0x61, 0x64, 0x69, 0x75, 0x73, 0x12, 0x21, 0x0a, 0x0c, 0x74, 0x6f, 0x74, 0x61, 0x6c,
    0x5f, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x0b, 0x74,
    0x6f, 0x74, 0x61, 0x6c, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61,
    0x6d, 0x65, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x22, 0xad,
    0x02, 0x0a, 0x04, 0x54, 0x65, 0x61, 0x6d, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
    0x02, 0x28, 0x0d, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18,
    0x02, 0x20, 0x02, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x73,
    0x63, 0x6f, 0x72, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x05, 0x73, 0x63, 0x6f, 0x72,
    0x65, 0x12, 0x12, 0x0a, 0x04, 0x68, 0x75, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x02, 0x28, 0x0d, 0x52,
    0x04, 0x68, 0x75, 0x6e, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x73,
    0x18, 0x03, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x07, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x73, 0x12,
    0x23, 0x0a, 0x06, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x18, 0x04, 0x20, 0x03, 0x28, 0x0b, 0x32,
    0x0b, 0x2e, 0x54, 0x65, 0x61, 0x6d, 0x2e, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x52, 0x06, 0x70, 0x6f,
    0x69, 0x6e, 0x74, 0x73, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65, 0x73, 0x18,
    0x07, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x07, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65, 0x73, 0x12, 0x21,
    0x0a, 0x0c, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x5f, 0x66, 0x6f, 0x75, 0x6e, 0x64, 0x18, 0x08,
    0x20, 0x01, 0x28, 0x0d, 0x52, 0x0b, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x46, 0x6f, 0x75, 0x6e,
    0x64, 0x12, 0x23, 0x0a, 0x0d, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65, 0x73, 0x5f, 0x66, 0x6f, 0x75,
    0x6e, 0x64, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x0c, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65,
    0x73, 0x46, 0x6f, 0x75, 0x6e, 0x64, 0x1a, 0x36, 0x0a, 0x05, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x12,
    0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02, 0x28, 0x0d, 0x52, 0x02, 0x69, 0x64, 0x12,
    0x1d, 0x0a, 0x0a, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x66, 0x6f, 0x75, 0x6e, 0x64, 0x18, 0x02, 0x20,
    0x01, 0x28, 0x04, 0x52, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x46, 0x6f, 0x75, 0x6e, 0x64, 0x22, 0xab,
    0x01, 0x0a, 0x05, 0x50, 0x6f, 0x69, 0x6e, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
    0x20, 0x02, 0x28, 0x0d, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6c, 0x75, 0x65,
    0x18, 0x02, 0x20, 0x02, 0x28, 0x09, 0x52, 0x04, 0x63, 0x6c, 0x75, 0x65, 0x12, 0x1a, 0x0a, 0x08,
    0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
    0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x25, 0x0a, 0x08, 0x6c, 0x6f, 0x63, 0x61,
    0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x09, 0x2e, 0x4c, 0x6f, 0x63,
    0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x08, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12,
    0x1e, 0x0a, 0x0a, 0x64, 0x69, 0x66, 0x66, 0x69, 0x63, 0x75, 0x6c, 0x74, 0x79, 0x18, 0x06, 0x20,
    0x01, 0x28, 0x0d, 0x52, 0x0a, 0x64, 0x69, 0x66, 0x66, 0x69, 0x63, 0x75, 0x6c, 0x74, 0x79, 0x12,
    0x1b, 0x0a, 0x09, 0x6a, 0x6f, 0x69, 0x6e, 0x5f, 0x68, 0x75, 0x6e, 0x74, 0x18, 0x07, 0x20, 0x01,
    0x28, 0x0d, 0x52, 0x08, 0x6a, 0x6f, 0x69, 0x6e, 0x48, 0x75, 0x6e, 0x74, 0x22, 0xc8, 0x01, 0x0a,
    0x05, 0x42, 0x6f, 0x6e, 0x75, 0x73, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02,
    0x28, 0x0d, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x5f,
    0x75, 0x72, 0x6c, 0x18, 0x02, 0x20, 0x02, 0x28, 0x09, 0x52, 0x08, 0x69, 0x6d, 0x61, 0x67, 0x65,
    0x55, 0x72, 0x6c, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28,
    0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x25, 0x0a, 0x08, 0x6c, 0x6f, 0x63, 0x61, 0x74,
    0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x09, 0x2e, 0x4c, 0x6f, 0x63, 0x61,
    0x74, 0x69, 0x6f, 0x6e, 0x52, 0x08, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1e,
    0x0a, 0x0a, 0x64, 0x69, 0x66, 0x66, 0x69, 0x63, 0x75, 0x6c, 0x74, 0x79, 0x18, 0x05, 0x20, 0x01,
    0x28, 0x0d, 0x52, 0x0a, 0x64, 0x69, 0x66, 0x66, 0x69, 0x63, 0x75, 0x6c, 0x74, 0x79, 0x12, 0x1a,
    0x0a, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
    0x52, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x6a, 0x6f,
    0x69, 0x6e, 0x5f, 0x68, 0x75, 0x6e, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x08, 0x6a,
    0x6f, 0x69, 0x6e, 0x48, 0x75, 0x6e, 0x74, 0x22, 0xbb, 0x01, 0x0a, 0x04, 0x43, 0x68, 0x61, 0x74,
    0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02, 0x28, 0x0d, 0x52, 0x02, 0x69, 0x64,
    0x12, 0x1d, 0x0a, 0x0a, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x06,
    0x20, 0x02, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x4e, 0x61, 0x6d, 0x65, 0x12,
    0x22, 0x0a, 0x0c, 0x70, 0x61, 0x72, 0x74, 0x69, 0x63, 0x69, 0x70, 0x61, 0x6e, 0x74, 0x73, 0x18,
    0x02, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x0c, 0x70, 0x61, 0x72, 0x74, 0x69, 0x63, 0x69, 0x70, 0x61,
    0x6e, 0x74, 0x73, 0x12, 0x1a, 0x0a, 0x08, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x18,
    0x03, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x08, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x12,
    0x21, 0x0a, 0x0c, 0x75, 0x73, 0x65, 0x72, 0x73, 0x5f, 0x74, 0x79, 0x70, 0x69, 0x6e, 0x67, 0x18,
    0x04, 0x20, 0x03, 0x28, 0x0d, 0x52, 0x0b, 0x75, 0x73, 0x65, 0x72, 0x73, 0x54, 0x79, 0x70, 0x69,
    0x6e, 0x67, 0x12, 0x21, 0x0a, 0x0c, 0x69, 0x73, 0x5f, 0x62, 0x72, 0x6f, 0x61, 0x64, 0x63, 0x61,
    0x73, 0x74, 0x18, 0x05, 0x20, 0x02, 0x28, 0x08, 0x52, 0x0b, 0x69, 0x73, 0x42, 0x72, 0x6f, 0x61,
    0x64, 0x63, 0x61, 0x73, 0x74, 0x22, 0xa5, 0x01, 0x0a, 0x0b, 0x43, 0x68, 0x61, 0x74, 0x4d, 0x65,
    0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02, 0x28,
    0x0d, 0x52, 0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x63, 0x68, 0x61, 0x74, 0x5f, 0x69, 0x64,
    0x18, 0x02, 0x20, 0x02, 0x28, 0x0d, 0x52, 0x06, 0x63, 0x68, 0x61, 0x74, 0x49, 0x64, 0x12, 0x1b,
    0x0a, 0x09, 0x73, 0x65, 0x6e, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x02, 0x28,
    0x0d, 0x52, 0x08, 0x73, 0x65, 0x6e, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x74,
    0x65, 0x78, 0x74, 0x18, 0x04, 0x20, 0x02, 0x28, 0x09, 0x52, 0x04, 0x74, 0x65, 0x78, 0x74, 0x12,
    0x1b, 0x0a, 0x09, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x73, 0x65, 0x6e, 0x74, 0x18, 0x05, 0x20, 0x02,
    0x28, 0x04, 0x52, 0x08, 0x74, 0x69, 0x6d, 0x65, 0x53, 0x65, 0x6e, 0x74, 0x12, 0x1f, 0x0a, 0x0b,
    0x74, 0x69, 0x6d, 0x65, 0x5f, 0x65, 0x64, 0x69, 0x74, 0x65, 0x64, 0x18, 0x06, 0x20, 0x01, 0x28,
    0x04, 0x52, 0x0a, 0x74, 0x69, 0x6d, 0x65, 0x45, 0x64, 0x69, 0x74, 0x65, 0x64, 0x22, 0x73, 0x0a,
    0x0c, 0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x12, 0x17, 0x0a,
    0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x02, 0x28, 0x0d, 0x52, 0x06,
    0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61,
    0x6d, 0x65, 0x18, 0x02, 0x20, 0x02, 0x28, 0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61,
    0x6d, 0x65, 0x12, 0x1a, 0x0a, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x03,
    0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x12, 0x12,
    0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61,
    0x6d, 0x65, 0x22, 0x21, 0x0a, 0x09, 0x52, 0x65, 0x71, 0x52, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x12,
    0x14, 0x0a, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
    0x65, 0x72, 0x72, 0x6f, 0x72, 0x22, 0x4f, 0x0a, 0x07, 0x4e, 0x65, 0x77, 0x48, 0x75, 0x6e, 0x74,
    0x12, 0x17, 0x0a, 0x07, 0x68, 0x75, 0x6e, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x02, 0x28,
    0x0d, 0x52, 0x06, 0x68, 0x75, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x2b, 0x0a, 0x09, 0x70, 0x61, 0x73,
    0x73, 0x77, 0x6f, 0x72, 0x64, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x4c,
    0x6f, 0x67, 0x69, 0x6e, 0x44, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x52, 0x09, 0x70, 0x61, 0x73,
    0x73, 0x77, 0x6f, 0x72, 0x64, 0x73, 0x22, 0x3c, 0x0a, 0x05, 0x52, 0x65, 0x73, 0x65, 0x74, 0x12,
    0x1b, 0x0a, 0x09, 0x73, 0x61, 0x6d, 0x65, 0x5f, 0x75, 0x73, 0x65, 0x72, 0x18, 0x01, 0x20, 0x02,
    0x28, 0x08, 0x52, 0x08, 0x73, 0x61, 0x6d, 0x65, 0x55, 0x73, 0x65, 0x72, 0x12, 0x16, 0x0a, 0x06,
    0x72, 0x65, 0x61, 0x73, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x02, 0x28, 0x09, 0x52, 0x06, 0x72, 0x65,
    0x61, 0x73, 0x6f, 0x6e, 0x2a, 0x1f, 0x0a, 0x10, 0x4e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61,
    0x74, 0x69, 0x6f, 0x6e, 0x49, 0x63, 0x6f, 0x6e, 0x12, 0x0b, 0x0a, 0x07, 0x47, 0x45, 0x4e, 0x45,
    0x52, 0x41, 0x4c, 0x10, 0x00, 0x2a, 0x33, 0x0a, 0x0c, 0x50, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x53,
    0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x0b, 0x0a, 0x07, 0x4f, 0x46, 0x46, 0x4c, 0x49, 0x4e, 0x45,
    0x10, 0x00, 0x12, 0x0a, 0x0a, 0x06, 0x41, 0x53, 0x4c, 0x45, 0x45, 0x50, 0x10, 0x01, 0x12, 0x0a,
    0x0a, 0x06, 0x41, 0x43, 0x54, 0x49, 0x56, 0x45, 0x10, 0x02, 0x2a, 0x6d, 0x0a, 0x08, 0x44, 0x61,
    0x74, 0x61, 0x54, 0x79, 0x70, 0x65, 0x12, 0x08, 0x0a, 0x04, 0x48, 0x55, 0x4e, 0x54, 0x10, 0x00,
    0x12, 0x08, 0x0a, 0x04, 0x55, 0x53, 0x45, 0x52, 0x10, 0x01, 0x12, 0x08, 0x0a, 0x04, 0x54, 0x45,
    0x41, 0x4d, 0x10, 0x02, 0x12, 0x09, 0x0a, 0x05, 0x50, 0x4f, 0x49, 0x4e, 0x54, 0x10, 0x03, 0x12,
    0x09, 0x0a, 0x05, 0x42, 0x4f, 0x4e, 0x55, 0x53, 0x10, 0x04, 0x12, 0x08, 0x0a, 0x04, 0x43, 0x48,
    0x41, 0x54, 0x10, 0x06, 0x12, 0x10, 0x0a, 0x0c, 0x43, 0x48, 0x41, 0x54, 0x5f, 0x4d, 0x45, 0x53,
    0x53, 0x41, 0x47, 0x45, 0x10, 0x07, 0x12, 0x11, 0x0a, 0x0d, 0x4c, 0x4f, 0x47, 0x49, 0x4e, 0x5f,
    0x44, 0x45, 0x54, 0x41, 0x49, 0x4c, 0x53, 0x10, 0x08, 0x2a, 0x35, 0x0a, 0x0a, 0x50, 0x65, 0x72,
    0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x0a, 0x0a, 0x06, 0x50, 0x4c, 0x41, 0x59, 0x45,
    0x52, 0x10, 0x00, 0x12, 0x10, 0x0a, 0x0c, 0x44, 0x45, 0x4d, 0x4f, 0x4e, 0x53, 0x54, 0x52, 0x41,
    0x54, 0x4f, 0x52, 0x10, 0x01, 0x12, 0x09, 0x0a, 0x05, 0x41, 0x44, 0x4d, 0x49, 0x4e, 0x10, 0x02,
    0x42, 0x1f, 0x0a, 0x1b, 0x68, 0x75, 0x73, 0x74, 0x6c, 0x65, 0x72, 0x73, 0x2e, 0x68, 0x75, 0x6e,
    0x74, 0x64, 0x72, 0x6f, 0x69, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x6f, 0x6c, 0x50,
    0x01, 0x4a, 0x8f, 0x66, 0x0a, 0x07, 0x12, 0x05, 0x00, 0x00, 0xaa, 0x02, 0x01, 0x0a, 0x08, 0x0a,
    0x01, 0x0c, 0x12, 0x03, 0x00, 0x00, 0x12, 0x0a, 0x08, 0x0a, 0x01, 0x08, 0x12, 0x03, 0x02, 0x00,
    0x34, 0x0a, 0x0b, 0x0a, 0x04, 0x08, 0xe7, 0x07, 0x00, 0x12, 0x03, 0x02, 0x00, 0x34, 0x0a, 0x0c,
    0x0a, 0x05, 0x08, 0xe7, 0x07, 0x00, 0x02, 0x12, 0x03, 0x02, 0x07, 0x13, 0x0a, 0x0d, 0x0a, 0x06,
    0x08, 0xe7, 0x07, 0x00, 0x02, 0x00, 0x12, 0x03, 0x02, 0x07, 0x13, 0x0a, 0x0e, 0x0a, 0x07, 0x08,
    0xe7, 0x07, 0x00, 0x02, 0x00, 0x01, 0x12, 0x03, 0x02, 0x07, 0x13, 0x0a, 0x0c, 0x0a, 0x05, 0x08,
    0xe7, 0x07, 0x00, 0x07, 0x12, 0x03, 0x02, 0x16, 0x33, 0x0a, 0x08, 0x0a, 0x01, 0x08, 0x12, 0x03,
    0x03, 0x00, 0x22, 0x0a, 0x0b, 0x0a, 0x04, 0x08, 0xe7, 0x07, 0x01, 0x12, 0x03, 0x03, 0x00, 0x22,
    0x0a, 0x0c, 0x0a, 0x05, 0x08, 0xe7, 0x07, 0x01, 0x02, 0x12, 0x03, 0x03, 0x07, 0x1a, 0x0a, 0x0d,
    0x0a, 0x06, 0x08, 0xe7, 0x07, 0x01, 0x02, 0x00, 0x12, 0x03, 0x03, 0x07, 0x1a, 0x0a, 0x0e, 0x0a,
    0x07, 0x08, 0xe7, 0x07, 0x01, 0x02, 0x00, 0x01, 0x12, 0x03, 0x03, 0x07, 0x1a, 0x0a, 0x0c, 0x0a,
    0x05, 0x08, 0xe7, 0x07, 0x01, 0x03, 0x12, 0x03, 0x03, 0x1d, 0x21, 0x0a, 0x49, 0x0a, 0x02, 0x04,
    0x00, 0x12, 0x04, 0x06, 0x00, 0x3a, 0x01, 0x1a, 0x3d, 0x20, 0x54, 0x68, 0x65, 0x20, 0x64, 0x61,
    0x74, 0x61, 0x20, 0x73, 0x65, 0x6e, 0x74, 0x20, 0x62, 0x65, 0x74, 0x77, 0x65, 0x65, 0x6e, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x73,
    0x65, 0x72, 0x76, 0x65, 0x72, 0x20, 0x74, 0x6f, 0x20, 0x63, 0x6f, 0x6d, 0x6d, 0x75, 0x6e, 0x69,
    0x63, 0x61, 0x74, 0x65, 0x2e, 0x0a, 0x0a, 0x0a, 0x0a, 0x03, 0x04, 0x00, 0x01, 0x12, 0x03, 0x06,
    0x08, 0x0f, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x00, 0x08, 0x00, 0x12, 0x04, 0x08, 0x03, 0x0d, 0x04,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x08, 0x00, 0x01, 0x12, 0x03, 0x08, 0x09, 0x12, 0x0a, 0x4c,
    0x0a, 0x04, 0x04, 0x00, 0x02, 0x00, 0x12, 0x03, 0x0a, 0x06, 0x15, 0x1a, 0x3f, 0x20, 0x54, 0x68,
    0x69, 0x73, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x27, 0x73, 0x20, 0x73, 0x65, 0x71,
    0x75, 0x65, 0x6e, 0x63, 0x69, 0x6e, 0x67, 0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x2c, 0x20,
    0x75, 0x73, 0x65, 0x64, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6f, 0x6e,
    0x20, 0x72, 0x65, 0x73, 0x75, 0x6d, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x00, 0x02, 0x00, 0x05, 0x12, 0x03, 0x0a, 0x06, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00,
    0x02, 0x00, 0x01, 0x12, 0x03, 0x0a, 0x0d, 0x10, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x00,
    0x03, 0x12, 0x03, 0x0a, 0x13, 0x14, 0x0a, 0x3a, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x01, 0x12, 0x03,
    0x0c, 0x06, 0x15, 0x1a, 0x2d, 0x20, 0x41, 0x6e, 0x20, 0x61, 0x63, 0x6b, 0x6e, 0x6f, 0x77, 0x6c,
    0x65, 0x64, 0x67, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x72, 0x65, 0x63, 0x65,
    0x69, 0x70, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x61, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
    0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x01, 0x05, 0x12, 0x03, 0x0c, 0x06, 0x0c,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x01, 0x01, 0x12, 0x03, 0x0c, 0x0d, 0x10, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x00, 0x02, 0x01, 0x03, 0x12, 0x03, 0x0c, 0x13, 0x14, 0x0a, 0x77, 0x0a, 0x04,
    0x04, 0x00, 0x08, 0x01, 0x12, 0x04, 0x11, 0x03, 0x35, 0x04, 0x1a, 0x69, 0x20, 0x45, 0x61, 0x63,
    0x68, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x63, 0x61, 0x6e, 0x20, 0x62, 0x65,
    0x20, 0x6f, 0x6e, 0x65, 0x20, 0x6f, 0x66, 0x20, 0x61, 0x20, 0x66, 0x65, 0x77, 0x20, 0x74, 0x79,
    0x70, 0x65, 0x73, 0x20, 0x2d, 0x2d, 0x20, 0x6f, 0x6e, 0x6c, 0x79, 0x20, 0x6f, 0x6e, 0x65, 0x20,
    0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x66, 0x6f, 0x6c, 0x6c, 0x6f, 0x77, 0x69, 0x6e, 0x67,
    0x20, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x0a, 0x20, 0x63, 0x61, 0x6e, 0x20, 0x62, 0x65, 0x20,
    0x73, 0x65, 0x74, 0x20, 0x69, 0x6e, 0x20, 0x65, 0x61, 0x63, 0x68, 0x20, 0x6d, 0x65, 0x73, 0x73,
    0x61, 0x67, 0x65, 0x3a, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x08, 0x01, 0x01, 0x12, 0x03,
    0x11, 0x09, 0x11, 0x0a, 0x2a, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x02, 0x12, 0x03, 0x13, 0x06, 0x14,
    0x1a, 0x1d, 0x20, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x20, 0x6f, 0x72, 0x20, 0x75, 0x70, 0x64,
    0x61, 0x74, 0x65, 0x20, 0x73, 0x6f, 0x6d, 0x65, 0x20, 0x64, 0x61, 0x74, 0x61, 0x2e, 0x0a, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x02, 0x06, 0x12, 0x03, 0x13, 0x06, 0x0a, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x00, 0x02, 0x02, 0x01, 0x12, 0x03, 0x13, 0x0b, 0x0f, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x00, 0x02, 0x02, 0x03, 0x12, 0x03, 0x13, 0x12, 0x13, 0x0a, 0x2a, 0x0a, 0x04, 0x04, 0x00, 0x02,
    0x03, 0x12, 0x03, 0x15, 0x06, 0x1b, 0x1a, 0x1d, 0x20, 0x54, 0x68, 0x65, 0x20, 0x72, 0x65, 0x73,
    0x75, 0x6c, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x61, 0x6e, 0x20, 0x6f, 0x70, 0x65, 0x72, 0x61, 0x74,
    0x69, 0x6f, 0x6e, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x03, 0x06, 0x12, 0x03,
    0x15, 0x06, 0x0f, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x03, 0x01, 0x12, 0x03, 0x15, 0x10,
    0x16, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x03, 0x03, 0x12, 0x03, 0x15, 0x19, 0x1a, 0x0a,
    0x35, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x04, 0x12, 0x03, 0x17, 0x06, 0x18, 0x1a, 0x28, 0x20, 0x41,
    0x6e, 0x20, 0x61, 0x74, 0x74, 0x65, 0x6d, 0x70, 0x74, 0x20, 0x74, 0x6f, 0x20, 0x75, 0x6e, 0x6c,
    0x6f, 0x63, 0x6b, 0x20, 0x61, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x20, 0x6f, 0x72, 0x20, 0x62,
    0x6f, 0x6e, 0x75, 0x73, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x04, 0x06, 0x12,
    0x03, 0x17, 0x06, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x04, 0x01, 0x12, 0x03, 0x17,
    0x0d, 0x13, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x04, 0x03, 0x12, 0x03, 0x17, 0x16, 0x17,
    0x0a, 0x20, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x05, 0x12, 0x03, 0x19, 0x06, 0x1d, 0x1a, 0x13, 0x20,
    0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x20, 0x64, 0x6f, 0x6d, 0x65, 0x20, 0x64, 0x61, 0x74, 0x61,
    0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x05, 0x06, 0x12, 0x03, 0x19, 0x06, 0x0c,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x05, 0x01, 0x12, 0x03, 0x19, 0x0d, 0x18, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x00, 0x02, 0x05, 0x03, 0x12, 0x03, 0x19, 0x1b, 0x1c, 0x0a, 0x35, 0x0a, 0x04,
    0x04, 0x00, 0x02, 0x06, 0x12, 0x03, 0x1b, 0x06, 0x1f, 0x1a, 0x28, 0x20, 0x41, 0x20, 0x6e, 0x6f,
    0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x74, 0x6f, 0x20, 0x64, 0x69,
    0x73, 0x70, 0x6c, 0x61, 0x79, 0x20, 0x74, 0x6f, 0x20, 0x74, 0x68, 0x65, 0x20, 0x75, 0x73, 0x65,
    0x72, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x06, 0x06, 0x12, 0x03, 0x1b, 0x06,
    0x12, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x06, 0x01, 0x12, 0x03, 0x1b, 0x13, 0x19, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x06, 0x03, 0x12, 0x03, 0x1b, 0x1c, 0x1e, 0x0a, 0x92, 0x01,
    0x0a, 0x04, 0x04, 0x00, 0x02, 0x07, 0x12, 0x03, 0x1e, 0x06, 0x19, 0x1a, 0x84, 0x01, 0x20, 0x41,
    0x64, 0x64, 0x20, 0x73, 0x6f, 0x6d, 0x65, 0x20, 0x64, 0x61, 0x74, 0x61, 0x2e, 0x20, 0x54, 0x68,
    0x65, 0x20, 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x20, 0x69, 0x67, 0x6e, 0x6f, 0x72, 0x65, 0x73,
    0x20, 0x74, 0x68, 0x65, 0x20, 0x67, 0x69, 0x76, 0x65, 0x6e, 0x20, 0x49, 0x44, 0x20, 0x61, 0x6e,
    0x64, 0x20, 0x61, 0x73, 0x73, 0x69, 0x67, 0x6e, 0x73, 0x20, 0x69, 0x74, 0x27, 0x73, 0x20, 0x6f,
    0x77, 0x6e, 0x2c, 0x0a, 0x20, 0x74, 0x68, 0x65, 0x6e, 0x20, 0x73, 0x65, 0x6e, 0x64, 0x73, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x61, 0x64, 0x64, 0x65, 0x64, 0x20, 0x64, 0x61, 0x74, 0x61, 0x20, 0x74,
    0x6f, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x20, 0x69, 0x6e, 0x20,
    0x61, 0x20, 0x60, 0x64, 0x61, 0x74, 0x61, 0x60, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
    0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x07, 0x06, 0x12, 0x03, 0x1e, 0x06, 0x0a,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x07, 0x01, 0x12, 0x03, 0x1e, 0x0b, 0x13, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x00, 0x02, 0x07, 0x03, 0x12, 0x03, 0x1e, 0x16, 0x18, 0x0a, 0x56, 0x0a, 0x04,
    0x04, 0x00, 0x02, 0x08, 0x12, 0x03, 0x20, 0x06, 0x1d, 0x1a, 0x49, 0x20, 0x53, 0x65, 0x6e, 0x74,
    0x20, 0x62, 0x79, 0x20, 0x61, 0x6e, 0x20, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x20, 0x74, 0x6f, 0x20,
    0x62, 0x65, 0x67, 0x69, 0x6e, 0x20, 0x61, 0x20, 0x74, 0x72, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65,
    0x20, 0x68, 0x75, 0x6e, 0x74, 0x2c, 0x20, 0x67, 0x69, 0x76, 0x69, 0x6e, 0x67, 0x20, 0x74, 0x68,
    0x65, 0x20, 0x69, 0x64, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x60, 0x48, 0x75, 0x6e,
    0x74, 0x60, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x08, 0x05, 0x12, 0x03, 0x20,
    0x06, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x08, 0x01, 0x12, 0x03, 0x20, 0x0d, 0x17,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x08, 0x03, 0x12, 0x03, 0x20, 0x1a, 0x1c, 0x0a, 0x3a,
    0x0a, 0x04, 0x04, 0x00, 0x02, 0x09, 0x12, 0x03, 0x22, 0x06, 0x1c, 0x1a, 0x2d, 0x20, 0x43, 0x72,
    0x65, 0x61, 0x74, 0x65, 0x20, 0x61, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x20, 0x66, 0x72, 0x6f, 0x6d,
    0x20, 0x69, 0x74, 0x27, 0x73, 0x20, 0x4a, 0x53, 0x4f, 0x4e, 0x20, 0x72, 0x65, 0x70, 0x72, 0x65,
    0x73, 0x65, 0x6e, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00,
    0x02, 0x09, 0x05, 0x12, 0x03, 0x22, 0x06, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x09,
    0x01, 0x12, 0x03, 0x22, 0x0d, 0x16, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x09, 0x03, 0x12,
    0x03, 0x22, 0x19, 0x1b, 0x0a, 0x7d, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x0a, 0x12, 0x03, 0x25, 0x06,
    0x22, 0x1a, 0x70, 0x20, 0x41, 0x20, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x20, 0x74,
    0x6f, 0x20, 0x61, 0x20, 0x6a, 0x73, 0x6f, 0x6e, 0x5f, 0x68, 0x75, 0x6e, 0x74, 0x20, 0x6d, 0x65,
    0x73, 0x73, 0x61, 0x67, 0x65, 0x2c, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20, 0x74, 0x68, 0x65, 0x20,
    0x72, 0x61, 0x6e, 0x64, 0x6f, 0x6d, 0x6c, 0x79, 0x20, 0x67, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74,
    0x65, 0x64, 0x20, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x73, 0x0a, 0x20, 0x66, 0x6f,
    0x72, 0x20, 0x65, 0x61, 0x63, 0x68, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x6e, 0x65,
    0x77, 0x6c, 0x79, 0x20, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x20, 0x75, 0x73, 0x65, 0x72,
    0x73, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0a, 0x06, 0x12, 0x03, 0x25, 0x06,
    0x0d, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0a, 0x01, 0x12, 0x03, 0x25, 0x0e, 0x1c, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0a, 0x03, 0x12, 0x03, 0x25, 0x1f, 0x21, 0x0a, 0x4a, 0x0a,
    0x04, 0x04, 0x00, 0x02, 0x0b, 0x12, 0x03, 0x27, 0x06, 0x17, 0x1a, 0x3d, 0x20, 0x41, 0x20, 0x72,
    0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63,
    0x6c, 0x69, 0x65, 0x6e, 0x74, 0x20, 0x74, 0x6f, 0x20, 0x72, 0x65, 0x63, 0x6f, 0x6e, 0x6e, 0x65,
    0x63, 0x74, 0x2c, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20, 0x61, 0x20, 0x67, 0x69, 0x76, 0x65, 0x6e,
    0x20, 0x72, 0x65, 0x61, 0x73, 0x6f, 0x6e, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02,
    0x0b, 0x06, 0x12, 0x03, 0x27, 0x06, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0b, 0x01,
    0x12, 0x03, 0x27, 0x0c, 0x11, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0b, 0x03, 0x12, 0x03,
    0x27, 0x14, 0x16, 0x0a, 0x61, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x0c, 0x12, 0x03, 0x2c, 0x06, 0x1e,
    0x1a, 0x54, 0x20, 0x41, 0x20, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x20, 0x74, 0x6f, 0x20,
    0x6c, 0x6f, 0x67, 0x20, 0x69, 0x6e, 0x20, 0x2d, 0x2d, 0x20, 0x61, 0x6c, 0x6c, 0x20, 0x6f, 0x74,
    0x68, 0x65, 0x72, 0x20, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x73, 0x20, 0x77, 0x69, 0x6c,
    0x6c, 0x20, 0x66, 0x61, 0x69, 0x6c, 0x20, 0x62, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x20, 0x74, 0x68,
    0x65, 0x20, 0x75, 0x73, 0x65, 0x72, 0x20, 0x69, 0x73, 0x0a, 0x20, 0x6c, 0x6f, 0x67, 0x67, 0x65,
    0x64, 0x20, 0x69, 0x6e, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0c, 0x06, 0x12,
    0x03, 0x2c, 0x06, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0c, 0x01, 0x12, 0x03, 0x2c,
    0x0c, 0x19, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0c, 0x03, 0x12, 0x03, 0x2c, 0x1c, 0x1d,
    0x0a, 0x4b, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x0d, 0x12, 0x03, 0x2e, 0x06, 0x21, 0x1a, 0x3e, 0x20,
    0x4c, 0x6f, 0x67, 0x69, 0x6e, 0x20, 0x77, 0x61, 0x73, 0x20, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73,
    0x73, 0x66, 0x75, 0x6c, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x61, 0x6c, 0x6c, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x69, 0x6e, 0x69, 0x74, 0x69, 0x61, 0x6c, 0x20, 0x64, 0x61, 0x74, 0x61, 0x20, 0x68, 0x61,
    0x73, 0x20, 0x62, 0x65, 0x65, 0x6e, 0x20, 0x73, 0x65, 0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x00, 0x02, 0x0d, 0x06, 0x12, 0x03, 0x2e, 0x06, 0x0d, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x00, 0x02, 0x0d, 0x01, 0x12, 0x03, 0x2e, 0x0e, 0x1c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02,
    0x0d, 0x03, 0x12, 0x03, 0x2e, 0x1f, 0x20, 0x0a, 0x67, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x0e, 0x12,
    0x03, 0x31, 0x06, 0x19, 0x1a, 0x5a, 0x20, 0x41, 0x74, 0x74, 0x65, 0x6d, 0x70, 0x74, 0x20, 0x74,
    0x6f, 0x20, 0x72, 0x65, 0x73, 0x75, 0x6d, 0x65, 0x20, 0x61, 0x20, 0x73, 0x65, 0x73, 0x73, 0x69,
    0x6f, 0x6e, 0x2c, 0x20, 0x6f, 0x6e, 0x20, 0x77, 0x68, 0x69, 0x63, 0x68, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x20, 0x77, 0x69, 0x6c, 0x6c, 0x20, 0x72, 0x65, 0x2d,
    0x73, 0x65, 0x6e, 0x64, 0x20, 0x61, 0x6c, 0x6c, 0x0a, 0x20, 0x75, 0x6e, 0x2d, 0x61, 0x63, 0x6b,
    0x6e, 0x6f, 0x77, 0x6c, 0x65, 0x64, 0x67, 0x65, 0x64, 0x20, 0x64, 0x61, 0x74, 0x61, 0x2e, 0x0a,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0e, 0x06, 0x12, 0x03, 0x31, 0x06, 0x0d, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x00, 0x02, 0x0e, 0x01, 0x12, 0x03, 0x31, 0x0e, 0x14, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x00, 0x02, 0x0e, 0x03, 0x12, 0x03, 0x31, 0x17, 0x18, 0x0a, 0x4f, 0x0a, 0x04, 0x04, 0x00,
    0x02, 0x0f, 0x12, 0x03, 0x34, 0x06, 0x15, 0x1a, 0x42, 0x20, 0x50, 0x69, 0x6e, 0x67, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x6f, 0x74, 0x68, 0x65, 0x72, 0x20, 0x65, 0x6e, 0x64, 0x70, 0x6f, 0x69, 0x6e,
    0x74, 0x2c, 0x20, 0x60, 0x74, 0x72, 0x75, 0x65, 0x60, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x61, 0x20,
    0x70, 0x69, 0x6e, 0x67, 0x2c, 0x20, 0x60, 0x66, 0x61, 0x6c, 0x73, 0x65, 0x60, 0x20, 0x66, 0x6f,
    0x72, 0x20, 0x61, 0x20, 0x72, 0x65, 0x70, 0x6c, 0x79, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x00, 0x02, 0x0f, 0x05, 0x12, 0x03, 0x34, 0x06, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02,
    0x0f, 0x01, 0x12, 0x03, 0x34, 0x0b, 0x0f, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x0f, 0x03,
    0x12, 0x03, 0x34, 0x12, 0x14, 0x0a, 0x6a, 0x0a, 0x04, 0x04, 0x00, 0x02, 0x10, 0x12, 0x03, 0x39,
    0x03, 0x24, 0x1a, 0x5d, 0x20, 0x55, 0x73, 0x65, 0x64, 0x20, 0x69, 0x6e, 0x20, 0x72, 0x65, 0x73,
    0x70, 0x6f, 0x6e, 0x73, 0x65, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2c, 0x20,
    0x67, 0x69, 0x76, 0x65, 0x73, 0x20, 0x74, 0x68, 0x65, 0x20, 0x73, 0x65, 0x71, 0x75, 0x65, 0x6e,
    0x63, 0x65, 0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x0a, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x69,
    0x73, 0x20, 0x61, 0x20, 0x72, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x20, 0x74, 0x6f, 0x2e,
    0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x10, 0x04, 0x12, 0x03, 0x39, 0x03, 0x0b, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x00, 0x02, 0x10, 0x05, 0x12, 0x03, 0x39, 0x0c, 0x12, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x00, 0x02, 0x10, 0x01, 0x12, 0x03, 0x39, 0x13, 0x1e, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x00, 0x02, 0x10, 0x03, 0x12, 0x03, 0x39, 0x21, 0x23, 0x0a, 0x0a, 0x0a, 0x02, 0x04, 0x01, 0x12,
    0x04, 0x3c, 0x00, 0x3f, 0x01, 0x0a, 0x0a, 0x0a, 0x03, 0x04, 0x01, 0x01, 0x12, 0x03, 0x3c, 0x08,
    0x0f, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x01, 0x02, 0x00, 0x12, 0x03, 0x3d, 0x03, 0x1f, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x01, 0x02, 0x00, 0x04, 0x12, 0x03, 0x3d, 0x03, 0x0b, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x01, 0x02, 0x00, 0x05, 0x12, 0x03, 0x3d, 0x0c, 0x12, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x01,
    0x02, 0x00, 0x01, 0x12, 0x03, 0x3d, 0x13, 0x1a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x01, 0x02, 0x00,
    0x03, 0x12, 0x03, 0x3d, 0x1d, 0x1e, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x01, 0x02, 0x01, 0x12, 0x03,
    0x3e, 0x03, 0x22, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x01, 0x02, 0x01, 0x04, 0x12, 0x03, 0x3e, 0x03,
    0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x01, 0x02, 0x01, 0x05, 0x12, 0x03, 0x3e, 0x0c, 0x12, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x01, 0x02, 0x01, 0x01, 0x12, 0x03, 0x3e, 0x13, 0x1d, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x01, 0x02, 0x01, 0x03, 0x12, 0x03, 0x3e, 0x20, 0x21, 0x0a, 0x0a, 0x0a, 0x02, 0x04,
    0x02, 0x12, 0x04, 0x41, 0x00, 0x43, 0x01, 0x0a, 0x0a, 0x0a, 0x03, 0x04, 0x02, 0x01, 0x12, 0x03,
    0x41, 0x08, 0x0e, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x02, 0x02, 0x00, 0x12, 0x03, 0x42, 0x02, 0x1b,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x02, 0x02, 0x00, 0x04, 0x12, 0x03, 0x42, 0x02, 0x0a, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x02, 0x02, 0x00, 0x05, 0x12, 0x03, 0x42, 0x0b, 0x11, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x02, 0x02, 0x00, 0x01, 0x12, 0x03, 0x42, 0x12, 0x16, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x02,
    0x02, 0x00, 0x03, 0x12, 0x03, 0x42, 0x19, 0x1a, 0x0a, 0x0a, 0x0a, 0x02, 0x05, 0x00, 0x12, 0x04,
    0x45, 0x00, 0x48, 0x01, 0x0a, 0x0a, 0x0a, 0x03, 0x05, 0x00, 0x01, 0x12, 0x03, 0x45, 0x05, 0x15,
    0x0a, 0x41, 0x0a, 0x04, 0x05, 0x00, 0x02, 0x00, 0x12, 0x03, 0x47, 0x03, 0x0f, 0x1a, 0x34, 0x20,
    0x55, 0x73, 0x65, 0x20, 0x74, 0x68, 0x65, 0x20, 0x61, 0x70, 0x70, 0x27, 0x73, 0x20, 0x6c, 0x6f,
    0x67, 0x6f, 0x2c, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x6e, 0x6f, 0x6e, 0x2d, 0x73, 0x70, 0x65, 0x63,
    0x69, 0x66, 0x69, 0x63, 0x20, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f,
    0x6e, 0x73, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x00, 0x02, 0x00, 0x01, 0x12, 0x03, 0x47, 0x03,
    0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x00, 0x02, 0x00, 0x02, 0x12, 0x03, 0x47, 0x0d, 0x0e, 0x0a,
    0x0a, 0x0a, 0x02, 0x04, 0x03, 0x12, 0x04, 0x4a, 0x00, 0x54, 0x01, 0x0a, 0x0a, 0x0a, 0x03, 0x04,
    0x03, 0x01, 0x12, 0x03, 0x4a, 0x08, 0x14, 0x0a, 0x1d, 0x0a, 0x04, 0x04, 0x03, 0x02, 0x00, 0x12,
    0x03, 0x4c, 0x02, 0x1c, 0x1a, 0x10, 0x20, 0x41, 0x20, 0x73, 0x68, 0x6f, 0x72, 0x74, 0x20, 0x74,
    0x69, 0x74, 0x6c, 0x65, 0x2e, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x00, 0x04, 0x12,
    0x03, 0x4c, 0x02, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x00, 0x05, 0x12, 0x03, 0x4c,
    0x0b, 0x11, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x00, 0x01, 0x12, 0x03, 0x4c, 0x12, 0x17,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x00, 0x03, 0x12, 0x03, 0x4c, 0x1a, 0x1b, 0x0a, 0x30,
    0x0a, 0x04, 0x04, 0x03, 0x02, 0x01, 0x12, 0x03, 0x4e, 0x02, 0x1b, 0x1a, 0x23, 0x20, 0x54, 0x68,
    0x65, 0x20, 0x6d, 0x61, 0x69, 0x6e, 0x20, 0x74, 0x65, 0x78, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x6e, 0x6f, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x0a,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x01, 0x04, 0x12, 0x03, 0x4e, 0x02, 0x0a, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x03, 0x02, 0x01, 0x05, 0x12, 0x03, 0x4e, 0x0b, 0x11, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x03, 0x02, 0x01, 0x01, 0x12, 0x03, 0x4e, 0x12, 0x16, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03,
    0x02, 0x01, 0x03, 0x12, 0x03, 0x4e, 0x19, 0x1a, 0x0a, 0x27, 0x0a, 0x04, 0x04, 0x03, 0x02, 0x02,
    0x12, 0x03, 0x50, 0x02, 0x25, 0x1a, 0x1a, 0x20, 0x54, 0x68, 0x65, 0x20, 0x69, 0x63, 0x6f, 0x6e,
    0x20, 0x74, 0x6f, 0x20, 0x62, 0x65, 0x20, 0x64, 0x69, 0x73, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x64,
    0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x02, 0x04, 0x12, 0x03, 0x50, 0x02, 0x0a, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x02, 0x06, 0x12, 0x03, 0x50, 0x0b, 0x1b, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x03, 0x02, 0x02, 0x01, 0x12, 0x03, 0x50, 0x1c, 0x20, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x03, 0x02, 0x02, 0x03, 0x12, 0x03, 0x50, 0x23, 0x24, 0x0a, 0x67, 0x0a, 0x04, 0x04, 0x03, 0x02,
    0x03, 0x12, 0x03, 0x53, 0x02, 0x19, 0x1a, 0x5a, 0x20, 0x54, 0x68, 0x65, 0x20, 0x49, 0x44, 0x20,
    0x74, 0x6f, 0x20, 0x75, 0x73, 0x65, 0x2c, 0x20, 0x77, 0x68, 0x65, 0x6e, 0x20, 0x6e, 0x6f, 0x74,
    0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x20, 0x68, 0x61, 0x76, 0x65, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x73, 0x61, 0x6d, 0x65, 0x20, 0x49, 0x44, 0x20, 0x6e, 0x65, 0x77, 0x65,
    0x72, 0x20, 0x6f, 0x6e, 0x65, 0x73, 0x20, 0x77, 0x69, 0x6c, 0x6c, 0x0a, 0x20, 0x6f, 0x76, 0x65,
    0x72, 0x77, 0x72, 0x69, 0x74, 0x65, 0x20, 0x6f, 0x6c, 0x64, 0x65, 0x72, 0x20, 0x6f, 0x6e, 0x65,
    0x73, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x03, 0x04, 0x12, 0x03, 0x53, 0x02, 0x0a,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x03, 0x02, 0x03, 0x05, 0x12, 0x03, 0x53, 0x0b, 0x11, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x03, 0x02, 0x03, 0x01, 0x12, 0x03, 0x53, 0x12, 0x14, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x03, 0x02, 0x03, 0x03, 0x12, 0x03, 0x53, 0x17, 0x18, 0x0a, 0x36, 0x0a, 0x02, 0x04, 0x04,
    0x12, 0x04, 0x57, 0x00, 0x5a, 0x01, 0x1a, 0x2a, 0x20, 0x41, 0x20, 0x72, 0x65, 0x71, 0x75, 0x65,
    0x73, 0x74, 0x20, 0x74, 0x6f, 0x20, 0x6c, 0x6f, 0x67, 0x69, 0x6e, 0x20, 0x74, 0x6f, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x74, 0x72, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74,
    0x2e, 0x0a, 0x0a, 0x0a, 0x0a, 0x03, 0x04, 0x04, 0x01, 0x12, 0x03, 0x57, 0x08, 0x0d, 0x0a, 0x0b,
    0x0a, 0x04, 0x04, 0x04, 0x02, 0x00, 0x12, 0x03, 0x58, 0x03, 0x20, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x04, 0x02, 0x00, 0x04, 0x12, 0x03, 0x58, 0x03, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x04, 0x02,
    0x00, 0x05, 0x12, 0x03, 0x58, 0x0c, 0x12, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x04, 0x02, 0x00, 0x01,
    0x12, 0x03, 0x58, 0x13, 0x1b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x04, 0x02, 0x00, 0x03, 0x12, 0x03,
    0x58, 0x1e, 0x1f, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x04, 0x02, 0x01, 0x12, 0x03, 0x59, 0x03, 0x20,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x04, 0x02, 0x01, 0x04, 0x12, 0x03, 0x59, 0x03, 0x0b, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x04, 0x02, 0x01, 0x05, 0x12, 0x03, 0x59, 0x0c, 0x12, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x04, 0x02, 0x01, 0x01, 0x12, 0x03, 0x59, 0x13, 0x1b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x04,
    0x02, 0x01, 0x03, 0x12, 0x03, 0x59, 0x1e, 0x1f, 0x0a, 0x3b, 0x0a, 0x02, 0x04, 0x05, 0x12, 0x04,
    0x5d, 0x00, 0x62, 0x01, 0x1a, 0x2f, 0x20, 0x41, 0x20, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
    0x20, 0x66, 0x6f, 0x72, 0x20, 0x73, 0x6f, 0x6d, 0x65, 0x20, 0x64, 0x61, 0x74, 0x61, 0x20, 0x28,
    0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x2c, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x2c, 0x20, 0x65, 0x74,
    0x63, 0x2e, 0x2e, 0x29, 0x0a, 0x0a, 0x0a, 0x0a, 0x03, 0x04, 0x05, 0x01, 0x12, 0x03, 0x5d, 0x08,
    0x0e, 0x0a, 0x33, 0x0a, 0x04, 0x04, 0x05, 0x02, 0x00, 0x12, 0x03, 0x5f, 0x03, 0x1e, 0x1a, 0x26,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x74, 0x79, 0x70, 0x65, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x64, 0x61, 0x74, 0x61, 0x20, 0x62, 0x65, 0x69, 0x6e, 0x67, 0x20, 0x72, 0x65, 0x71, 0x75,
    0x65, 0x73, 0x74, 0x65, 0x64, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x05, 0x02, 0x00, 0x04, 0x12,
    0x03, 0x5f, 0x03, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x05, 0x02, 0x00, 0x06, 0x12, 0x03, 0x5f,
    0x0c, 0x14, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x05, 0x02, 0x00, 0x01, 0x12, 0x03, 0x5f, 0x15, 0x19,
    0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x05, 0x02, 0x00, 0x03, 0x12, 0x03, 0x5f, 0x1c, 0x1d, 0x0a, 0x21,
    0x0a, 0x04, 0x04, 0x05, 0x02, 0x01, 0x12, 0x03, 0x61, 0x03, 0x1b, 0x1a, 0x14, 0x20, 0x54, 0x68,
    0x65, 0x20, 0x49, 0x44, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x64, 0x61, 0x74, 0x61,
    0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x05, 0x02, 0x01, 0x04, 0x12, 0x03, 0x61, 0x03, 0x0b, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x05, 0x02, 0x01, 0x05, 0x12, 0x03, 0x61, 0x0c, 0x12, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x05, 0x02, 0x01, 0x01, 0x12, 0x03, 0x61, 0x13, 0x16, 0x0a, 0x0c, 0x0a, 0x05, 0x04,
    0x05, 0x02, 0x01, 0x03, 0x12, 0x03, 0x61, 0x19, 0x1a, 0x0a, 0x17, 0x0a, 0x02, 0x04, 0x06, 0x12,
    0x04, 0x65, 0x00, 0x70, 0x01, 0x1a, 0x0b, 0x20, 0x53, 0x6f, 0x6d, 0x65, 0x20, 0x64, 0x61, 0x74,
    0x61, 0x0a, 0x0a, 0x0a, 0x0a, 0x03, 0x04, 0x06, 0x01, 0x12, 0x03, 0x65, 0x08, 0x0c, 0x0a, 0x0c,
    0x0a, 0x04, 0x04, 0x06, 0x08, 0x00, 0x12, 0x04, 0x66, 0x04, 0x6f, 0x05, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x06, 0x08, 0x00, 0x01, 0x12, 0x03, 0x66, 0x0a, 0x0e, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x06,
    0x02, 0x00, 0x12, 0x03, 0x67, 0x07, 0x15, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x00, 0x06,
    0x12, 0x03, 0x67, 0x07, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x00, 0x01, 0x12, 0x03,
    0x67, 0x0c, 0x10, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x00, 0x03, 0x12, 0x03, 0x67, 0x13,
    0x14, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x06, 0x02, 0x01, 0x12, 0x03, 0x68, 0x07, 0x15, 0x0a, 0x0c,
    0x0a, 0x05, 0x04, 0x06, 0x02, 0x01, 0x06, 0x12, 0x03, 0x68, 0x07, 0x0b, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x06, 0x02, 0x01, 0x01, 0x12, 0x03, 0x68, 0x0c, 0x10, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06,
    0x02, 0x01, 0x03, 0x12, 0x03, 0x68, 0x13, 0x14, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x06, 0x02, 0x02,
    0x12, 0x03, 0x69, 0x07, 0x15, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x02, 0x06, 0x12, 0x03,
    0x69, 0x07, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x02, 0x01, 0x12, 0x03, 0x69, 0x0c,
    0x10, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x02, 0x03, 0x12, 0x03, 0x69, 0x13, 0x14, 0x0a,
    0x0b, 0x0a, 0x04, 0x04, 0x06, 0x02, 0x03, 0x12, 0x03, 0x6a, 0x07, 0x17, 0x0a, 0x0c, 0x0a, 0x05,
    0x04, 0x06, 0x02, 0x03, 0x06, 0x12, 0x03, 0x6a, 0x07, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06,
    0x02, 0x03, 0x01, 0x12, 0x03, 0x6a, 0x0d, 0x12, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x03,
    0x03, 0x12, 0x03, 0x6a, 0x15, 0x16, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x06, 0x02, 0x04, 0x12, 0x03,
    0x6b, 0x07, 0x17, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x04, 0x06, 0x12, 0x03, 0x6b, 0x07,
    0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x04, 0x01, 0x12, 0x03, 0x6b, 0x0d, 0x12, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x04, 0x03, 0x12, 0x03, 0x6b, 0x15, 0x16, 0x0a, 0x0b, 0x0a,
    0x04, 0x04, 0x06, 0x02, 0x05, 0x12, 0x03, 0x6c, 0x07, 0x15, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06,
    0x02, 0x05, 0x06, 0x12, 0x03, 0x6c, 0x07, 0x0b, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x05,
    0x01, 0x12, 0x03, 0x6c, 0x0c, 0x10, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x05, 0x03, 0x12,
    0x03, 0x6c, 0x13, 0x14, 0x0a, 0x0b, 0x0a, 0x04, 0x04, 0x06, 0x02, 0x06, 0x12, 0x03, 0x6d, 0x07,
    0x24, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x06, 0x06, 0x12, 0x03, 0x6d, 0x07, 0x12, 0x0a,
    0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x06, 0x01, 0x12, 0x03, 0x6d, 0x13, 0x1f, 0x0a, 0x0c, 0x0a,
    0x05, 0x04, 0x06, 0x02, 0x06, 0x03, 0x12, 0x03, 0x6d, 0x22, 0x23, 0x0a, 0x0b, 0x0a, 0x04, 0x04,
    0x06, 0x02, 0x07, 0x12, 0x03, 0x6e, 0x07, 0x27, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x07,
    0x06, 0x12, 0x03, 0x6e, 0x07, 0x13, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x07, 0x01, 0x12,
    0x03, 0x6e, 0x14, 0x21, 0x0a, 0x0c, 0x0a, 0x05, 0x04, 0x06, 0x02, 0x07, 0x03, 0x12, 0x03, 0x6e,
    0x24, 0x26, 0x0a, 0x0a, 0x0a, 0x02, 0x05, 0x01, 0x12, 0x04, 0x72, 0x00, 0x76, 0x01, 0x0a, 0x0a,
    0x0a, 0x03, 0x05, 0x01, 0x01, 0x12, 0x03, 0x72, 0x05, 0x11, 0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x01,
    0x02, 0x00, 0x12, 0x03, 0x73, 0x03, 0x0f, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x01, 0x02, 0x00, 0x01,
    0x12, 0x03, 0x73, 0x03, 0x0a, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x01, 0x02, 0x00, 0x02, 0x12, 0x03,
    0x73, 0x0d, 0x0e, 0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x01, 0x02, 0x01, 0x12, 0x03, 0x74, 0x03, 0x0e,
    0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x01, 0x02, 0x01, 0x01, 0x12, 0x03, 0x74, 0x03, 0x09, 0x0a, 0x0c,
    0x0a, 0x05, 0x05, 0x01, 0x02, 0x01, 0x02, 0x12, 0x03, 0x74, 0x0c, 0x0d, 0x0a, 0x0b, 0x0a, 0x04,
    0x05, 0x01, 0x02, 0x02, 0x12, 0x03, 0x75, 0x03, 0x0e, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x01, 0x02,
    0x02, 0x01, 0x12, 0x03, 0x75, 0x03, 0x09, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x01, 0x02, 0x02, 0x02,
    0x12, 0x03, 0x75, 0x0c, 0x0d, 0x0a, 0x0b, 0x0a, 0x02, 0x05, 0x02, 0x12, 0x05, 0x78, 0x00, 0x81,
    0x01, 0x01, 0x0a, 0x0a, 0x0a, 0x03, 0x05, 0x02, 0x01, 0x12, 0x03, 0x78, 0x05, 0x0d, 0x0a, 0x0b,
    0x0a, 0x04, 0x05, 0x02, 0x02, 0x00, 0x12, 0x03, 0x79, 0x03, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x05,
    0x02, 0x02, 0x00, 0x01, 0x12, 0x03, 0x79, 0x03, 0x07, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02,
    0x00, 0x02, 0x12, 0x03, 0x79, 0x0a, 0x0b, 0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x02, 0x02, 0x01, 0x12,
    0x03, 0x7a, 0x03, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x01, 0x01, 0x12, 0x03, 0x7a,
    0x03, 0x07, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x01, 0x02, 0x12, 0x03, 0x7a, 0x0a, 0x0b,
    0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x02, 0x02, 0x02, 0x12, 0x03, 0x7b, 0x03, 0x0c, 0x0a, 0x0c, 0x0a,
    0x05, 0x05, 0x02, 0x02, 0x02, 0x01, 0x12, 0x03, 0x7b, 0x03, 0x07, 0x0a, 0x0c, 0x0a, 0x05, 0x05,
    0x02, 0x02, 0x02, 0x02, 0x12, 0x03, 0x7b, 0x0a, 0x0b, 0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x02, 0x02,
    0x03, 0x12, 0x03, 0x7c, 0x03, 0x0d, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x03, 0x01, 0x12,
    0x03, 0x7c, 0x03, 0x08, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x03, 0x02, 0x12, 0x03, 0x7c,
    0x0b, 0x0c, 0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x02, 0x02, 0x04, 0x12, 0x03, 0x7d, 0x03, 0x0d, 0x0a,
    0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x04, 0x01, 0x12, 0x03, 0x7d, 0x03, 0x08, 0x0a, 0x0c, 0x0a,
    0x05, 0x05, 0x02, 0x02, 0x04, 0x02, 0x12, 0x03, 0x7d, 0x0b, 0x0c, 0x0a, 0x0b, 0x0a, 0x04, 0x05,
    0x02, 0x02, 0x05, 0x12, 0x03, 0x7e, 0x03, 0x0c, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x05,
    0x01, 0x12, 0x03, 0x7e, 0x03, 0x07, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x05, 0x02, 0x12,
    0x03, 0x7e, 0x0a, 0x0b, 0x0a, 0x0b, 0x0a, 0x04, 0x05, 0x02, 0x02, 0x06, 0x12, 0x03, 0x7f, 0x03,
    0x14, 0x0a, 0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x06, 0x01, 0x12, 0x03, 0x7f, 0x03, 0x0f, 0x0a,
    0x0c, 0x0a, 0x05, 0x05, 0x02, 0x02, 0x06, 0x02, 0x12, 0x03, 0x7f, 0x12, 0x13, 0x0a, 0x0c, 0x0a,
    0x04, 0x05, 0x02, 0x02, 0x07, 0x12, 0x04, 0x80, 0x01, 0x03, 0x15, 0x0a, 0x0d, 0x0a, 0x05, 0x05,
    0x02, 0x02, 0x07, 0x01, 0x12, 0x04, 0x80, 0x01, 0x03, 0x10, 0x0a, 0x0d, 0x0a, 0x05, 0x05, 0x02,
    0x02, 0x07, 0x02, 0x12, 0x04, 0x80, 0x01, 0x13, 0x14, 0x0a, 0x1b, 0x0a, 0x02, 0x04, 0x07, 0x12,
    0x06, 0x84, 0x01, 0x00, 0x87, 0x01, 0x01, 0x1a, 0x0d, 0x20, 0x41, 0x20, 0x6c, 0x6f, 0x63, 0x61,
    0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x0a, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x07, 0x01, 0x12, 0x04, 0x84,
    0x01, 0x08, 0x10, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x07, 0x02, 0x00, 0x12, 0x04, 0x85, 0x01, 0x03,
    0x20, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x07, 0x02, 0x00, 0x04, 0x12, 0x04, 0x85, 0x01, 0x03, 0x0b,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x07, 0x02, 0x00, 0x05, 0x12, 0x04, 0x85, 0x01, 0x0c, 0x12, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x07, 0x02, 0x00, 0x01, 0x12, 0x04, 0x85, 0x01, 0x13, 0x1b, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x07, 0x02, 0x00, 0x03, 0x12, 0x04, 0x85, 0x01, 0x1e, 0x1f, 0x0a, 0x0c, 0x0a,
    0x04, 0x04, 0x07, 0x02, 0x01, 0x12, 0x04, 0x86, 0x01, 0x03, 0x21, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x07, 0x02, 0x01, 0x04, 0x12, 0x04, 0x86, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x07,
    0x02, 0x01, 0x05, 0x12, 0x04, 0x86, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x07, 0x02,
    0x01, 0x01, 0x12, 0x04, 0x86, 0x01, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x07, 0x02, 0x01,
    0x03, 0x12, 0x04, 0x86, 0x01, 0x1f, 0x20, 0x0a, 0x2f, 0x0a, 0x02, 0x05, 0x03, 0x12, 0x06, 0x8a,
    0x01, 0x00, 0x8e, 0x01, 0x01, 0x1a, 0x21, 0x20, 0x54, 0x68, 0x65, 0x20, 0x70, 0x65, 0x72, 0x6d,
    0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x20, 0x6c, 0x65, 0x76, 0x65, 0x6c, 0x20, 0x6f, 0x66, 0x20,
    0x61, 0x20, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x0a, 0x0a, 0x0b, 0x0a, 0x03, 0x05, 0x03, 0x01, 0x12,
    0x04, 0x8a, 0x01, 0x05, 0x0f, 0x0a, 0x0c, 0x0a, 0x04, 0x05, 0x03, 0x02, 0x00, 0x12, 0x04, 0x8b,
    0x01, 0x03, 0x0e, 0x0a, 0x0d, 0x0a, 0x05, 0x05, 0x03, 0x02, 0x00, 0x01, 0x12, 0x04, 0x8b, 0x01,
    0x03, 0x09, 0x0a, 0x0d, 0x0a, 0x05, 0x05, 0x03, 0x02, 0x00, 0x02, 0x12, 0x04, 0x8b, 0x01, 0x0c,
    0x0d, 0x0a, 0x0c, 0x0a, 0x04, 0x05, 0x03, 0x02, 0x01, 0x12, 0x04, 0x8c, 0x01, 0x03, 0x14, 0x0a,
    0x0d, 0x0a, 0x05, 0x05, 0x03, 0x02, 0x01, 0x01, 0x12, 0x04, 0x8c, 0x01, 0x03, 0x0f, 0x0a, 0x0d,
    0x0a, 0x05, 0x05, 0x03, 0x02, 0x01, 0x02, 0x12, 0x04, 0x8c, 0x01, 0x12, 0x13, 0x0a, 0x0c, 0x0a,
    0x04, 0x05, 0x03, 0x02, 0x02, 0x12, 0x04, 0x8d, 0x01, 0x03, 0x0d, 0x0a, 0x0d, 0x0a, 0x05, 0x05,
    0x03, 0x02, 0x02, 0x01, 0x12, 0x04, 0x8d, 0x01, 0x03, 0x08, 0x0a, 0x0d, 0x0a, 0x05, 0x05, 0x03,
    0x02, 0x02, 0x02, 0x12, 0x04, 0x8d, 0x01, 0x0b, 0x0c, 0x0a, 0x0c, 0x0a, 0x02, 0x04, 0x08, 0x12,
    0x06, 0x90, 0x01, 0x00, 0xa0, 0x01, 0x01, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x08, 0x01, 0x12, 0x04,
    0x90, 0x01, 0x08, 0x0c, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x08, 0x02, 0x00, 0x12, 0x04, 0x91, 0x01,
    0x03, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x00, 0x04, 0x12, 0x04, 0x91, 0x01, 0x03,
    0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x00, 0x05, 0x12, 0x04, 0x91, 0x01, 0x0c, 0x12,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x00, 0x01, 0x12, 0x04, 0x91, 0x01, 0x13, 0x15, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x00, 0x03, 0x12, 0x04, 0x91, 0x01, 0x18, 0x19, 0x0a, 0x56,
    0x0a, 0x04, 0x04, 0x08, 0x02, 0x01, 0x12, 0x04, 0x93, 0x01, 0x03, 0x1f, 0x1a, 0x48, 0x20, 0x54,
    0x68, 0x65, 0x20, 0x49, 0x44, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x74, 0x65, 0x61,
    0x6d, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x75, 0x73, 0x65, 0x72, 0x20, 0x69, 0x73, 0x20, 0x69,
    0x6e, 0x2e, 0x20, 0x55, 0x6e, 0x73, 0x65, 0x74, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x64, 0x65, 0x6d,
    0x6f, 0x6e, 0x73, 0x74, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x73, 0x20, 0x6f, 0x72, 0x20, 0x61, 0x64,
    0x6d, 0x69, 0x6e, 0x73, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x01, 0x04, 0x12,
    0x04, 0x93, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x01, 0x05, 0x12, 0x04,
    0x93, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x01, 0x01, 0x12, 0x04, 0x93,
    0x01, 0x13, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x01, 0x03, 0x12, 0x04, 0x93, 0x01,
    0x1d, 0x1e, 0x0a, 0x20, 0x0a, 0x04, 0x04, 0x08, 0x02, 0x02, 0x12, 0x04, 0x95, 0x01, 0x03, 0x1c,
    0x1a, 0x12, 0x20, 0x54, 0x68, 0x65, 0x20, 0x75, 0x73, 0x65, 0x72, 0x27, 0x73, 0x20, 0x6e, 0x61,
    0x6d, 0x65, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x02, 0x04, 0x12, 0x04, 0x95,
    0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x02, 0x05, 0x12, 0x04, 0x95, 0x01,
    0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x02, 0x01, 0x12, 0x04, 0x95, 0x01, 0x13,
    0x17, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x02, 0x03, 0x12, 0x04, 0x95, 0x01, 0x1a, 0x1b,
    0x0a, 0x31, 0x0a, 0x04, 0x04, 0x08, 0x02, 0x03, 0x12, 0x04, 0x97, 0x01, 0x03, 0x26, 0x1a, 0x23,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x20,
    0x6c, 0x65, 0x76, 0x65, 0x6c, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x75, 0x73, 0x65,
    0x72, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x03, 0x04, 0x12, 0x04, 0x97, 0x01,
    0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x03, 0x06, 0x12, 0x04, 0x97, 0x01, 0x0c,
    0x16, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x03, 0x01, 0x12, 0x04, 0x97, 0x01, 0x17, 0x21,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x03, 0x03, 0x12, 0x04, 0x97, 0x01, 0x24, 0x25, 0x0a,
    0x7a, 0x0a, 0x04, 0x04, 0x08, 0x02, 0x04, 0x12, 0x04, 0x9a, 0x01, 0x03, 0x22, 0x1a, 0x6c, 0x20,
    0x54, 0x68, 0x65, 0x20, 0x6c, 0x61, 0x73, 0x74, 0x20, 0x6b, 0x6e, 0x6f, 0x77, 0x6e, 0x20, 0x6c,
    0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x75,
    0x73, 0x65, 0x72, 0x2c, 0x20, 0x73, 0x65, 0x74, 0x20, 0x6f, 0x6e, 0x6c, 0x79, 0x20, 0x69, 0x66,
    0x20, 0x74, 0x68, 0x65, 0x20, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x65, 0x72, 0x20, 0x69,
    0x73, 0x20, 0x61, 0x20, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x0a, 0x20, 0x69, 0x6e, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x73, 0x61, 0x6d, 0x65, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x20, 0x61, 0x73, 0x20,
    0x74, 0x68, 0x69, 0x73, 0x20, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x08, 0x02, 0x04, 0x04, 0x12, 0x04, 0x9a, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08,
    0x02, 0x04, 0x06, 0x12, 0x04, 0x9a, 0x01, 0x0c, 0x14, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02,
    0x04, 0x01, 0x12, 0x04, 0x9a, 0x01, 0x15, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x04,
    0x03, 0x12, 0x04, 0x9a, 0x01, 0x20, 0x21, 0x0a, 0x28, 0x0a, 0x04, 0x04, 0x08, 0x02, 0x05, 0x12,
    0x04, 0x9c, 0x01, 0x03, 0x24, 0x1a, 0x1a, 0x20, 0x54, 0x68, 0x65, 0x20, 0x73, 0x74, 0x61, 0x74,
    0x75, 0x73, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72,
    0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x05, 0x04, 0x12, 0x04, 0x9c, 0x01, 0x03, 0x0b,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x05, 0x06, 0x12, 0x04, 0x9c, 0x01, 0x0c, 0x18, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02, 0x05, 0x01, 0x12, 0x04, 0x9c, 0x01, 0x19, 0x1f, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x08, 0x02, 0x05, 0x03, 0x12, 0x04, 0x9c, 0x01, 0x22, 0x23, 0x0a, 0x6d, 0x0a,
    0x04, 0x04, 0x08, 0x02, 0x06, 0x12, 0x04, 0x9f, 0x01, 0x03, 0x21, 0x1a, 0x5f, 0x20, 0x57, 0x68,
    0x65, 0x6e, 0x20, 0x73, 0x65, 0x74, 0x20, 0x6f, 0x6e, 0x20, 0x63, 0x72, 0x65, 0x61, 0x74, 0x69,
    0x6f, 0x6e, 0x2c, 0x20, 0x74, 0x68, 0x65, 0x20, 0x75, 0x73, 0x65, 0x72, 0x20, 0x77, 0x69, 0x6c,
    0x6c, 0x20, 0x62, 0x65, 0x20, 0x61, 0x64, 0x64, 0x65, 0x64, 0x20, 0x61, 0x73, 0x20, 0x61, 0x20,
    0x64, 0x65, 0x6d, 0x6f, 0x6e, 0x73, 0x74, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x20, 0x74, 0x6f, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x0a, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x67, 0x69, 0x76, 0x65, 0x6e, 0x20, 0x49, 0x44, 0x0a, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x08, 0x02, 0x06, 0x04, 0x12, 0x04, 0x9f, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x08, 0x02, 0x06, 0x05, 0x12, 0x04, 0x9f, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08,
    0x02, 0x06, 0x01, 0x12, 0x04, 0x9f, 0x01, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x08, 0x02,
    0x06, 0x03, 0x12, 0x04, 0x9f, 0x01, 0x1f, 0x20, 0x0a, 0x0c, 0x0a, 0x02, 0x04, 0x09, 0x12, 0x06,
    0xa2, 0x01, 0x00, 0xb4, 0x01, 0x01, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x09, 0x01, 0x12, 0x04, 0xa2,
    0x01, 0x08, 0x0c, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x09, 0x02, 0x00, 0x12, 0x04, 0xa3, 0x01, 0x03,
    0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x00, 0x04, 0x12, 0x04, 0xa3, 0x01, 0x03, 0x0b,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x00, 0x05, 0x12, 0x04, 0xa3, 0x01, 0x0c, 0x12, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x00, 0x01, 0x12, 0x04, 0xa3, 0x01, 0x13, 0x15, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x09, 0x02, 0x00, 0x03, 0x12, 0x04, 0xa3, 0x01, 0x18, 0x19, 0x0a, 0x4b, 0x0a,
    0x04, 0x04, 0x09, 0x02, 0x01, 0x12, 0x04, 0xa5, 0x01, 0x03, 0x1d, 0x1a, 0x3d, 0x20, 0x54, 0x68,
    0x65, 0x20, 0x6c, 0x69, 0x73, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x49, 0x44, 0x73, 0x20, 0x6f, 0x66,
    0x20, 0x74, 0x65, 0x61, 0x6d, 0x73, 0x20, 0x74, 0x61, 0x6b, 0x69, 0x6e, 0x67, 0x20, 0x70, 0x61,
    0x72, 0x74, 0x20, 0x69, 0x6e, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74, 0x72, 0x65, 0x61, 0x73,
    0x75, 0x72, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09,
    0x02, 0x01, 0x04, 0x12, 0x04, 0xa5, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02,
    0x01, 0x05, 0x12, 0x04, 0xa5, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x01,
    0x01, 0x12, 0x04, 0xa5, 0x01, 0x13, 0x18, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x01, 0x03,
    0x12, 0x04, 0xa5, 0x01, 0x1b, 0x1c, 0x0a, 0x29, 0x0a, 0x04, 0x04, 0x09, 0x02, 0x02, 0x12, 0x04,
    0xa7, 0x01, 0x03, 0x1e, 0x1a, 0x1b, 0x20, 0x49, 0x44, 0x73, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68,
    0x69, 0x73, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x73, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x2e,
    0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x02, 0x04, 0x12, 0x04, 0xa7, 0x01, 0x03, 0x0b,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x02, 0x05, 0x12, 0x04, 0xa7, 0x01, 0x0c, 0x12, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x02, 0x01, 0x12, 0x04, 0xa7, 0x01, 0x13, 0x19, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x09, 0x02, 0x02, 0x03, 0x12, 0x04, 0xa7, 0x01, 0x1c, 0x1d, 0x0a, 0x30, 0x0a,
    0x04, 0x04, 0x09, 0x02, 0x03, 0x12, 0x04, 0xa9, 0x01, 0x03, 0x1f, 0x1a, 0x22, 0x20, 0x49, 0x44,
    0x73, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x27, 0x73,
    0x20, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x2e, 0x0a, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x03, 0x04, 0x12, 0x04, 0xa9, 0x01, 0x03, 0x0b, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x09, 0x02, 0x03, 0x05, 0x12, 0x04, 0xa9, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x09, 0x02, 0x03, 0x01, 0x12, 0x04, 0xa9, 0x01, 0x13, 0x1a, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x09, 0x02, 0x03, 0x03, 0x12, 0x04, 0xa9, 0x01, 0x1d, 0x1e, 0x0a, 0x2f, 0x0a, 0x04, 0x04,
    0x09, 0x02, 0x04, 0x12, 0x04, 0xab, 0x01, 0x03, 0x1d, 0x1a, 0x21, 0x20, 0x49, 0x44, 0x73, 0x20,
    0x6f, 0x66, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x27, 0x73, 0x20, 0x63,
    0x68, 0x61, 0x74, 0x20, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x73, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x09, 0x02, 0x04, 0x04, 0x12, 0x04, 0xab, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x09, 0x02, 0x04, 0x05, 0x12, 0x04, 0xab, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09,
    0x02, 0x04, 0x01, 0x12, 0x04, 0xab, 0x01, 0x13, 0x18, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02,
    0x04, 0x03, 0x12, 0x04, 0xab, 0x01, 0x1b, 0x1c, 0x0a, 0x36, 0x0a, 0x04, 0x04, 0x09, 0x02, 0x05,
    0x12, 0x04, 0xad, 0x01, 0x03, 0x25, 0x1a, 0x28, 0x20, 0x49, 0x44, 0x73, 0x20, 0x6f, 0x66, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x64, 0x65, 0x6d, 0x6f, 0x6e, 0x73, 0x74, 0x72, 0x61, 0x74, 0x6f, 0x72,
    0x73, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x74, 0x68, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x2e, 0x0a,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x05, 0x04, 0x12, 0x04, 0xad, 0x01, 0x03, 0x0b, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x05, 0x05, 0x12, 0x04, 0xad, 0x01, 0x0c, 0x12, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x09, 0x02, 0x05, 0x01, 0x12, 0x04, 0xad, 0x01, 0x13, 0x20, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x09, 0x02, 0x05, 0x03, 0x12, 0x04, 0xad, 0x01, 0x23, 0x24, 0x0a, 0x27, 0x0a, 0x04,
    0x04, 0x09, 0x02, 0x06, 0x12, 0x04, 0xaf, 0x01, 0x03, 0x22, 0x1a, 0x19, 0x20, 0x54, 0x68, 0x65,
    0x20, 0x63, 0x65, 0x6e, 0x74, 0x65, 0x72, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x68,
    0x75, 0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x06, 0x04, 0x12, 0x04,
    0xaf, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x06, 0x06, 0x12, 0x04, 0xaf,
    0x01, 0x0c, 0x14, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x06, 0x01, 0x12, 0x04, 0xaf, 0x01,
    0x15, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x06, 0x03, 0x12, 0x04, 0xaf, 0x01, 0x20,
    0x21, 0x0a, 0x35, 0x0a, 0x04, 0x04, 0x09, 0x02, 0x07, 0x12, 0x04, 0xb1, 0x01, 0x03, 0x1e, 0x1a,
    0x27, 0x20, 0x54, 0x68, 0x65, 0x20, 0x72, 0x61, 0x64, 0x69, 0x75, 0x73, 0x20, 0x69, 0x6e, 0x20,
    0x6b, 0x69, 0x6c, 0x6f, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x73, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68,
    0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x07,
    0x04, 0x12, 0x04, 0xb1, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x07, 0x05,
    0x12, 0x04, 0xb1, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x07, 0x01, 0x12,
    0x04, 0xb1, 0x01, 0x13, 0x19, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x07, 0x03, 0x12, 0x04,
    0xb1, 0x01, 0x1c, 0x1d, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x09, 0x02, 0x08, 0x12, 0x04, 0xb2, 0x01,
    0x03, 0x25, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x08, 0x04, 0x12, 0x04, 0xb2, 0x01, 0x03,
    0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x08, 0x05, 0x12, 0x04, 0xb2, 0x01, 0x0c, 0x12,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x08, 0x01, 0x12, 0x04, 0xb2, 0x01, 0x13, 0x1f, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02, 0x08, 0x03, 0x12, 0x04, 0xb2, 0x01, 0x22, 0x24, 0x0a, 0x0c,
    0x0a, 0x04, 0x04, 0x09, 0x02, 0x09, 0x12, 0x04, 0xb3, 0x01, 0x03, 0x1d, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x09, 0x02, 0x09, 0x04, 0x12, 0x04, 0xb3, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x09, 0x02, 0x09, 0x05, 0x12, 0x04, 0xb3, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09,
    0x02, 0x09, 0x01, 0x12, 0x04, 0xb3, 0x01, 0x13, 0x17, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x09, 0x02,
    0x09, 0x03, 0x12, 0x04, 0xb3, 0x01, 0x1a, 0x1c, 0x0a, 0x0c, 0x0a, 0x02, 0x04, 0x0a, 0x12, 0x06,
    0xb6, 0x01, 0x00, 0xcd, 0x01, 0x01, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x0a, 0x01, 0x12, 0x04, 0xb6,
    0x01, 0x08, 0x0c, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x0a, 0x02, 0x00, 0x12, 0x04, 0xb7, 0x01, 0x03,
    0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x00, 0x04, 0x12, 0x04, 0xb7, 0x01, 0x03, 0x0b,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x00, 0x05, 0x12, 0x04, 0xb7, 0x01, 0x0c, 0x12, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x00, 0x01, 0x12, 0x04, 0xb7, 0x01, 0x13, 0x15, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0a, 0x02, 0x00, 0x03, 0x12, 0x04, 0xb7, 0x01, 0x18, 0x19, 0x0a, 0x20, 0x0a,
    0x04, 0x04, 0x0a, 0x02, 0x01, 0x12, 0x04, 0xb9, 0x01, 0x03, 0x1c, 0x1a, 0x12, 0x20, 0x54, 0x68,
    0x65, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x27, 0x73, 0x20, 0x6e, 0x61, 0x6d, 0x65, 0x2e, 0x0a, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x01, 0x04, 0x12, 0x04, 0xb9, 0x01, 0x03, 0x0b, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0a, 0x02, 0x01, 0x05, 0x12, 0x04, 0xb9, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0a, 0x02, 0x01, 0x01, 0x12, 0x04, 0xb9, 0x01, 0x13, 0x17, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0a, 0x02, 0x01, 0x03, 0x12, 0x04, 0xb9, 0x01, 0x1a, 0x1b, 0x0a, 0x29, 0x0a, 0x04, 0x04,
    0x0a, 0x02, 0x02, 0x12, 0x04, 0xbb, 0x01, 0x03, 0x1d, 0x1a, 0x1b, 0x20, 0x54, 0x68, 0x65, 0x20,
    0x74, 0x65, 0x61, 0x6d, 0x27, 0x73, 0x20, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x20, 0x73,
    0x63, 0x6f, 0x72, 0x65, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x02, 0x04, 0x12,
    0x04, 0xbb, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x02, 0x05, 0x12, 0x04,
    0xbb, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x02, 0x01, 0x12, 0x04, 0xbb,
    0x01, 0x13, 0x18, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x02, 0x03, 0x12, 0x04, 0xbb, 0x01,
    0x1b, 0x1c, 0x0a, 0x40, 0x0a, 0x04, 0x04, 0x0a, 0x02, 0x03, 0x12, 0x04, 0xbd, 0x01, 0x03, 0x1c,
    0x1a, 0x32, 0x20, 0x54, 0x68, 0x65, 0x20, 0x49, 0x44, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x74, 0x72, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x20, 0x62, 0x65, 0x6c, 0x6f, 0x6e, 0x67, 0x73, 0x20,
    0x74, 0x6f, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x03, 0x04, 0x12, 0x04, 0xbd,
    0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x03, 0x05, 0x12, 0x04, 0xbd, 0x01,
    0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x03, 0x01, 0x12, 0x04, 0xbd, 0x01, 0x13,
    0x17, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x03, 0x03, 0x12, 0x04, 0xbd, 0x01, 0x1a, 0x1b,
    0x0a, 0x3b, 0x0a, 0x04, 0x04, 0x0a, 0x02, 0x04, 0x12, 0x04, 0xbf, 0x01, 0x03, 0x1f, 0x1a, 0x2d,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x6c, 0x69, 0x73, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x70, 0x6c, 0x61,
    0x79, 0x65, 0x72, 0x73, 0x20, 0x62, 0x65, 0x6c, 0x6f, 0x6e, 0x67, 0x69, 0x6e, 0x67, 0x20, 0x74,
    0x6f, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0a, 0x02, 0x04, 0x04, 0x12, 0x04, 0xbf, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0a, 0x02, 0x04, 0x05, 0x12, 0x04, 0xbf, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0a, 0x02, 0x04, 0x01, 0x12, 0x04, 0xbf, 0x01, 0x13, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a,
    0x02, 0x04, 0x03, 0x12, 0x04, 0xbf, 0x01, 0x1d, 0x1e, 0x0a, 0x3d, 0x0a, 0x04, 0x04, 0x0a, 0x02,
    0x05, 0x12, 0x04, 0xc1, 0x01, 0x03, 0x22, 0x1a, 0x2f, 0x20, 0x54, 0x68, 0x65, 0x20, 0x6c, 0x69,
    0x73, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x20, 0x77, 0x68, 0x69,
    0x63, 0x68, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x20, 0x68, 0x61, 0x73,
    0x20, 0x66, 0x6f, 0x75, 0x6e, 0x64, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x05,
    0x04, 0x12, 0x04, 0xc1, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x05, 0x06,
    0x12, 0x04, 0xc1, 0x01, 0x0c, 0x16, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x05, 0x01, 0x12,
    0x04, 0xc1, 0x01, 0x17, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x05, 0x03, 0x12, 0x04,
    0xc1, 0x01, 0x20, 0x21, 0x0a, 0x3e, 0x0a, 0x04, 0x04, 0x0a, 0x02, 0x06, 0x12, 0x04, 0xc3, 0x01,
    0x03, 0x1f, 0x1a, 0x30, 0x20, 0x54, 0x68, 0x65, 0x20, 0x6c, 0x69, 0x73, 0x74, 0x20, 0x6f, 0x66,
    0x20, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x65, 0x73, 0x20, 0x77, 0x68, 0x69, 0x63, 0x68, 0x20, 0x74,
    0x68, 0x69, 0x73, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x20, 0x68, 0x61, 0x73, 0x20, 0x66, 0x6f, 0x75,
    0x6e, 0x64, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x06, 0x04, 0x12, 0x04, 0xc3,
    0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x06, 0x05, 0x12, 0x04, 0xc3, 0x01,
    0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x06, 0x01, 0x12, 0x04, 0xc3, 0x01, 0x13,
    0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x06, 0x03, 0x12, 0x04, 0xc3, 0x01, 0x1d, 0x1e,
    0x0a, 0x3c, 0x0a, 0x04, 0x04, 0x0a, 0x02, 0x07, 0x12, 0x04, 0xc5, 0x01, 0x03, 0x24, 0x1a, 0x2e,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x20, 0x6f, 0x66, 0x20, 0x6d,
    0x61, 0x69, 0x6e, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x20, 0x74, 0x68, 0x65, 0x20, 0x74,
    0x65, 0x61, 0x6d, 0x20, 0x68, 0x61, 0x73, 0x20, 0x66, 0x6f, 0x75, 0x6e, 0x64, 0x0a, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0a, 0x02, 0x07, 0x04, 0x12, 0x04, 0xc5, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0a, 0x02, 0x07, 0x05, 0x12, 0x04, 0xc5, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0a, 0x02, 0x07, 0x01, 0x12, 0x04, 0xc5, 0x01, 0x13, 0x1f, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0a, 0x02, 0x07, 0x03, 0x12, 0x04, 0xc5, 0x01, 0x22, 0x23, 0x0a, 0x3d, 0x0a, 0x04, 0x04, 0x0a,
    0x02, 0x08, 0x12, 0x04, 0xc7, 0x01, 0x03, 0x25, 0x1a, 0x2f, 0x20, 0x54, 0x68, 0x65, 0x20, 0x6e,
    0x75, 0x6d, 0x62, 0x65, 0x72, 0x20, 0x6f, 0x66, 0x20, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x20, 0x70,
    0x6f, 0x69, 0x6e, 0x74, 0x73, 0x20, 0x74, 0x68, 0x65, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x20, 0x68,
    0x61, 0x73, 0x20, 0x66, 0x6f, 0x75, 0x6e, 0x64, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02,
    0x08, 0x04, 0x12, 0x04, 0xc7, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x08,
    0x05, 0x12, 0x04, 0xc7, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x08, 0x01,
    0x12, 0x04, 0xc7, 0x01, 0x13, 0x20, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x02, 0x08, 0x03, 0x12,
    0x04, 0xc7, 0x01, 0x23, 0x24, 0x0a, 0x0e, 0x0a, 0x04, 0x04, 0x0a, 0x03, 0x00, 0x12, 0x06, 0xc9,
    0x01, 0x03, 0xcc, 0x01, 0x04, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0a, 0x03, 0x00, 0x01, 0x12, 0x04,
    0xc9, 0x01, 0x0b, 0x10, 0x0a, 0x0e, 0x0a, 0x06, 0x04, 0x0a, 0x03, 0x00, 0x02, 0x00, 0x12, 0x04,
    0xca, 0x01, 0x06, 0x1d, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a, 0x03, 0x00, 0x02, 0x00, 0x04, 0x12,
    0x04, 0xca, 0x01, 0x06, 0x0e, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a, 0x03, 0x00, 0x02, 0x00, 0x05,
    0x12, 0x04, 0xca, 0x01, 0x0f, 0x15, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a, 0x03, 0x00, 0x02, 0x00,
    0x01, 0x12, 0x04, 0xca, 0x01, 0x16, 0x18, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a, 0x03, 0x00, 0x02,
    0x00, 0x03, 0x12, 0x04, 0xca, 0x01, 0x1b, 0x1c, 0x0a, 0x0e, 0x0a, 0x06, 0x04, 0x0a, 0x03, 0x00,
    0x02, 0x01, 0x12, 0x04, 0xcb, 0x01, 0x06, 0x25, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a, 0x03, 0x00,
    0x02, 0x01, 0x04, 0x12, 0x04, 0xcb, 0x01, 0x06, 0x0e, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a, 0x03,
    0x00, 0x02, 0x01, 0x05, 0x12, 0x04, 0xcb, 0x01, 0x0f, 0x15, 0x0a, 0x0f, 0x0a, 0x07, 0x04, 0x0a,
    0x03, 0x00, 0x02, 0x01, 0x01, 0x12, 0x04, 0xcb, 0x01, 0x16, 0x20, 0x0a, 0x0f, 0x0a, 0x07, 0x04,
    0x0a, 0x03, 0x00, 0x02, 0x01, 0x03, 0x12, 0x04, 0xcb, 0x01, 0x23, 0x24, 0x0a, 0xc3, 0x01, 0x0a,
    0x02, 0x04, 0x0b, 0x12, 0x06, 0xd2, 0x01, 0x00, 0xe0, 0x01, 0x01, 0x1a, 0xb4, 0x01, 0x20, 0x41,
    0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x69, 0x6e, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x74, 0x72, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x2e, 0x0a,
    0x20, 0x41, 0x63, 0x63, 0x65, 0x73, 0x73, 0x69, 0x62, 0x6c, 0x65, 0x20, 0x74, 0x6f, 0x20, 0x70,
    0x6c, 0x61, 0x79, 0x65, 0x72, 0x73, 0x20, 0x6f, 0x6e, 0x6c, 0x79, 0x20, 0x77, 0x68, 0x65, 0x6e,
    0x20, 0x74, 0x68, 0x65, 0x69, 0x72, 0x20, 0x74, 0x65, 0x61, 0x6d, 0x20, 0x68, 0x61, 0x73, 0x20,
    0x66, 0x6f, 0x75, 0x6e, 0x64, 0x20, 0x6f, 0x72, 0x20, 0x69, 0x73, 0x20, 0x6c, 0x6f, 0x6f, 0x6b,
    0x69, 0x6e, 0x67, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x74, 0x68, 0x69, 0x73, 0x0a, 0x20, 0x6c, 0x6f,
    0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x20, 0x42, 0x65, 0x66, 0x6f, 0x72, 0x65, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x20, 0x69, 0x73, 0x20, 0x66, 0x6f, 0x75, 0x6e,
    0x64, 0x2c, 0x20, 0x6f, 0x6e, 0x6c, 0x79, 0x20, 0x74, 0x68, 0x65, 0x20, 0x49, 0x44, 0x20, 0x61,
    0x6e, 0x64, 0x20, 0x63, 0x6c, 0x75, 0x65, 0x20, 0x61, 0x72, 0x65, 0x20, 0x73, 0x65, 0x6e, 0x74,
    0x2e, 0x0a, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x0b, 0x01, 0x12, 0x04, 0xd2, 0x01, 0x08, 0x0d, 0x0a,
    0x0c, 0x0a, 0x04, 0x04, 0x0b, 0x02, 0x00, 0x12, 0x04, 0xd3, 0x01, 0x03, 0x1a, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0b, 0x02, 0x00, 0x04, 0x12, 0x04, 0xd3, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0b, 0x02, 0x00, 0x05, 0x12, 0x04, 0xd3, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0b, 0x02, 0x00, 0x01, 0x12, 0x04, 0xd3, 0x01, 0x13, 0x15, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b,
    0x02, 0x00, 0x03, 0x12, 0x04, 0xd3, 0x01, 0x18, 0x19, 0x0a, 0x34, 0x0a, 0x04, 0x04, 0x0b, 0x02,
    0x01, 0x12, 0x04, 0xd5, 0x01, 0x03, 0x1c, 0x1a, 0x26, 0x20, 0x54, 0x68, 0x65, 0x20, 0x63, 0x6c,
    0x75, 0x65, 0x20, 0x61, 0x73, 0x73, 0x6f, 0x63, 0x69, 0x61, 0x74, 0x65, 0x64, 0x20, 0x77, 0x69,
    0x74, 0x68, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x2e, 0x0a, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x01, 0x04, 0x12, 0x04, 0xd5, 0x01, 0x03, 0x0b, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0b, 0x02, 0x01, 0x05, 0x12, 0x04, 0xd5, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0b, 0x02, 0x01, 0x01, 0x12, 0x04, 0xd5, 0x01, 0x13, 0x17, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0b, 0x02, 0x01, 0x03, 0x12, 0x04, 0xd5, 0x01, 0x1a, 0x1b, 0x0a, 0x4e, 0x0a, 0x04, 0x04,
    0x0b, 0x02, 0x02, 0x12, 0x04, 0xd7, 0x01, 0x03, 0x20, 0x1a, 0x40, 0x20, 0x54, 0x68, 0x65, 0x20,
    0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x75, 0x6e, 0x6c,
    0x6f, 0x63, 0x6b, 0x69, 0x6e, 0x67, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x70, 0x6f, 0x69, 0x6e,
    0x74, 0x2e, 0x20, 0x4e, 0x6f, 0x74, 0x20, 0x76, 0x69, 0x73, 0x69, 0x62, 0x6c, 0x65, 0x20, 0x74,
    0x6f, 0x20, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x73, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0b, 0x02, 0x02, 0x04, 0x12, 0x04, 0xd7, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b,
    0x02, 0x02, 0x05, 0x12, 0x04, 0xd7, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02,
    0x02, 0x01, 0x12, 0x04, 0xd7, 0x01, 0x13, 0x1b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x02,
    0x03, 0x12, 0x04, 0xd7, 0x01, 0x1e, 0x1f, 0x0a, 0x5f, 0x0a, 0x04, 0x04, 0x0b, 0x02, 0x03, 0x12,
    0x04, 0xd9, 0x01, 0x03, 0x22, 0x1a, 0x51, 0x20, 0x54, 0x68, 0x65, 0x20, 0x70, 0x6f, 0x69, 0x6e,
    0x74, 0x27, 0x73, 0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x2d, 0x2d, 0x20,
    0x6f, 0x6e, 0x6c, 0x79, 0x20, 0x73, 0x68, 0x6f, 0x77, 0x6e, 0x20, 0x74, 0x6f, 0x20, 0x70, 0x6c,
    0x61, 0x79, 0x65, 0x72, 0x73, 0x20, 0x61, 0x66, 0x74, 0x65, 0x72, 0x20, 0x74, 0x68, 0x65, 0x79,
    0x27, 0x76, 0x65, 0x20, 0x75, 0x6e, 0x6c, 0x6f, 0x63, 0x6b, 0x65, 0x64, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x03,
    0x04, 0x12, 0x04, 0xd9, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x03, 0x06,
    0x12, 0x04, 0xd9, 0x01, 0x0c, 0x14, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x03, 0x01, 0x12,
    0x04, 0xd9, 0x01, 0x15, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x03, 0x03, 0x12, 0x04,
    0xd9, 0x01, 0x20, 0x21, 0x0a, 0xc7, 0x01, 0x0a, 0x04, 0x04, 0x0b, 0x02, 0x04, 0x12, 0x04, 0xdd,
    0x01, 0x03, 0x22, 0x1a, 0xb8, 0x01, 0x20, 0x53, 0x6f, 0x6d, 0x65, 0x20, 0x6d, 0x65, 0x61, 0x73,
    0x75, 0x72, 0x65, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x64, 0x69, 0x66, 0x66, 0x69,
    0x63, 0x75, 0x6c, 0x74, 0x79, 0x20, 0x6f, 0x66, 0x20, 0x66, 0x69, 0x6e, 0x64, 0x69, 0x6e, 0x67,
    0x20, 0x74, 0x68, 0x65, 0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x67, 0x69,
    0x76, 0x65, 0x6e, 0x20, 0x74, 0x68, 0x65, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x0a, 0x20, 0x55,
    0x73, 0x65, 0x64, 0x20, 0x61, 0x73, 0x20, 0x70, 0x61, 0x72, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x61,
    0x20, 0x72, 0x75, 0x62, 0x62, 0x65, 0x72, 0x2d, 0x62, 0x61, 0x6e, 0x64, 0x69, 0x6e, 0x67, 0x20,
    0x70, 0x6f, 0x69, 0x6e, 0x74, 0x20, 0x73, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x20,
    0x68, 0x65, 0x75, 0x72, 0x69, 0x73, 0x74, 0x69, 0x63, 0x20, 0x74, 0x6f, 0x20, 0x6b, 0x65, 0x65,
    0x70, 0x20, 0x74, 0x68, 0x65, 0x0a, 0x20, 0x73, 0x63, 0x6f, 0x72, 0x65, 0x73, 0x20, 0x63, 0x6c,
    0x6f, 0x73, 0x65, 0x20, 0x74, 0x6f, 0x67, 0x65, 0x74, 0x68, 0x65, 0x72, 0x20, 0x69, 0x6e, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x6d, 0x69, 0x64, 0x2d, 0x67, 0x61, 0x6d, 0x65, 0x2e, 0x0a, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0b, 0x02, 0x04, 0x04, 0x12, 0x04, 0xdd, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0b, 0x02, 0x04, 0x05, 0x12, 0x04, 0xdd, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0b, 0x02, 0x04, 0x01, 0x12, 0x04, 0xdd, 0x01, 0x13, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0b, 0x02, 0x04, 0x03, 0x12, 0x04, 0xdd, 0x01, 0x20, 0x21, 0x0a, 0x5b, 0x0a, 0x04, 0x04, 0x0b,
    0x02, 0x05, 0x12, 0x04, 0xdf, 0x01, 0x03, 0x21, 0x1a, 0x4d, 0x20, 0x57, 0x68, 0x65, 0x6e, 0x20,
    0x73, 0x65, 0x74, 0x20, 0x6f, 0x6e, 0x20, 0x63, 0x72, 0x65, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2c,
    0x20, 0x74, 0x68, 0x65, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x20, 0x77, 0x69, 0x6c, 0x6c, 0x20,
    0x62, 0x65, 0x20, 0x61, 0x64, 0x64, 0x65, 0x64, 0x20, 0x74, 0x6f, 0x20, 0x74, 0x68, 0x65, 0x20,
    0x68, 0x75, 0x6e, 0x74, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20, 0x74, 0x68, 0x65, 0x20, 0x67, 0x69,
    0x76, 0x65, 0x6e, 0x20, 0x49, 0x44, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x05, 0x04,
    0x12, 0x04, 0xdf, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x05, 0x05, 0x12,
    0x04, 0xdf, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x05, 0x01, 0x12, 0x04,
    0xdf, 0x01, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0b, 0x02, 0x05, 0x03, 0x12, 0x04, 0xdf,
    0x01, 0x1f, 0x20, 0x0a, 0x88, 0x01, 0x0a, 0x02, 0x04, 0x0c, 0x12, 0x06, 0xe4, 0x01, 0x00, 0xf3,
    0x01, 0x01, 0x1a, 0x7a, 0x20, 0x41, 0x6e, 0x20, 0x65, 0x78, 0x74, 0x72, 0x61, 0x20, 0x6c, 0x6f,
    0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x28, 0x73, 0x74, 0x61, 0x74, 0x75, 0x65, 0x2f, 0x6c,
    0x61, 0x6e, 0x64, 0x6d, 0x61, 0x72, 0x6b, 0x2f, 0x65, 0x74, 0x63, 0x29, 0x20, 0x6e, 0x6f, 0x74,
    0x20, 0x70, 0x61, 0x72, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x6d, 0x61, 0x69,
    0x6e, 0x20, 0x74, 0x72, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x2c,
    0x0a, 0x20, 0x77, 0x68, 0x69, 0x63, 0x68, 0x20, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x73, 0x20,
    0x63, 0x61, 0x6e, 0x20, 0x66, 0x69, 0x6e, 0x64, 0x20, 0x74, 0x6f, 0x20, 0x77, 0x69, 0x6e, 0x20,
    0x62, 0x6f, 0x6e, 0x75, 0x73, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x2e, 0x0a, 0x0a, 0x0b,
    0x0a, 0x03, 0x04, 0x0c, 0x01, 0x12, 0x04, 0xe4, 0x01, 0x08, 0x0d, 0x0a, 0x0c, 0x0a, 0x04, 0x04,
    0x0c, 0x02, 0x00, 0x12, 0x04, 0xe5, 0x01, 0x03, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02,
    0x00, 0x04, 0x12, 0x04, 0xe5, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x00,
    0x05, 0x12, 0x04, 0xe5, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x00, 0x01,
    0x12, 0x04, 0xe5, 0x01, 0x13, 0x15, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x00, 0x03, 0x12,
    0x04, 0xe5, 0x01, 0x18, 0x19, 0x0a, 0x34, 0x0a, 0x04, 0x04, 0x0c, 0x02, 0x01, 0x12, 0x04, 0xe7,
    0x01, 0x03, 0x21, 0x1a, 0x26, 0x20, 0x41, 0x6e, 0x20, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x20, 0x6f,
    0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2c, 0x20,
    0x61, 0x73, 0x20, 0x61, 0x20, 0x68, 0x69, 0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0c, 0x02, 0x01, 0x04, 0x12, 0x04, 0xe7, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c,
    0x02, 0x01, 0x05, 0x12, 0x04, 0xe7, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02,
    0x01, 0x01, 0x12, 0x04, 0xe7, 0x01, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x01,
    0x03, 0x12, 0x04, 0xe7, 0x01, 0x1f, 0x20, 0x0a, 0x34, 0x0a, 0x04, 0x04, 0x0c, 0x02, 0x02, 0x12,
    0x04, 0xe9, 0x01, 0x03, 0x1c, 0x1a, 0x26, 0x20, 0x41, 0x20, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
    0x70, 0x74, 0x69, 0x76, 0x65, 0x20, 0x6e, 0x61, 0x6d, 0x65, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0c, 0x02, 0x02, 0x04, 0x12, 0x04, 0xe9, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0c, 0x02, 0x02, 0x05, 0x12, 0x04, 0xe9, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0c, 0x02, 0x02, 0x01, 0x12, 0x04, 0xe9, 0x01, 0x13, 0x17, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c,
    0x02, 0x02, 0x03, 0x12, 0x04, 0xe9, 0x01, 0x1a, 0x1b, 0x0a, 0x57, 0x0a, 0x04, 0x04, 0x0c, 0x02,
    0x03, 0x12, 0x04, 0xeb, 0x01, 0x03, 0x22, 0x1a, 0x49, 0x20, 0x54, 0x68, 0x65, 0x20, 0x61, 0x63,
    0x74, 0x75, 0x61, 0x6c, 0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x6f, 0x66,
    0x20, 0x74, 0x68, 0x65, 0x20, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x2c, 0x20, 0x6f, 0x6e, 0x6c, 0x79,
    0x20, 0x73, 0x65, 0x74, 0x20, 0x61, 0x66, 0x74, 0x65, 0x72, 0x20, 0x74, 0x68, 0x65, 0x20, 0x74,
    0x65, 0x61, 0x6d, 0x20, 0x68, 0x61, 0x73, 0x20, 0x66, 0x6f, 0x75, 0x6e, 0x64, 0x20, 0x69, 0x74,
    0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x03, 0x04, 0x12, 0x04, 0xeb, 0x01, 0x03,
    0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x03, 0x06, 0x12, 0x04, 0xeb, 0x01, 0x0c, 0x14,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x03, 0x01, 0x12, 0x04, 0xeb, 0x01, 0x15, 0x1d, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x03, 0x03, 0x12, 0x04, 0xeb, 0x01, 0x20, 0x21, 0x0a, 0x7e,
    0x0a, 0x04, 0x04, 0x0c, 0x02, 0x04, 0x12, 0x04, 0xee, 0x01, 0x03, 0x22, 0x1a, 0x70, 0x20, 0x53,
    0x6f, 0x6d, 0x65, 0x20, 0x6d, 0x65, 0x61, 0x73, 0x75, 0x72, 0x65, 0x20, 0x6f, 0x66, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x64, 0x69, 0x66, 0x66, 0x69, 0x63, 0x75, 0x6c, 0x74, 0x79, 0x20, 0x6f, 0x66,
    0x20, 0x66, 0x69, 0x6e, 0x64, 0x69, 0x6e, 0x67, 0x20, 0x74, 0x68, 0x65, 0x20, 0x6c, 0x6f, 0x63,
    0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2c, 0x20, 0x6d, 0x6f, 0x72, 0x65, 0x20, 0x64, 0x69, 0x66, 0x66,
    0x69, 0x63, 0x75, 0x6c, 0x74, 0x0a, 0x20, 0x6c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x73,
    0x20, 0x77, 0x69, 0x6c, 0x6c, 0x20, 0x67, 0x69, 0x76, 0x65, 0x20, 0x6d, 0x6f, 0x72, 0x65, 0x20,
    0x62, 0x6f, 0x6e, 0x75, 0x73, 0x20, 0x70, 0x6f, 0x69, 0x6e, 0x74, 0x73, 0x2e, 0x0a, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0c, 0x02, 0x04, 0x04, 0x12, 0x04, 0xee, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0c, 0x02, 0x04, 0x05, 0x12, 0x04, 0xee, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0c, 0x02, 0x04, 0x01, 0x12, 0x04, 0xee, 0x01, 0x13, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0c, 0x02, 0x04, 0x03, 0x12, 0x04, 0xee, 0x01, 0x20, 0x21, 0x0a, 0x36, 0x0a, 0x04, 0x04, 0x0c,
    0x02, 0x05, 0x12, 0x04, 0xf0, 0x01, 0x03, 0x20, 0x1a, 0x28, 0x20, 0x54, 0x68, 0x65, 0x20, 0x70,
    0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x20, 0x77, 0x68, 0x69, 0x63, 0x68, 0x20, 0x75, 0x6e,
    0x6c, 0x6f, 0x63, 0x6b, 0x73, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x62, 0x6f, 0x6e, 0x75, 0x73,
    0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x05, 0x04, 0x12, 0x04, 0xf0, 0x01, 0x03,
    0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x05, 0x05, 0x12, 0x04, 0xf0, 0x01, 0x0c, 0x12,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x05, 0x01, 0x12, 0x04, 0xf0, 0x01, 0x13, 0x1b, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x05, 0x03, 0x12, 0x04, 0xf0, 0x01, 0x1e, 0x1f, 0x0a, 0x5b,
    0x0a, 0x04, 0x04, 0x0c, 0x02, 0x06, 0x12, 0x04, 0xf2, 0x01, 0x03, 0x21, 0x1a, 0x4d, 0x20, 0x57,
    0x68, 0x65, 0x6e, 0x20, 0x73, 0x65, 0x74, 0x20, 0x6f, 0x6e, 0x20, 0x63, 0x72, 0x65, 0x61, 0x74,
    0x69, 0x6f, 0x6e, 0x2c, 0x20, 0x74, 0x68, 0x65, 0x20, 0x62, 0x6f, 0x6e, 0x75, 0x73, 0x20, 0x77,
    0x69, 0x6c, 0x6c, 0x20, 0x62, 0x65, 0x20, 0x61, 0x64, 0x64, 0x65, 0x64, 0x20, 0x74, 0x6f, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20, 0x74, 0x68,
    0x65, 0x20, 0x67, 0x69, 0x76, 0x65, 0x6e, 0x20, 0x49, 0x44, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0c, 0x02, 0x06, 0x04, 0x12, 0x04, 0xf2, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c,
    0x02, 0x06, 0x05, 0x12, 0x04, 0xf2, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02,
    0x06, 0x01, 0x12, 0x04, 0xf2, 0x01, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0c, 0x02, 0x06,
    0x03, 0x12, 0x04, 0xf2, 0x01, 0x1f, 0x20, 0x0a, 0x25, 0x0a, 0x02, 0x04, 0x0d, 0x12, 0x06, 0xf6,
    0x01, 0x00, 0x82, 0x02, 0x01, 0x1a, 0x17, 0x20, 0x41, 0x20, 0x73, 0x74, 0x72, 0x65, 0x61, 0x6d,
    0x20, 0x6f, 0x66, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x73, 0x2e, 0x0a, 0x0a, 0x0b,
    0x0a, 0x03, 0x04, 0x0d, 0x01, 0x12, 0x04, 0xf6, 0x01, 0x08, 0x0c, 0x0a, 0x0c, 0x0a, 0x04, 0x04,
    0x0d, 0x02, 0x00, 0x12, 0x04, 0xf7, 0x01, 0x03, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02,
    0x00, 0x04, 0x12, 0x04, 0xf7, 0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x00,
    0x05, 0x12, 0x04, 0xf7, 0x01, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x00, 0x01,
    0x12, 0x04, 0xf7, 0x01, 0x13, 0x15, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x00, 0x03, 0x12,
    0x04, 0xf7, 0x01, 0x18, 0x19, 0x0a, 0x4d, 0x0a, 0x04, 0x04, 0x0d, 0x02, 0x01, 0x12, 0x04, 0xf9,
    0x01, 0x03, 0x22, 0x1a, 0x3f, 0x20, 0x54, 0x68, 0x65, 0x20, 0x6e, 0x61, 0x6d, 0x65, 0x20, 0x6f,
    0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x68, 0x61, 0x74, 0x20, 0x67, 0x72, 0x6f, 0x75, 0x70,
    0x2c, 0x20, 0x65, 0x2e, 0x67, 0x2e, 0x20, 0x22, 0x42, 0x72, 0x6f, 0x61, 0x64, 0x63, 0x61, 0x73,
    0x74, 0x73, 0x22, 0x2c, 0x20, 0x22, 0x54, 0x65, 0x61, 0x6d, 0x20, 0x32, 0x30, 0x22, 0x2c, 0x20,
    0x2e, 0x2e, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x01, 0x04, 0x12, 0x04, 0xf9,
    0x01, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x01, 0x05, 0x12, 0x04, 0xf9, 0x01,
    0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x01, 0x01, 0x12, 0x04, 0xf9, 0x01, 0x13,
    0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x01, 0x03, 0x12, 0x04, 0xf9, 0x01, 0x20, 0x21,
    0x0a, 0x3e, 0x0a, 0x04, 0x04, 0x0d, 0x02, 0x02, 0x12, 0x04, 0xfb, 0x01, 0x03, 0x24, 0x1a, 0x30,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x73, 0x65, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x75, 0x73, 0x65, 0x72,
    0x20, 0x49, 0x44, 0x73, 0x20, 0x77, 0x68, 0x6f, 0x20, 0x63, 0x61, 0x6e, 0x20, 0x70, 0x6f, 0x73,
    0x74, 0x20, 0x69, 0x6e, 0x20, 0x74, 0x68, 0x69, 0x73, 0x20, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x0a,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x02, 0x04, 0x12, 0x04, 0xfb, 0x01, 0x03, 0x0b, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x02, 0x05, 0x12, 0x04, 0xfb, 0x01, 0x0c, 0x12, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0d, 0x02, 0x02, 0x01, 0x12, 0x04, 0xfb, 0x01, 0x13, 0x1f, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0d, 0x02, 0x02, 0x03, 0x12, 0x04, 0xfb, 0x01, 0x22, 0x23, 0x0a, 0x54, 0x0a, 0x04,
    0x04, 0x0d, 0x02, 0x03, 0x12, 0x04, 0xfd, 0x01, 0x03, 0x20, 0x1a, 0x46, 0x20, 0x54, 0x68, 0x65,
    0x20, 0x73, 0x65, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x35, 0x30, 0x20, 0x6d,
    0x6f, 0x73, 0x74, 0x20, 0x72, 0x65, 0x63, 0x65, 0x6e, 0x74, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61,
    0x67, 0x65, 0x20, 0x49, 0x44, 0x73, 0x2e, 0x20, 0x4f, 0x6c, 0x64, 0x20, 0x6d, 0x65, 0x73, 0x73,
    0x61, 0x67, 0x65, 0x73, 0x20, 0x61, 0x72, 0x65, 0x20, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64,
    0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x03, 0x04, 0x12, 0x04, 0xfd, 0x01, 0x03,
    0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x03, 0x05, 0x12, 0x04, 0xfd, 0x01, 0x0c, 0x12,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x03, 0x01, 0x12, 0x04, 0xfd, 0x01, 0x13, 0x1b, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x03, 0x03, 0x12, 0x04, 0xfd, 0x01, 0x1e, 0x1f, 0x0a, 0x50,
    0x0a, 0x04, 0x04, 0x0d, 0x02, 0x04, 0x12, 0x04, 0xff, 0x01, 0x03, 0x24, 0x1a, 0x42, 0x20, 0x54,
    0x68, 0x65, 0x20, 0x73, 0x65, 0x74, 0x20, 0x6f, 0x66, 0x20, 0x75, 0x73, 0x65, 0x72, 0x73, 0x20,
    0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x6c, 0x79, 0x20, 0x74, 0x79, 0x70, 0x69, 0x6e, 0x67,
    0x20, 0x61, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x28, 0x6e, 0x6f, 0x74, 0x20,
    0x79, 0x65, 0x74, 0x20, 0x69, 0x6d, 0x70, 0x6c, 0x6d, 0x65, 0x6e, 0x74, 0x65, 0x64, 0x29, 0x0a,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x04, 0x04, 0x12, 0x04, 0xff, 0x01, 0x03, 0x0b, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x04, 0x05, 0x12, 0x04, 0xff, 0x01, 0x0c, 0x12, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x0d, 0x02, 0x04, 0x01, 0x12, 0x04, 0xff, 0x01, 0x13, 0x1f, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x0d, 0x02, 0x04, 0x03, 0x12, 0x04, 0xff, 0x01, 0x22, 0x23, 0x0a, 0x4a, 0x0a, 0x04,
    0x04, 0x0d, 0x02, 0x05, 0x12, 0x04, 0x81, 0x02, 0x03, 0x22, 0x1a, 0x3c, 0x20, 0x57, 0x68, 0x65,
    0x6e, 0x20, 0x74, 0x72, 0x75, 0x65, 0x2c, 0x20, 0x61, 0x6c, 0x6c, 0x20, 0x70, 0x6c, 0x61, 0x79,
    0x65, 0x72, 0x73, 0x20, 0x69, 0x6e, 0x20, 0x74, 0x68, 0x65, 0x20, 0x68, 0x75, 0x6e, 0x74, 0x20,
    0x63, 0x61, 0x6e, 0x20, 0x72, 0x65, 0x61, 0x64, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
    0x73, 0x20, 0x68, 0x65, 0x72, 0x65, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x05,
    0x04, 0x12, 0x04, 0x81, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x05, 0x05,
    0x12, 0x04, 0x81, 0x02, 0x0c, 0x10, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x05, 0x01, 0x12,
    0x04, 0x81, 0x02, 0x11, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0d, 0x02, 0x05, 0x03, 0x12, 0x04,
    0x81, 0x02, 0x20, 0x21, 0x0a, 0x26, 0x0a, 0x02, 0x04, 0x0e, 0x12, 0x06, 0x85, 0x02, 0x00, 0x91,
    0x02, 0x01, 0x1a, 0x18, 0x20, 0x41, 0x20, 0x73, 0x69, 0x6e, 0x67, 0x6c, 0x65, 0x20, 0x74, 0x65,
    0x78, 0x74, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x0a, 0x0a, 0x0b, 0x0a, 0x03,
    0x04, 0x0e, 0x01, 0x12, 0x04, 0x85, 0x02, 0x08, 0x13, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x0e, 0x02,
    0x00, 0x12, 0x04, 0x86, 0x02, 0x03, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x00, 0x04,
    0x12, 0x04, 0x86, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x00, 0x05, 0x12,
    0x04, 0x86, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x00, 0x01, 0x12, 0x04,
    0x86, 0x02, 0x13, 0x15, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x00, 0x03, 0x12, 0x04, 0x86,
    0x02, 0x18, 0x19, 0x0a, 0x3f, 0x0a, 0x04, 0x04, 0x0e, 0x02, 0x01, 0x12, 0x04, 0x88, 0x02, 0x03,
    0x1f, 0x1a, 0x31, 0x20, 0x54, 0x68, 0x65, 0x20, 0x49, 0x44, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68,
    0x65, 0x20, 0x63, 0x68, 0x74, 0x20, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x20, 0x74, 0x68, 0x65, 0x20,
    0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x62, 0x65, 0x6c, 0x6f, 0x6e, 0x67, 0x73, 0x20,
    0x74, 0x6f, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x01, 0x04, 0x12, 0x04, 0x88,
    0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x01, 0x05, 0x12, 0x04, 0x88, 0x02,
    0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x01, 0x01, 0x12, 0x04, 0x88, 0x02, 0x13,
    0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x01, 0x03, 0x12, 0x04, 0x88, 0x02, 0x1d, 0x1e,
    0x0a, 0x37, 0x0a, 0x04, 0x04, 0x0e, 0x02, 0x02, 0x12, 0x04, 0x8a, 0x02, 0x03, 0x21, 0x1a, 0x29,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x49, 0x44, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x75,
    0x73, 0x65, 0x72, 0x20, 0x77, 0x68, 0x6f, 0x20, 0x73, 0x65, 0x6e, 0x74, 0x20, 0x74, 0x68, 0x65,
    0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02,
    0x02, 0x04, 0x12, 0x04, 0x8a, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x02,
    0x05, 0x12, 0x04, 0x8a, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x02, 0x01,
    0x12, 0x04, 0x8a, 0x02, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x02, 0x03, 0x12,
    0x04, 0x8a, 0x02, 0x1f, 0x20, 0x0a, 0x21, 0x0a, 0x04, 0x04, 0x0e, 0x02, 0x03, 0x12, 0x04, 0x8c,
    0x02, 0x03, 0x1c, 0x1a, 0x13, 0x20, 0x54, 0x68, 0x65, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
    0x65, 0x73, 0x20, 0x74, 0x65, 0x78, 0x74, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x03,
    0x04, 0x12, 0x04, 0x8c, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x03, 0x05,
    0x12, 0x04, 0x8c, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x03, 0x01, 0x12,
    0x04, 0x8c, 0x02, 0x13, 0x17, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x03, 0x03, 0x12, 0x04,
    0x8c, 0x02, 0x1a, 0x1b, 0x0a, 0x33, 0x0a, 0x04, 0x04, 0x0e, 0x02, 0x04, 0x12, 0x04, 0x8e, 0x02,
    0x03, 0x21, 0x1a, 0x25, 0x20, 0x54, 0x68, 0x65, 0x20, 0x74, 0x69, 0x6d, 0x65, 0x20, 0x74, 0x68,
    0x65, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x77, 0x61, 0x73, 0x20, 0x66, 0x69,
    0x72, 0x73, 0x74, 0x20, 0x73, 0x65, 0x6e, 0x74, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02,
    0x04, 0x04, 0x12, 0x04, 0x8e, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x04,
    0x05, 0x12, 0x04, 0x8e, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x04, 0x01,
    0x12, 0x04, 0x8e, 0x02, 0x13, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x04, 0x03, 0x12,
    0x04, 0x8e, 0x02, 0x1f, 0x20, 0x0a, 0x34, 0x0a, 0x04, 0x04, 0x0e, 0x02, 0x05, 0x12, 0x04, 0x90,
    0x02, 0x03, 0x23, 0x1a, 0x26, 0x20, 0x54, 0x68, 0x65, 0x20, 0x74, 0x69, 0x6d, 0x65, 0x20, 0x74,
    0x68, 0x65, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x77, 0x61, 0x73, 0x20, 0x6c,
    0x61, 0x73, 0x74, 0x20, 0x65, 0x64, 0x69, 0x74, 0x65, 0x64, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0e, 0x02, 0x05, 0x04, 0x12, 0x04, 0x90, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e,
    0x02, 0x05, 0x05, 0x12, 0x04, 0x90, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02,
    0x05, 0x01, 0x12, 0x04, 0x90, 0x02, 0x13, 0x1e, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0e, 0x02, 0x05,
    0x03, 0x12, 0x04, 0x90, 0x02, 0x21, 0x22, 0x0a, 0x34, 0x0a, 0x02, 0x04, 0x0f, 0x12, 0x06, 0x94,
    0x02, 0x00, 0x99, 0x02, 0x01, 0x1a, 0x26, 0x20, 0x54, 0x68, 0x65, 0x20, 0x6c, 0x6f, 0x67, 0x69,
    0x6e, 0x20, 0x64, 0x65, 0x74, 0x61, 0x69, 0x6c, 0x73, 0x20, 0x66, 0x6f, 0x72, 0x20, 0x61, 0x20,
    0x73, 0x69, 0x6e, 0x67, 0x6c, 0x65, 0x20, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x0a, 0x0a, 0x0b, 0x0a,
    0x03, 0x04, 0x0f, 0x01, 0x12, 0x04, 0x94, 0x02, 0x08, 0x14, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x0f,
    0x02, 0x00, 0x12, 0x04, 0x95, 0x02, 0x03, 0x1f, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x00,
    0x04, 0x12, 0x04, 0x95, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x00, 0x05,
    0x12, 0x04, 0x95, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x00, 0x01, 0x12,
    0x04, 0x95, 0x02, 0x13, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x00, 0x03, 0x12, 0x04,
    0x95, 0x02, 0x1d, 0x1e, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x0f, 0x02, 0x01, 0x12, 0x04, 0x96, 0x02,
    0x03, 0x20, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x01, 0x04, 0x12, 0x04, 0x96, 0x02, 0x03,
    0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x01, 0x05, 0x12, 0x04, 0x96, 0x02, 0x0c, 0x12,
    0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x01, 0x01, 0x12, 0x04, 0x96, 0x02, 0x13, 0x1b, 0x0a,
    0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x01, 0x03, 0x12, 0x04, 0x96, 0x02, 0x1e, 0x1f, 0x0a, 0x0c,
    0x0a, 0x04, 0x04, 0x0f, 0x02, 0x02, 0x12, 0x04, 0x97, 0x02, 0x03, 0x20, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x0f, 0x02, 0x02, 0x04, 0x12, 0x04, 0x97, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x0f, 0x02, 0x02, 0x05, 0x12, 0x04, 0x97, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f,
    0x02, 0x02, 0x01, 0x12, 0x04, 0x97, 0x02, 0x13, 0x1b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02,
    0x02, 0x03, 0x12, 0x04, 0x97, 0x02, 0x1e, 0x1f, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x0f, 0x02, 0x03,
    0x12, 0x04, 0x98, 0x02, 0x03, 0x1c, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x03, 0x04, 0x12,
    0x04, 0x98, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x03, 0x05, 0x12, 0x04,
    0x98, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x03, 0x01, 0x12, 0x04, 0x98,
    0x02, 0x13, 0x17, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x0f, 0x02, 0x03, 0x03, 0x12, 0x04, 0x98, 0x02,
    0x1a, 0x1b, 0x0a, 0x2a, 0x0a, 0x02, 0x04, 0x10, 0x12, 0x06, 0x9c, 0x02, 0x00, 0x9e, 0x02, 0x01,
    0x1a, 0x1c, 0x20, 0x54, 0x68, 0x65, 0x20, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x20, 0x6f, 0x66,
    0x20, 0x73, 0x6f, 0x6d, 0x65, 0x20, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x0a, 0x0a, 0x0b,
    0x0a, 0x03, 0x04, 0x10, 0x01, 0x12, 0x04, 0x9c, 0x02, 0x08, 0x11, 0x0a, 0x0c, 0x0a, 0x04, 0x04,
    0x10, 0x02, 0x00, 0x12, 0x04, 0x9d, 0x02, 0x03, 0x1d, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x10, 0x02,
    0x00, 0x04, 0x12, 0x04, 0x9d, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x10, 0x02, 0x00,
    0x05, 0x12, 0x04, 0x9d, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x10, 0x02, 0x00, 0x01,
    0x12, 0x04, 0x9d, 0x02, 0x13, 0x18, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x10, 0x02, 0x00, 0x03, 0x12,
    0x04, 0x9d, 0x02, 0x1b, 0x1c, 0x0a, 0x0c, 0x0a, 0x02, 0x04, 0x11, 0x12, 0x06, 0xa0, 0x02, 0x00,
    0xa3, 0x02, 0x01, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x11, 0x01, 0x12, 0x04, 0xa0, 0x02, 0x08, 0x0f,
    0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x11, 0x02, 0x00, 0x12, 0x04, 0xa1, 0x02, 0x03, 0x1f, 0x0a, 0x0d,
    0x0a, 0x05, 0x04, 0x11, 0x02, 0x00, 0x04, 0x12, 0x04, 0xa1, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a,
    0x05, 0x04, 0x11, 0x02, 0x00, 0x05, 0x12, 0x04, 0xa1, 0x02, 0x0c, 0x12, 0x0a, 0x0d, 0x0a, 0x05,
    0x04, 0x11, 0x02, 0x00, 0x01, 0x12, 0x04, 0xa1, 0x02, 0x13, 0x1a, 0x0a, 0x0d, 0x0a, 0x05, 0x04,
    0x11, 0x02, 0x00, 0x03, 0x12, 0x04, 0xa1, 0x02, 0x1d, 0x1e, 0x0a, 0x0c, 0x0a, 0x04, 0x04, 0x11,
    0x02, 0x01, 0x12, 0x04, 0xa2, 0x02, 0x03, 0x27, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x11, 0x02, 0x01,
    0x04, 0x12, 0x04, 0xa2, 0x02, 0x03, 0x0b, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x11, 0x02, 0x01, 0x06,
    0x12, 0x04, 0xa2, 0x02, 0x0c, 0x18, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x11, 0x02, 0x01, 0x01, 0x12,
    0x04, 0xa2, 0x02, 0x19, 0x22, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x11, 0x02, 0x01, 0x03, 0x12, 0x04,
    0xa2, 0x02, 0x25, 0x26, 0x0a, 0x0c, 0x0a, 0x02, 0x04, 0x12, 0x12, 0x06, 0xa5, 0x02, 0x00, 0xaa,
    0x02, 0x01, 0x0a, 0x0b, 0x0a, 0x03, 0x04, 0x12, 0x01, 0x12, 0x04, 0xa5, 0x02, 0x08, 0x0d, 0x0a,
    0x53, 0x0a, 0x04, 0x04, 0x12, 0x02, 0x00, 0x12, 0x04, 0xa7, 0x02, 0x02, 0x1e, 0x1a, 0x45, 0x20,
    0x49, 0x66, 0x20, 0x74, 0x72, 0x75, 0x65, 0x2c, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x69,
    0x65, 0x6e, 0x74, 0x20, 0x6d, 0x61, 0x79, 0x20, 0x61, 0x74, 0x74, 0x65, 0x6d, 0x70, 0x74, 0x20,
    0x74, 0x6f, 0x20, 0x72, 0x65, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x20, 0x77, 0x69, 0x74,
    0x68, 0x20, 0x74, 0x68, 0x65, 0x20, 0x73, 0x61, 0x6d, 0x65, 0x20, 0x61, 0x63, 0x63, 0x6f, 0x75,
    0x6e, 0x74, 0x2e, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x00, 0x04, 0x12, 0x04, 0xa7,
    0x02, 0x02, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x00, 0x05, 0x12, 0x04, 0xa7, 0x02,
    0x0b, 0x0f, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x00, 0x01, 0x12, 0x04, 0xa7, 0x02, 0x10,
    0x19, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x00, 0x03, 0x12, 0x04, 0xa7, 0x02, 0x1c, 0x1d,
    0x0a, 0x55, 0x0a, 0x04, 0x04, 0x12, 0x02, 0x01, 0x12, 0x04, 0xa9, 0x02, 0x02, 0x1d, 0x1a, 0x47,
    0x20, 0x54, 0x68, 0x65, 0x20, 0x72, 0x65, 0x61, 0x73, 0x6f, 0x6e, 0x20, 0x66, 0x6f, 0x72, 0x20,
    0x74, 0x68, 0x65, 0x20, 0x72, 0x65, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x20, 0x72, 0x65,
    0x71, 0x75, 0x65, 0x73, 0x74, 0x20, 0x28, 0x65, 0x2e, 0x67, 0x2e, 0x20, 0x61, 0x20, 0x68, 0x75,
    0x6e, 0x74, 0x20, 0x68, 0x61, 0x73, 0x20, 0x62, 0x65, 0x65, 0x6e, 0x20, 0x72, 0x65, 0x73, 0x74,
    0x61, 0x72, 0x74, 0x65, 0x64, 0x29, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x01, 0x04,
    0x12, 0x04, 0xa9, 0x02, 0x02, 0x0a, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x01, 0x05, 0x12,
    0x04, 0xa9, 0x02, 0x0b, 0x11, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x01, 0x01, 0x12, 0x04,
    0xa9, 0x02, 0x12, 0x18, 0x0a, 0x0d, 0x0a, 0x05, 0x04, 0x12, 0x02, 0x01, 0x03, 0x12, 0x04, 0xa9,
    0x02, 0x1b, 0x1c,
];

static mut file_descriptor_proto_lazy: ::protobuf::lazy::Lazy<::protobuf::descriptor::FileDescriptorProto> = ::protobuf::lazy::Lazy {
    lock: ::protobuf::lazy::ONCE_INIT,
    ptr: 0 as *const ::protobuf::descriptor::FileDescriptorProto,
};

fn parse_descriptor_proto() -> ::protobuf::descriptor::FileDescriptorProto {
    ::protobuf::parse_from_bytes(file_descriptor_proto_data).unwrap()
}

pub fn file_descriptor_proto() -> &'static ::protobuf::descriptor::FileDescriptorProto {
    unsafe {
        file_descriptor_proto_lazy.get(|| {
            parse_descriptor_proto()
        })
    }
}
