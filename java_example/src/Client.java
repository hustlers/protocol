import java.io.*;
import java.util.Map;
import java.util.HashMap;
import javax.net.ssl.*;

import hustlers.huntdroid.protocol.*;
import hustlers.huntdroid.protocol.Message;

public class Client {
   private int loginSeq = -1;
   private int outSeq = 0;
   private ByteArrayOutputStream write_buffer = new ByteArrayOutputStream();
   private SSLSocket socket = null;
   private DataInputStream read = null;
   private DataOutputStream write = null;
   private String url;
   private String username;
   private String password;
   private Session session = null;
   private Map<Integer, User> users = new HashMap<>();
   private Map<Integer, Team> teams = new HashMap<>();
   private Map<Integer, Point> points = new HashMap<>();
   private Map<Integer, Hunt> hunts = new HashMap<>();

   public static void main(String[] args) throws IOException {
      Client client = new Client("hunt.xa1.uk", "username", "password");
      client.connect();
      client.run();
   }

   public Client(String url, String username, String password) {
      this.url = url;
      this.username = username;
      this.password = password;
   }

   /** Attempt to connect to the server and start a session */
   public Session connect() throws IOException {
      this.outSeq = 0;
      this.socket = (SSLSocket) SSLSocketFactory.getDefault()
              .createSocket(this.url, 25255);
      socket.setTcpNoDelay(true);
      socket.startHandshake();

      this.read = new DataInputStream(socket.getInputStream());
      this.write = new DataOutputStream(socket.getOutputStream());

      if (this.session == null) {
         this.loginSeq = this.send(Message.newBuilder()
                 .setLoginRequest(Login.newBuilder()
                         .setUsername(this.username)
                         .setPassword(this.password))
                 .build());
      } else {
         this.loginSeq = this.send(Message.newBuilder()
                 .setResume(this.session)
                 .build());
      }

      while(true) {
         Message msg = this.read();
         this.handle(msg);
         switch (msg.getMsgDataCase()) {
            case RESULT:
               if (msg.getRequestSeq() == this.loginSeq) {
                  ReqResult result = msg.getResult();
                  if (result.hasError()) {
                     System.out.println("Login request failed: " + result.getError());
                     this.resetSession();
                     return null;
                  } else {
                     // If there's no error, the login was successful, and the server
                     // will start sending all the data we need.
                     System.out.println("Login successful, loading data..");
                  }
               }
               break;
            case LOGIN_COMPLETE:
               // When we have all the data, the server sends a LoginComplete, which
               // gives us our ID. We know now that we have all the data we need --
               // the server will send more as it's created (when new points are
               // available), and overwrite existing data when it changes (e.g.
               // when a players location is updated)
               this.session = msg.getLoginComplete();
               System.out.println("Login complete.");
               return session;
         }
      }
   }

   /** Attempt to send a message to the server, reconnection if the connection
    * is lost. */
   public int send(Message msg) {
      msg = msg.toBuilder().setSeq(this.outSeq).build();
      this.outSeq += 1;
      this.sendInner(msg);
      return this.outSeq - 1;
   }

   /** Attempt to read an incoming message from the stream, reconnecting if the
    * connection is lost. */
   public Message read() {
      try {
         int len = read.readInt();
         byte[] buffer = new byte[len];
         read.readFully(buffer, 0, len);
         Message msg = Message.parseFrom(buffer);
         this.ack(msg.getSeq());
         return msg;
      } catch (IOException e) {
         this.resume(e);
         return this.read();
      }
   }

   public void run() {
      while(true) {
         this.handle(this.read());
      }
   }

   private void handle(Message msg) {
      switch(msg.getMsgDataCase()) {
         case RESULT:
            ReqResult result = msg.getResult();
            if (result.hasError()) {
               System.out.println("Server sent an error: " + result.getError());
               return;
            }
            break;

         case DATA:
            Data data = msg.getData();

            // Iterate through all the data items, check their type, and add each to the
            // relevant map.
            for(Data.DataItem item : data.getItemsList()) {
               switch(item.getDataCase()) {
                  case USER:
                     users.put(item.getUser().getId(), item.getUser()); break;
                  case TEAM:
                     teams.put(item.getTeam().getId(), item.getTeam()); break;
                  case POINT:
                     points.put(item.getPoint().getId(), item.getPoint()); break;
                  case HUNT:
                     hunts.put(item.getHunt().getId(), item.getHunt()); break;
                  default: break;
               }
            }
            break;
         default:
            break;
      }
   }

   private void ack(int seq) {
      Message msg = Message.newBuilder().setAck(seq).build();
      this.sendInner(msg);
   }

   private void sendInner(Message msg) {
      try {
         msg.writeTo(write_buffer);
         write.writeInt(write_buffer.size());
         write_buffer.writeTo(write);
         write_buffer.reset();
      } catch (IOException e) {
         this.resume(e);
         this.send(msg);
      }
   }

   private void resume(Exception e) {
      try { this.connect(); }
      catch (IOException e2) {
         System.out.println("Failed to resume broken connection: " + e + ".\n" + e);
      }
   }

   private void resetSession() {
      this.session = null;
      this.hunts.clear();
      this.points.clear();
      this.users.clear();
      this.teams.clear();
   }
}
