#!/bin/bash

protoc --rust_out ./ message.proto
rm rust.patch
diff message.rs out/rust/message.rs > rust.patch
rm message.rs
