This repository contains the schema file for the treasure hunt protocol and
the compiled source for Java and Rust.

# Protocol

`huntserv` currently implements the protocol over both TCP and websockets. In
both cases TLS 1.2 is used for security.

To deliminate messages for TCP, each message is prefixes with a four byte
big-endian integer giving it's length in bytes.

To enable session resumption we use asymmetric acknowledgements -- the client
must acknowledge all messages sent by the server, such that if the client
disconnects the server knows which messages need to be resent. Clients may omit
the sequence number in any messages they send.

## Sessions

The first message a client sends after establishing the connection should be
either a `login_request` or a `resume` -- the server will not send the client
any data until they are logged in.
A session is only established when a user logs in.

Upon logging in the server will send the client all relevant data. Updates to this
data and new data will be send as the treasure hunt progresses.

On a successful login the client is send their user ID and a session ID. If the
connection is lost, the client may send a `resume` request with this data within
two hours to re-establish the connection, upon which the server will send the
client any data which has changed since they disconnected.

When a new connection is established message sequence numbers are reset to 0.

In rare cases an attempt to resume a session can fail, in which case the client
should clear it's data and attempt to login again.
